{!! Form::open(array(URL::to('admin/delivery-receipt') . '/' . $deliveryReceipt->id.'/print', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">Print Delivery Receipt</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
		<div class="form-group">
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					Alamat (Line 1)
					<div class="input-group">
						<input type="text" name="alamat1" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Alamat (Line 2)
					<div class="input-group">
						<input type="text" name="alamat2" class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					No Kendaraan
					<div class="input-group">
						<input type="text" name="noKend" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Tanggal
					<div class="input-group">
						<input type="text" id="rangepicker4" name="tgl" class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					Blok
					<div class="input-group">
						<input type="text" name="blok" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Jam
					<div class="input-group">
						<input type="text" name="jam" class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					Proyek Manager
					<div class="input-group">
						<input type="text" name="pm" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Pelaksana
					<div class="input-group">
						<input type="text" name="pl" class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					Leveransir
					<div class="input-group">
						<input type="text" name="le" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Bag. Gudang/Logistik
					<div class="input-group">
						<input type="text" name="bl" class="form-control" />
					</div>
				</div>
			</div>
		</div>
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  @if(!$error)
	<button type="submit" class="btn btn-success">Print</button>
  @endif
</div>
{!! Form::close() !!}