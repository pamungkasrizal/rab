{!! Form::open(array(URL::to('admin/invoce/service-order') . '/' . $invoiceServiceOrder->id.'/reject', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">Reject Kuitansi SPK Subkon</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
        Are you sure to reject this Kuitansi SPK Subkon?
		<textarea name="status_msg" placeholder="Write something here..." class="form-control resize_vertical " data-autogrow="" rows="3" cols="30" style="height: 70px;"></textarea>
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">@lang($model.'/modal.cancel')</button>
  @if(!$error)
	<button type="submit" class="btn btn-success">Reject</button>
  @endif
</div>
{!! Form::close() !!}