<ul id="menu" class="page-sidebar-menu">
    <li {!! (Request::is('admin') || Request::is('dirop') ? 'class="active"' : '') !!}>
        <a href="{{ route('dashboard') }}">
            <i class="livicon" data-name="home" data-size="18" data-c="#418BCA" data-hc="#418BCA"
               data-loop="true"></i>
            <span class="title">Dashboard</span>
        </a>
    </li>
	<!-- START -->
	
	@if(Sentinel::inRole('admin') || Sentinel::inRole('kasir') || Sentinel::inRole('logistik_kantor'))
	<li {!! (Request::segment(2) == 'master' ? 'class="active"' : '') !!} >
        <a href="#">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            <span class="title">Master Data</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
			@if(Sentinel::inRole('admin') || Sentinel::inRole('kasir'))
            <li {!! (Request::segment(3) == 'cash-expense' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/master/cash-expense') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Bukti Kas
                </a>
            </li>
			@endif
			
			@if(Sentinel::inRole('admin') || Sentinel::inRole('logistik_kantor'))
			<li {!! (Request::segment(3) == 'inventory' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/master/inventory') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Material
                </a>
            </li>
			<li {!! (Request::segment(3) == 'service-master' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/master/service-master') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Item Pekerjaan Subkon
                </a>
            </li>
			<li {!! (Request::segment(3) == 'service-type' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/master/service-type') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Jenis Pekerjaan Subkon
                </a>
            </li>
			<li {!! (Request::segment(3) == 'subcontractor' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/master/subcontractor') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Daftar Subkon
                </a>
            </li>
			<li {!! (Request::segment(3) == 'supplier' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/master/supplier') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Daftar Supplier
                </a>
            </li>
			@endif
        </ul>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin') || Sentinel::inRole('administrasi'))
	<li {!! (Request::segment(2) == 'client' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/client') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            Client
        </a>
    </li>
	
	<li {!! (Request::segment(2) == 'project' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/project') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            Project
        </a>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin'))
	<li {!! (Request::segment(2) == 'rab' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/rab') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            RAB
        </a>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin') || Sentinel::inRole('logistik_kantor'))
	<li {!! (Request::segment(2) == 'purchase-order' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/purchase-order') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            Purchase Order
        </a>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin') || Sentinel::inRole('logistik_lapangan'))
	<li {!! (Request::segment(2) == 'delivery-receipt' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/delivery-receipt') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            Surat Jalan
        </a>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin'))
	<li {!! (Request::segment(2) == 'service-order' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/service-order') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            SPK Subkon
        </a>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin') || Sentinel::inRole('kasir'))
	<li {!! (Request::segment(2) == 'cash-expense' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/cash-expense') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            Bukti Kas
        </a>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin') || Sentinel::inRole('kasir') || Sentinel::inRole('logistik_kantor') || Sentinel::inRole('finance'))
	<li {!! (Request::segment(2) == 'invoice' ? 'class="active"' : '') !!} >
        <a href="#">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            <span class="title">Invoice</span>
            <span class="fa arrow"></span>
        </a>
        <ul class="sub-menu">
            <li {!! (Request::segment(3) == 'purchase-order' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/invoice/purchase-order') }}">
                    <i class="fa fa-angle-double-right"></i>
                    Purchase Order
                </a>
            </li>
			<li {!! (Request::segment(3) == 'service-order' ? 'class="active"' : '') !!}>
                <a href="{{ URL::to('admin/invoice/service-order') }}">
                    <i class="fa fa-angle-double-right"></i>
                    SPK Subkon
                </a>
            </li>
        </ul>
    </li>
	@endif
	
	@if(Sentinel::inRole('admin'))
	<li {!! (Request::segment(2) == 'users' ? 'class="active"' : '') !!}>
        <a href="{{ URL::to('admin/users') }}">
            <i class="livicon" data-c="#EF6F6C" data-hc="#EF6F6C" data-name="list-ul" data-size="18"
               data-loop="true"></i>
            Users
        </a>
    </li>
	@endif
	<!-- END -->
</ul>
