{!! Form::open(array(URL::to('admin/cash-expense') . '/' . $cashExpense->id.'/print', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">Print Cash Expense</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
		<div class="form-group">
			<div class="col-md-12">
				<div class="col-md-12">
					Dibayar Dengan
					<div class="input-group" style="padding-top:5px;">
						{!! Form::radio('dibayar', 'Cash', true, array('class' => 'custom-radio')) !!} Cash&nbsp;&nbsp;&nbsp;&nbsp;
						{!! Form::radio('dibayar', 'Cheque', false, array('class' => 'custom-radio')) !!} Cheque
						&nbsp;&nbsp;&nbsp;&nbsp;
						{!! Form::radio('dibayar', 'Bilyet Giro', false, array('class' => 'custom-radio')) !!} Bilyet Giro
					</div>
				</div>
			</div>
		</div>
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  @if(!$error)
	<button type="submit" class="btn btn-success">Print</button>
  @endif
</div>
{!! Form::close() !!}