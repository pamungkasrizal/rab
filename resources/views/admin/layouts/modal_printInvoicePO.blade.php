{!! Form::open(array(URL::to('admin/invoice/purchase-order') . '/' . $invoicePurchaseOrder->id.'/print', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">Print Invoice Purchase Order</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
		<div class="form-group">
			<div class="col-md-12">
				<div class="col-md-6">
					No. Faktur/Kwitansi/Nota/Order
					<div class="input-group">
						<input type="text" name="no" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Jenis Tagihan
					<div class="input-group">
						<input type="text" name="jnsTagihan" class="form-control" />
					</div>
				</div>
			</div>
		</div>
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
  @if(!$error)
	<button type="submit" class="btn btn-success">Print</button>
  @endif
</div>
{!! Form::close() !!}