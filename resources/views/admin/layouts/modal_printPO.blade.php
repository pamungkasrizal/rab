{!! Form::open(array(URL::to('admin/purchase-order') . '/' . $purchaseOrder->id.'/print', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="user_delete_confirm_title">Print Purchase Order</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
		<div class="form-group">
			<div class="col-md-12">
				<div class="col-md-6">
					Penerima PO 1
					<div class="input-group">
						<input type="text" name="penerima1" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Penerima PO 2
					<div class="input-group">
						<input type="text" name="penerima2" class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					Nama Logistik
					<div class="input-group">
						<input type="text" name="logistik" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Nama PM
					<div class="input-group">
						<input type="text" name="pm" class="form-control" />
					</div>
				</div>
			</div>
			<div class="col-md-12" style="padding-top:20px;">
				<div class="col-md-6">
					Nama UP
					<div class="input-group">
						<input type="text" name="up" class="form-control" />
					</div>
				</div>
				<div class="col-md-6">
					Alamat Kantor & No NPWP
					<div class="input-group" style="padding-top:5px;">
						{!! Form::radio('rad', 'lama', true, array('class' => 'custom-radio')) !!} Lama&nbsp;&nbsp;&nbsp;&nbsp;
						{!! Form::radio('rad', 'baru', false, array('class' => 'custom-radio')) !!} Baru
					</div>
				</div>
			</div>
		</div>
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">@lang($model.'/modal.cancel')</button>
  @if(!$error)
	<button type="submit" class="btn btn-success">Print</button>
  @endif
</div>
{!! Form::close() !!}