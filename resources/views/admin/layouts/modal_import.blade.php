{!! Form::open(array(URL::to('admin/master/import') . '/' . $type.'', 'method' => 'post', 'class' => 'form-horizontal', 'enctype'=>'multipart/form-data', 'files'=> true)) !!}
<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
<div class="modal-header">
  <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
  <h4 class="modal-title" id="import_modal_confirm_title">Import Data {{ ucwords(str_replace('-', ' ', $type))  }} To Database</h4>
</div>
<div class="modal-body">
    @if($error)
        <div>{!! $error !!}</div>
    @else
        File Upload
		<div class="form-group">
			<div class="col-md-12">
				<div class="fileinput fileinput-new input-group" data-provides="fileinput">
					<div class="form-control" data-trigger="fileinput">
						<i class="glyphicon glyphicon-file fileinput-exists"></i>
						<!--<span class="fileinput-filename"></span>-->
					</div>
					<span class="input-group-addon btn btn-default btn-file">
						<span class="fileinput-new">Select file</span>
						<span class="fileinput-exists">Change</span>
						<input type="hidden" name="type_name" value="{{ $type }}">
						<input type="file" name="name_file">
					</span>
					<a href="#" class="input-group-addon btn btn-default fileinput-exists" data-dismiss="fileinput">Remove</a>
				</div>
			</div>
		</div>
		<div class="form-group">
			<div class="col-md-12 fileinput-filename-new">
			
			</div>
		</div>
    @endif
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">@lang($model.'/modal.cancel')</button>
  @if(!$error)
	<button type="submit" class="btn btn-success">Save</button>
  @endif
</div>
<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
<script src="{{ asset('assets/js/pages/form_examples.js') }}"></script>
{!! Form::close() !!}