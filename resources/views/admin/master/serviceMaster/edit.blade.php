@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('masterdata/serviceMaster/title.edit') @lang('masterdata/serviceMaster/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('masterdata/serviceMaster/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
			<a href="{{ route('masters') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/serviceMaster/title.groups')
            </a>
		</li>
		<li>
			<a href="{{ route('serviceMaster') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/serviceMaster/title.title')
            </a>
		</li>
        <li class="active">
            @lang('masterdata/serviceMaster/title.edit')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('masterdata/serviceMaster/title.edit') @lang('masterdata/serviceMaster/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($masterData, array('url' => URL::to('admin/master/service-master') . '/' . $masterData->id.'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'serviceMaster') !!}
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/serviceMaster/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', null, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                    </div>
					
					<div class="form-group {{ $errors->first('name', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/serviceMaster/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('name', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					
					<div class="form-group {{ $errors->first('service_type', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/serviceMaster/form.type')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('service_type', $servicetype, old('service_type',$masterData->service_type), array('class' => 'form-control ajaxServiceType', 'placeholder'=>trans('masterdata/serviceMaster/form.select-category'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('service_type', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/master/service-master') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.update')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
<script>
$('document').ready(function(){
	$(".select2").select2({
		theme:"bootstrap",
		placeholder:"select a value"
	});
	
	$('.ajaxServiceType').select2({
		theme:"bootstrap",
		placeholder: 'Select an item',
		minimumInputLength: 1,
		ajax: {
			dataType: 'json',
			url: '{{ route("serviceTypeDataSearchAjax") }}',
			delay: 250,
			data: function(params) {
				return {
					term: params.term
				}
			},
			processResults: function (data, page) {
				return {
					results: data
				};
			},
		}
	});
});
</script>
<!-- end of page level js -->
@stop