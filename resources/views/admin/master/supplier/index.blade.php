@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('masterdata/supplier/title.list') :: @parent
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
	<style>
		#table_wrapper .row {
			padding: 0 15px !important;
		}
	</style>
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('masterdata/supplier/title.groups')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
        <li>
			<a href="{{ route('masters') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/supplier/title.groups')
            </a>
		</li>
		<li>@lang('masterdata/supplier/title.title')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('masterdata/supplier/title.list')
                </h3>
				<div class="btn-group pull-right">
					<a href="supplier/create" class="btn btn-xs btn-primary gm-preview">
						<span class="glyphicon glyphicon-plus"></span> Add New
					</a>
					<div class="dropdown pull-left gm-layout-mode">
						<button type="button" class="btn  btn-xs  btn-primary dropdown-toggle" title="Import Supplier" data-toggle="dropdown" aria-expanded="false">
							<span class="fa fa-fw fa-cloud-upload"></span> Import File
							<span class="caret"></span>
						</button>
						<ul class="dropdown-menu" role="menu">
							<li>
								<a href="{{ route('import/importType', 'supplier') }}"  data-toggle="modal" data-target="#import_modal" title="import data" class="gm-save">
								<span class="fa fa-fw fa-cloud-upload" style="margin-left: -2px;"></span> Import Data
								</a>
							</li>
							<li>
								<a title="Excel File" href="download-template/supplier" class="gm-save"><span class="glyphicon glyphicon-cloud-download"></span> Download Template</a>
							</li>
						</ul>
					</div> 
					<button type="button" class="btn  btn-xs  btn-primary dropdown-toggle" title="Export Supplier" data-toggle="dropdown" aria-expanded="false">
						Export File
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a title="CSV File" href="supplier/csv" class="gm-save"><span class="glyphicon glyphicon-download-alt"></span> CSV</a>
						</li>
						<li>
							<a title="Excel File" href="supplier/xlsx" class="gm-save"><span class="glyphicon glyphicon-cloud-download"></span> Excel</a>
						</li>
						<li>
							<a title="PDF File" href="supplier/pdf" class="gm-save"><span class="glyphicon glyphicon-save"></span> PDF</a>
						</li>
					</ul>
				</div>
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
							<th><input name="select_all" value="1" id="select-all" type="checkbox"></th>
                            <th>ID</th>
                            <th>Supplier Name</th>
                            <th>Email</th>
                            <th>Fax</th>
                            <th>Telepon</th>
                            <th>Created At</th>
                            <th>Updated By</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
					
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            var table = $('#table').DataTable({
				processing: true,
				serverSide: true,
				pageLength: 10,
				ajax: '{!! route('master.supplier.data') !!}',
				columns: [
					{ data: 'checkbox', name: 'checkbox', "orderable": false },
					{ data: 'id', name: 'id' },
					{ data: 'supplier_name', name: 'supplier_name' },
					{ data: 'email', name: 'email' },
					{ data: 'fax', name: 'fax' },
					{ data: 'telepon', name: 'telepon' },
					{ data: 'created_at', name: 'created_at' },
					{ data: 'first_name', name: 'user'},
					{ data: 'actions', name: 'actions', orderable: false, searchable: false }
				]
			});
			table.on( 'draw', function () {
				$('.livicon').each(function(){
					$(this).updateLivicon();
				});
			} );
			$('#select-all').click(function(e){
				var table= $(e.target).closest('table');
				$('td input:checkbox',table).prop('checked',this.checked);
			});
			
			$('<button class="btn btn-danger" id="deleteAll" style="margin-right: 10px;">Delete Data</button><a href="" data-toggle="modal" data-target="#delete_confirm" style="display: none;" id="addLink"></a>').insertBefore('div.dataTables_length label');
			
			$('#deleteAll').click(function(e){
				var checkValues = $('input[name=idList]:checked').map(function(){
					return $(this).val();
				}).get().join('-');
				if(checkValues.length > 0){
					$("#addLink").attr('href', window.location.href+'/'+checkValues+'/confirm-delete');
					$("#addLink").click();
				}else{
					alert('Pilih 1 atau lebih data yang akan di hapus');
				}
			});
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<div class="modal fade" id="import_modal" tabindex="-1" role="dialog" aria-labelledby="import_modal_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop