@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('masterdata/inventory/title.edit') @lang('masterdata/inventory/title.title') :: @parent
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('masterdata/inventory/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
			<a href="{{ route('masters') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/inventory/title.groups')
            </a>
		</li>
		<li>
			<a href="{{ route('inventory') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/inventory/title.title')
            </a>
		</li>
        <li class="active">
            @lang('masterdata/inventory/title.edit')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('masterdata/inventory/title.edit') @lang('masterdata/inventory/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($masterData, array('url' => URL::to('admin/master/inventory') . '/' . $masterData->id.'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'inventory') !!}
                    <div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/inventory/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', null, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                    </div>
					
					<div class="form-group {{ $errors->first('name', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/inventory/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('name', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					
					<div class="form-group {{ $errors->first('unit', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/inventory/form.unit')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('inventory_unit', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('inventory_unit', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/master/inventory') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.update')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop
