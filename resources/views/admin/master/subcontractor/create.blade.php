@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('masterdata/subcontractor/title.create') @lang('masterdata/subcontractor/title.title') :: @parent
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('masterdata/subcontractor/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
        <li>
			<a href="{{ route('masters') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/subcontractor/title.groups')
            </a>
		</li>
		<li>
			<a href="{{ route('subcontractor') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('masterdata/subcontractor/title.title')
            </a>
		</li>
        <li class="active">
            @lang('masterdata/subcontractor/title.create')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('masterdata/subcontractor/title.create') @lang('masterdata/subcontractor/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/master/subcontractor/create'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'subcontractor') !!}
                    <div class="form-group {{ $errors->first('subcontractor_name', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('masterdata/subcontractor/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('subcontractor_name', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('subcontractor_name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/master/subcontractor') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop
