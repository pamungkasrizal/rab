@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('InvoiceServiceOrder/title.edit') @lang('InvoiceServiceOrder/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/all.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/iCheck/css/line/line.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-switch/css/bootstrap-switch.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/switchery/css/switchery.css') }}"/>
	<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/awesomeBootstrapCheckbox/awesome-bootstrap-checkbox.css') }}"/>
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('InvoiceServiceOrder/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
		<li>
			<a href="{{ route('serviceOrder') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('InvoiceServiceOrder/title.title')
            </a>
		</li>
        <li class="active">
            @lang('InvoiceServiceOrder/title.edit')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('InvoiceServiceOrder/title.edit') @lang('InvoiceServiceOrder/title.title')
                    </h4>
                </div>
                <div class="panel-body">
						{!! Form::hidden('rulesValidation', 'invoiceServiceOrder') !!}
						{!! Form::model($invoiceServiceOrder, array('url' => URL::to('admin/invoice/service-order') . '/' . $invoiceServiceOrder->id.'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
							{!! Form::hidden('id', $invoiceServiceOrder->id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
					
					@if($invoiceServiceOrder->status == '2')
					<div class="alert alert-danger alert-dismissable margin5">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Reject Messages:</strong> {{ $invoiceServiceOrder->status_msg }}
					</div>
					@endif					
					<div class="form-group {{ $errors->first('invoice_number', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('InvoiceServiceOrder/form.drid')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('invoice_number', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
						<div class="col-sm-4">
                            {!! $errors->first('invoice_number', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('service_order_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label changeLabel">
                            @lang('InvoiceServiceOrder/form.poid')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('service_order_id', $po, old('service_order_id',$invoiceServiceOrder->service_order_id), array('class' => 'form-control ajaxSPK', 'disabled'=>'true', 'placeholder'=>trans('invoiceServiceOrder/form.select-po'))) !!}
							<!--<select class="ajaxMaster form-control" name="itemName">
							</select>-->
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('service_order_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<span id="onChangePO">
						<!-- onChange PO -->
					</span>
					<div class="form-group {{ $errors->first('notes', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('InvoiceServiceOrder/form.notes')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('notes', null, array('class' => 'form-control', 'placeholder'=>'', 'rows'=>'3')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('notes', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            Kuitansi PO Item
                        </label>
					</div>
					<div class="form-group">
                        <div class="col-sm-12">
                            <div id="expandible-output">
								<table class="table table-bordered table-hover" id="poItemTable">
									<thead>
										<tr>
											<th width="3%"></th>
											<th width="34%">SPK Item ID / Master Data</th>
											<th width="8%">Progress</th>
											<th width="18%">Pembayaran Sebelumnya</th>
											<th width="14%">Total Price SPK Item</th>
											<th width="18%">Total Price</th>
											@if(Sentinel::inRole('admin')) <th width="5%"></th> @endif
										</tr>
									</thead>
									<tbody>
										@if(Sentinel::inRole('admin'))
										<tr>
											<td><input type="checkbox" class="flat-green checkbox" value="1"/></td>
											<td>
												{!! Form::select('purchase_order', $po, null, array('id' => 'purchase_order', 'class' => 'form-control ajaxSPKItem', 'placeholder'=>trans('invoiceServiceOrder/form.select-po'))) !!}
											</td>
											<td>
												{!! Form::text('progress', null, array('id' => 'progress', 'disabled' => 'disabled', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => '', 'data-a-dec' => '.')) !!}
												{!! Form::hidden('retensi', null, array('id' => 'retensi', 'disabled' => 'disabled', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'value'=>'', 'style' => 'text-align: right;', 'data-a-sep' => '', 'data-a-dec' => '.')) !!}
											</td>
											<td>
												{!! Form::text('pemb_sblm', null, array('id' => 'pemb_sblm', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ', 'readonly'=>'true')) !!}
											</td>
											<td>
												{!! Form::text('price', null, array('id' => 'price', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'readonly'=>'true', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
											</td>
											<td>
												{!! Form::text('total', null, array('id' => 'total', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'readonly'=>'true', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
											</td>
											<td>
												<button type="button" id="addPOItem" class="btn btn-responsive btn-primary btn-sm" style="margin-top: 2px;">Add</button>
											</td>
										</tr>
										@endif
										@if(!empty($invoiceServiceOrderItem))
											@foreach ($invoiceServiceOrderItem as $key => $data)
												<tr>
													<td><input type="checkbox" disabled {!! (($data->retention == '1') ? 'checked' : '') !!}></td>
													<td>
														<input type="hidden" name="invoice_service_order_item[{{ $key }}][id]" value="{{ $data->id }}">
														<input type="hidden" name="invoice_service_order_item[{{ $key }}][service_order_item_id]" value="{{ $data->service_order_item_id }}" class="purchase_order_id">
														{{ $data->service_order_item_id }}
													</td>
													<td class="text-right">{{ $data->progress }}</td>
													<td class="text-right">{{ $data->quantity }}</td>
													<td class="text-right">{{ $data->quantity }}</td>
													<td class="text-right">{{ number_format($data->total_price,'2','.',',') }}</td>
													@if(Sentinel::inRole('admin'))
													<td>
														<button type="button" class="btn btn-responsive btn-danger btn-sm deletePOItem" style="margin-top: -5px; margin-bottom: -4px !important" title="delete"><i class="fa fa-fw fa-trash-o"></i></button>
													</td>
													@endif
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/invoice/purchase-order') }}">
                                @lang('button.cancel')
                            </a>
							
							@if(Sentinel::inRole('admin'))
								<button type="submit" class="btn btn-success">
									@lang('button.edit')
								</button>
							@endif
							
							@if(Sentinel::inRole('admin') || Sentinel::inRole('logistik_kantor') || Sentinel::inRole('finance'))
								<a href="{{ route('confirm-approve/invoiceServiceOrder', $invoiceServiceOrder->id) }}" class="btn btn-success" data-toggle="modal" data-target="#confirm" title="approve data">
								   Approve
								</a>
								<a href="{{ route('confirm-reject/invoiceServiceOrder', $invoiceServiceOrder->id) }}" class="btn btn-primary" data-toggle="modal" data-target="#confirm" title="reject data">
								   Reject
								</a>
							@endif
							
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
	<!-- begining of page level js -->
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
	<script src="{{ asset('assets/vendors/autoNumeric.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
	<script src="{{ asset('assets/js/pages/form_examples.js') }}"></script>
	<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
	<script language="javascript" type="text/javascript" src="{{ asset('assets/js/pages/radio_checkbox.js') }}"></script>
	<script>
	$(function($) {
		$('body').on('hidden.bs.modal', '.modal', function () {
			$(this).removeData('bs.modal');
			$('.modal-content').html('');
		});
	
		$(".select2").select2({
			theme:"bootstrap",
			placeholder:"select a value"
		});
		
		$('.ajaxSPK').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("serviceOrderDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		var $checkbox = false;
		
		var $term = $('select[name=service_order_id]').val(),
			$token = '{{ csrf_token() }}';
		$.ajax({
			dataType: 'json',
			url: '{{ route("serviceOrderItemChangeAjax") }}',
			data: { '_token': $token, 'term': $term },
			success: function (response) {
				$("#onChangePO").html('');
				if(response.length > 0){
					$.each(response, function(i, item){
						$('#pemb_sblm').autoNumeric('set',item.jumlah);
						$('#price').autoNumeric('set',item.jml_total_onitem);
						$retensi = item.vat/100;
						$('#retensi').val($retensi);
						var $row = $('<div class="form-group" style="margin-bottom: 0px !important;">'+
										'<label for="title" class="col-sm-2 control-label"></label>'+
										'<div class="col-md-8">'+
											'<div class="box well well-sm">'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'RAB Name'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.rab_name+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Daftar Subkon'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.subcontractor_name+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Retensi'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.vat +
													' %</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Start Date'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.start_date+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Notes'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.notes+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Term of Payment'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.term_of_payment+
													' Hari</label>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>');
						$("#onChangePO").html($row);
						ifUnchecked();
					});
					$('.autoNumericx').autoNumeric('init');
				}else{
					$("table>tbody").html('<div class="form-group">'+
												'<label for="title" class="col-sm-4 control-label">'+
													'DATA TIDAK TERSEDIA'+
												'</label>'+
											'</div>');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.responseText);
			}
		});
		
		$('.autoNumeric').autoNumeric('init');
		
		var whereNotIn = [];
		@if(!empty($invoiceServiceOrderItem))
			@foreach ($invoiceServiceOrderItem as $key => $data)
				whereNotIn.push('{{ $data->service_order_item_id }}');
			@endforeach
		@endif

		$('.ajaxSPKItem').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("purchaseOrderItemDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
						poId: $('.ajaxSPK').val(),
						whereNotIn: (whereNotIn.length > 0) ? JSON.stringify(whereNotIn) : ''
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		var $no = parseInt('{{ count($invoiceServiceOrderItem)+2 }}');
		$("#addPOItem").click(function(){
			var $purchase_order_id = $('#purchase_order option:selected').val(),
				$purchase_order = $('#purchase_order option:selected').text(),
				$progress = $('#progress').val(),
				$price = $('#price').val(),
				$retention = $('input[type="checkbox"].checkbox').val(),
				$price1 = $('#price').autoNumeric('get'),
				$pemb_sblm1 = $('#pemb_sblm').autoNumeric('get'),
				$total = $('#total').val(),
				$total1 = $('#total').autoNumeric('get'),
				$valDR = $purchase_order_id.trim().length,
				$valQty = $progress.trim().length,
				$chk = ($valQty > 0) ? '<input type="checkbox" checked disabled /><input type="hidden" name="invoice_service_order_item['+$no+'][retention]" value="1"/>' : '<input type="checkbox" disabled /><input type="hidden" name="invoice_service_order_item['+$no+'][retention]" value="0"/>';
				
			($valDR < 1) ? $('.select2-selection--single').css('border-color','#EF6F6C') : $('.select2-selection--single').css('border-color','');
			($valQty < 1 && $retention != 1) ? $('#progress').css('border-color','#EF6F6C') : $('#progress').css('border-color','');
			
			if($valDR < 1 || $valQty < 1 && $retention != 1) return false;
			whereNotIn.push($purchase_order_id);
			
			var $row = $('<tr>'+
							'<td>'+$chk+'</td>'+
							'<td><input type="hidden" name="invoice_service_order_item['+$no+'][service_order_item_id]" value="'+$purchase_order_id+'" class="purchase_order_id">'+$purchase_order+'</td>'+
							'<td class="text-right"><input type="hidden" name="invoice_service_order_item['+$no+'][progress]" value="'+$progress+'">'+$progress+'</td>'+
							'<td class="text-right">'+$('#pemb_sblm').val()+'</td>'+
							'<td class="text-right">'+$price+'</td>'+
							'<td class="text-right"><input type="hidden" name="invoice_service_order_item['+$no+'][total_price]" value="'+$total1+'">'+$total+'</td>'+
							'<td><button type="button" class="btn btn-responsive btn-danger btn-sm deletePOItem" style="margin-top: -5px; margin-bottom: -4px !important" title="delete"><i class="fa fa-fw fa-trash-o"></i></button></td>'+
						'</tr>');
						
			$("table>tbody").append($row);
			$no = parseInt($no) + 1;
			$('table select').val('');
			$('table #select2-purchase_order-container').html('');
		});
		
		$("table>tbody").on('click', 'button.deletePOItem', function(){
			var purchase_order_id = $(this).closest('tr').find('input.purchase_order_id').val();
			whereNotIn = whereNotIn.filter(x=>x != purchase_order_id);
			$(this).closest('tr').remove();
		});
		
		$('select[name=purchase_order_id], input[name=invoice_number]').prop('disabled', true);
		if("{{ Sentinel::inRole('admin') }}" == ''){
			$('input, select, textarea').prop('disabled', true);
		}
		
		$('input[type="checkbox"].checkbox').on('ifChecked', function(e){
			$('#progress').removeAttr('disabled');
			$('#progress').autoNumeric('set',0);
			var $checkbox = true;
			ifChecked();
		});
		
		$('input[type="checkbox"].checkbox').on('ifUnchecked', function(e){
			$('#progress').attr('disabled', 'disabled');
			$('#progress').val('');
			var $checkbox = false;
			ifUnchecked();
		});
		
		$('input[name=progress]').keyup(function(){
			ifChecked();
		});
	});
	
	function ifUnchecked(){
		var $pembayaran_sebelum = $('input[name=pemb_sblm]').autoNumeric('get') !='' ? $('input[name=pemb_sblm]').autoNumeric('get') : 0,
			$jml_total_onitem = $('input[name=price]').autoNumeric('get') !='' ? $('input[name=price]').autoNumeric('get') : 0,
			$total = ($jml_total_onitem - $pembayaran_sebelum);
		$('#total').autoNumeric('set', $total);
	}
	
	function ifChecked(){
		var	$pembayaran_sebelum = $('input[name=pemb_sblm]').autoNumeric('get') !='' ? $('input[name=pemb_sblm]').autoNumeric('get') : 0,
			$jml_total_onitem = $('input[name=price]').autoNumeric('get') !='' ? $('input[name=price]').autoNumeric('get') : 0,
			$progress = $('#progress').val(),
			$retensi = $('#retensi').val(),
			$total = (($jml_total_onitem * (1 - $retensi) * $progress) - $pembayaran_sebelum);
		$('#total').autoNumeric('set', $total);
	}
	</script>
	<!-- end of page level js -->
@stop
