@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    Add User
    @parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css -->
    <link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" type="text/css" rel="stylesheet">
    <link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet">
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/pages/wizard.css') }}" rel="stylesheet">
    <!--end of page level css-->
@stop


{{-- Page content --}}
@section('content')
    <section class="content-header">
        <h1>Add New User</h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ route('dashboard') }}">
                    <i class="livicon" data-name="home" data-size="14" data-color="#000"></i>
                    Dashboard
                </a>
            </li>
            <li><a href="#"> Users</a></li>
            <li class="active">Add New User</li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="livicon" data-name="user-add" data-size="18" data-c="#fff" data-hc="#fff" data-loop="true"></i>
                            Add New User
                        </h3>
                                <span class="pull-right clickable">
                                    <i class="glyphicon glyphicon-chevron-up"></i>
                                </span>
                    </div>
                    <div class="panel-body">
						{!! Form::open(array('url' => URL::to('admin/users/create'), 'method' => 'post', 'id' => 'commentForm', 'class' => 'form-horizontal', 'files'=> true)) !!}
							{!! Form::hidden('_token', csrf_token() ) !!}
						
						<div class="form-group {{ $errors->first('first_name', 'has-error') }}">
							<label for="title" class="col-sm-2 control-label">
								First Name *
							</label>
							<div class="col-sm-5">
								{!! Form::text('first_name', old('first_name'), array('class' => 'form-control required', 'id' => 'first_name', 'placeholder'=>'First Name')) !!}
							</div>
							<div class="col-sm-4">
								{!! $errors->first('first_name', '<span class="help-block">:message</span> ') !!}
							</div>
						</div>
						
						<div class="form-group {{ $errors->first('last_name', 'has-error') }}">
							<label for="title" class="col-sm-2 control-label">
								Last Name *
							</label>
							<div class="col-sm-5">
								{!! Form::text('last_name', old('first_name'), array('class' => 'form-control required', 'id' => 'last_name', 'placeholder'=>'Last Name')) !!}
							</div>
							<div class="col-sm-4">
								{!! $errors->first('last_name', '<span class="help-block">:message</span> ') !!}
							</div>
						</div>
						
						<div class="form-group {{ $errors->first('email', 'has-error') }}">
							<label for="title" class="col-sm-2 control-label">
								Email *
							</label>
							<div class="col-sm-5">
								{!! Form::text('email', old('email'), array('class' => 'form-control required email', 'id' => 'email', 'placeholder'=>'Email')) !!}
							</div>
							<div class="col-sm-4">
								{!! $errors->first('email', '<span class="help-block">:message</span> ') !!}
							</div>
						</div>
						
						<div class="form-group {{ $errors->first('group', 'has-error') }}">
							<label for="title" class="col-sm-2 control-label">
								Group *
							</label>
							<div class="col-sm-5">
								<select class="form-control required" title="Select group..." name="group" id="group">
									<option value="">Select</option>
									@foreach($groups as $group)
										<option value="{{ $group->id }}" @if($group->id == old('group')) selected="selected" @endif >
											{{ $group->name}}
										</option>
									@endforeach
								</select>
							</div>
							<div class="col-sm-4">
								{!! $errors->first('group', '<span class="help-block">:message</span>') !!}
							</div>
						</div>
						
						<div class="form-group {{ $errors->first('password', 'has-error') }}">
							<label for="title" class="col-sm-2 control-label">
								Password *
							</label>
							<div class="col-sm-5">
								<input id="password" name="password" placeholder="Password" class="form-control" value="{{old('password')}}" data-bv-field="password" type="password">
							</div>
							<div class="col-sm-4">
								{!! $errors->first('password', '<span class="help-block">:message</span> ') !!}
							</div>
						</div>
						
						<div class="form-group {{ $errors->first('password_confirm', 'has-error') }}">
							<label for="title" class="col-sm-2 control-label">
								Confirm Password *
							</label>
							<div class="col-sm-5">
								<input id="password_confirm" name="password_confirm" placeholder="Confirm Password" class="form-control" value="{{old('password_confirm')}}" data-bv-field="password_confirm" type="password">
							</div>
							<div class="col-sm-4">
								{!! $errors->first('password_confirm', '<span class="help-block">:message</span> ') !!}
							</div>
						</div>
						
						<div class="form-group">
							<label for="activate" class="col-sm-2 control-label"> Activate User *</label>
							<div class="col-sm-10">
								<input id="activate" name="activate" type="checkbox"
									   class="pos-rel p-l-30 custom-checkbox"
									   value="1" @if(old('activate')) checked="checked" @endif >
								<span>To activate your account click the check box</span></div>

						</div>

						<div class="form-group">
							<div class="col-sm-offset-2 col-sm-4">
								<a class="btn btn-danger" href="{{ URL::to('admin/users') }}">
									@lang('button.cancel')
								</a>
								<button type="submit" class="btn btn-success">
									@lang('button.save')
								</button>
							</div>
						</div>
						{!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!--row end-->
    </section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
    <script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" ></script>
    <script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}"  type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/select2/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapwizard/jquery.bootstrap.wizard.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/pages/adduser.js') }}"></script>
	<script>
		$('document').ready(function(){
			$("select").select2({
				theme:"bootstrap",
				placeholder:"select a value"
			});
		});
	</script>
@stop