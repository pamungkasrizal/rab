@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('purchaseOrderItem/title.create') @lang('purchaseOrderItem/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('purchaseOrderItem/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
		<li>
			<a href="{{ route('purchaseOrderItems') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('purchaseOrderItem/title.title')
            </a>
		</li>
        <li class="active">
            @lang('purchaseOrderItem/title.create')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('purchaseOrderItem/title.create') @lang('purchaseOrderItem/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/purchase-order-item/create'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'purchaseOrderItem') !!}
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('purchaseOrderItem/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', $id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('purchase_order_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('purchaseOrderItem/form.purchase-order')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('purchase_order_id', $purchaseOrder, null, array('class' => 'form-control ajaxPurchaseOrder', 'placeholder'=>trans('purchaseOrderItem/form.select-purchase-order'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('purchase_order_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('master_data_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('purchaseOrderItem/form.master_data')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('master_data_id', $master_data, null, array('class' => 'form-control ajaxMaster', 'placeholder'=>trans('purchaseOrderItem/form.select-master_data'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('master_data_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->first('quantity', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('purchaseOrderItem/form.qty')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('quantity', null, array('class' => 'form-control autoNumeric', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => '', 'data-v-max' => '9999', 'data-v-min' => '0')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('quantity', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('unit_price', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('purchaseOrderItem/form.price')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('unit_price', null, array('class' => 'form-control autoNumeric', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('unit_price', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('unit_total_price', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('purchaseOrderItem/form.total_price')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('unit_total_price', null, array('class' => 'form-control autoNumeric', 'placeholder'=>'', 'readonly'=>'true', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('unit_total_price', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/purchase-order-item') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
	<!-- begining of page level js -->
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
	<script src="{{ asset('assets/vendors/autoNumeric.js') }}" type="text/javascript"></script>
	<script>
	$(function($) {
		$(".select2").select2({
			theme:"bootstrap",
			placeholder:"select a value"
		});
		
		$('.autoNumeric').autoNumeric('init');
		
		$('input[name=unit_price]').keyup(function(){
			var unit_price = $(this).autoNumeric('get') !='' ? $(this).autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $('input[name=quantity]').val();
			$('input[name=unit_total_price]').autoNumeric('set',unit_total_price);
		});
		
		$('input[name=quantity]').keyup(function(){
			var unit_price = $('input[name=unit_price]').autoNumeric('get') !='' ? $('input[name=unit_price]').autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $(this).val();
			$('input[name=unit_total_price]').autoNumeric('set',unit_total_price);
		});
		
		$('.ajaxMaster').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("masterDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		$('.ajaxPurchaseOrder').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("purchaseOrderDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
	});
	</script>
	<!-- end of page level js -->
@stop
