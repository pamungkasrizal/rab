@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('purchaseOrderItem/title.list') :: @parent
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
	<style>
		#table_wrapper .row {
			padding: 0 15px !important;
		}
	</style>
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('purchaseOrderItem/title.groups')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
		<li>@lang('purchaseOrderItem/title.title')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('purchaseOrderItem/title.list')
                </h3>
				<div class="btn-group pull-right">
					<a href="purchase-order-item/create" class="btn btn-xs btn-primary gm-preview">
						<span class="glyphicon glyphicon-plus"></span> Add New
					</a>
					<button type="button" class="btn  btn-xs  btn-primary dropdown-toggle" title="Add New Service Type" data-toggle="dropdown" aria-expanded="false">
						Export File
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a title="CSV File" href="purchase-order-item/csv" class="gm-save"><span class="glyphicon glyphicon-download-alt"></span> CSV</a>
						</li>
						<li>
							<a title="Excel File" href="purchase-order-item/xlsx" class="gm-save"><span class="glyphicon glyphicon-cloud-download"></span> Excel</a>
						</li>
						<li>
							<a title="PDF File" href="purchase-order-item/pdf" class="gm-save"><span class="glyphicon glyphicon-save"></span> PDF</a>
						</li>
					</ul>
				</div>
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
							<th>Purchase Order ID</th>
							<th>Master Name</th>
                            <th>Qty</th>
                            <th>Price</th>
                            <th>Total Price</th>
                            <th>Created By</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($dataPurchaseOrderItem))
                        @foreach ($dataPurchaseOrderItem as $data)
                            <tr>
                                <td>{{ $data->id }}</td>
								<td>{{ $data->purchase_order_id }}</td>
                                <td>{{ $data->name }}</td>
                                <td class="text-right">{{ $data->quantity }}</td>
                                <td class="text-right">{{ number_format($data->unit_price,'0',',',',') }}</td>
                                <td class="text-right">{{ number_format($data->unit_total_price,'0',',',',') }}</td>
                                <td>{{ $data->first_name }} {{ $data->last_name }}</td>
                                <td>
                                    <a href="{{ URL::to('admin/purchase-order-item/' . $data->id . '/edit' ) }}" title="update data">
										<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update data"></i>
									</a>
                                    <a href="{{ route('confirm-delete/purchaseOrderItem', $data->id) }}" data-toggle="modal" data-target="#delete_confirm" title="delete data">
									   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete data"></i>
									</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            $('#table').DataTable();
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop