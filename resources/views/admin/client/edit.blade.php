@extends('admin/layouts/default')

{{-- Web site Title --}}
@section('title')
	@lang('client/title.edit') @lang('client/title.title') :: @parent
@stop

{{-- Content --}}
@section('content')
<section class="content-header">
    <h1>
        @lang('client/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
		<li>
			<a href="{{ route('clients') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('client/title.title')
            </a>
		</li>
        <li class="active">@lang('client/title.edit')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="wrench" data-size="16" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('client/title.edit') @lang('client/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::model($client, array('url' => URL::to('admin/client') . '/' . $client->id.'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'client') !!}
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('project/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', $client->id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->first('client_name', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('client/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('client_name', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('client_name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('client_address', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('client/form.address')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('client_address', null, array('class' => 'form-control', 'placeholder'=>'', 'rows'=>'3')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('client_address', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group">
						<div class="col-sm-offset-2 col-sm-4">
							<a class="btn btn-danger" href="{{ URL::to('admin/client') }}">
								@lang('button.cancel')
							</a>
							<button type="submit" class="btn btn-success">
								@lang('button.update')
							</button>							
						</div>
					</div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
<script src="{{ asset('assets/js/jquery.min.js') }}"></script>
<script>
	/* $(function($) {
		if("{{ Sentinel::inRole('admin') }}" == '' && "{{ Sentinel::inRole('administrasi') }}" == ''){
			$('input, select, textarea').prop('disabled', true);
		}
	}); */
	</script>
@stop