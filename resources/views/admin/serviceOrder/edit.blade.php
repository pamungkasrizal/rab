@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('serviceOrder/title.edit') @lang('serviceOrder/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('serviceOrder/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
		<li>
			<a href="{{ route('serviceOrders') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('serviceOrder/title.title')
            </a>
		</li>
        <li class="active">
            @lang('serviceOrder/title.edit')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('serviceOrder/title.edit') @lang('serviceOrder/title.title')
                    </h4>
                </div>
                <div class="panel-body">
						{!! Form::hidden('rulesValidation', 'serviceOrder') !!}
						{!! Form::model($serviceOrder, array('url' => URL::to('admin/service-order') . '/' . $serviceOrder->id.'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
					
					@if($serviceOrder->status == '2')
					<div class="alert alert-danger alert-dismissable margin5">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Reject Messages:</strong> {{ $serviceOrder->status_msg }}
					</div>
					@endif
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', $serviceOrder->id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('rab_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.rab')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('rab_id', $rab, old('rab_id',$serviceOrder->rab_id), array('class' => 'form-control ajaxRab', 'placeholder'=>trans('serviceOrder/form.select-rab'))) !!}
							<!--<select class="ajaxMaster form-control" name="itemName">
							</select>-->
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('rab_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('subcontractor_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.subcontractor')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('subcontractor_id',$subcontractor ,old('subcontractor_id',$serviceOrder->subcontractor_id), array('class' => 'form-control ajaxSubcontractor', 'placeholder'=>trans('serviceOrder/form.select-subcontractor'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('subcontractor_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->first('vat', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.vat')
                        </label>
                        <div class="col-sm-4">
							<label class="radio-inline" style="padding-left:0px !important">
								{!! Form::radio('vat', '0', true, array('class' => 'custom-radio')) !!} 0% &nbsp;&nbsp;&nbsp;&nbsp;
								{!! Form::radio('vat', '5', false, array('class' => 'custom-radio')) !!} 5% &nbsp;&nbsp;&nbsp;&nbsp;
								{!! Form::radio('vat', '10', false, array('class' => 'custom-radio')) !!} 10%
							</label>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('vat', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('start_date', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.start_date')
                        </label>
                        <div class="col-sm-2">
							<div class="input-group">
                                <div class="input-group-addon">
                                    <i class="livicon" data-name="calendar" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                                </div>
                                <input type="text" name="start_date" class="form-control" id="rangepicker4s" />
                            </div>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('start_date', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('notes', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.notes')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('notes', null, array('class' => 'form-control', 'placeholder'=>'', 'rows'=>'3')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('notes', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('term_of_payment', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('serviceOrder/form.term')
                        </label>
                        <div class="col-sm-2">
							<div class="input-group">
								{!! Form::text('term_of_payment', null, array('class' => 'form-control autoNumeric', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => '', 'data-v-max' => '9999', 'data-v-min' => '0')) !!}
								<span class="input-group-addon">Hari</span>
							</div>
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('term_of_payment', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            Add SO Item
                        </label>
                    </div>
					<div class="form-group">
                        <div class="col-sm-12">
                            <div id="expandible-output">
								<table class="table table-bordered table-hover" id="poItemTable">
									<thead>
										<tr>
											<th width="45%">RAB Item ID / Master Name</th>
											<th width="10%">Qty</th>
											<th width="20%">Price</th>
											<th width="20%">Total</th>
											@if(Sentinel::inRole('admin'))
												<th width="5%"></th>
											@endif
										</tr>
									</thead>
									@if(Sentinel::inRole('admin'))
									<tbody>
										<tr>
											<td>
												{!! Form::select('master', $master_data, null, array('id' => 'master', 'class' => 'form-control ajaxRabItemMaster', 'placeholder'=>trans('purchaseOrderItem/form.select-master_data'))) !!}
											</td>
											<td>
												{!! Form::text('qty', null, array('id' => 'qty', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-v-max' => '99999999999', 'data-v-min' => '0')) !!}
											</td>
											<td>
												{!! Form::text('price', null, array('id' => 'price', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
											</td>
											<td>
												{!! Form::text('total', null, array('id' => 'total', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'readonly'=>'true', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
											</td>
											<td>
												<button type="button" id="addPOItem" class="btn btn-responsive btn-primary btn-sm" style="margin-top: 2px;">Add</button>
											</td>
										</tr>
										@endif
										
										@if(!empty($purchaseOrderItem))
											@foreach ($purchaseOrderItem as $key => $data)
												<tr>
													<td>
														<input type="hidden" name="purchase_order_item[{{ $key }}][rab_item_id]" value="{{ $data->rab_item_id }}" class="rab_item_id">
														<input type="hidden" name="purchase_order_item[{{ $key }}][id]" value="{{ $data->id }}">
														{{ $data->rab_item_id }} / {{ $data->master_data_name }}
													</td>
													<td class="text-right">{{ $data->quantity }}</td>
													<td class="text-right">{{ number_format($data->unit_price,'2','.',',') }}</td>
													<td class="text-right">{{ number_format($data->unit_total_price,'2','.',',') }}</td>
													
													@if(Sentinel::inRole('admin'))
													<td>
														<button type="button" class="btn btn-responsive btn-danger btn-sm deletePOItem" style="margin-top: -5px; margin-bottom: -4px !important" title="delete"><i class="fa fa-fw fa-trash-o"></i></button>
													</td>
													@endif
													
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/service-order') }}">
                                @lang('button.cancel')
                            </a>
							
							@if(Sentinel::inRole('admin'))
							<button type="submit" class="btn btn-success">
								@lang('button.edit')
							</button>
							@endif
							
							@if(Sentinel::inRole('admin') || Sentinel::inRole('pm'))
							<a href="{{ route('confirm-approve/serviceOrder', $serviceOrder->id) }}" class="btn btn-success" data-toggle="modal" data-target="#confirm" title="approve data">
							   Approve
							</a>
							<a href="{{ route('confirm-reject/serviceOrder', $serviceOrder->id) }}" class="btn btn-primary" data-toggle="modal" data-target="#confirm" title="reject data">
							   Reject
							</a>
							@endif
							
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
	<!-- begining of page level js -->
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
	<script src="{{ asset('assets/vendors/autoNumeric.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
	<script src="{{ asset('assets/js/pages/form_examples.js') }}"></script>
	<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
	<script>
	$(function($) {
		$('body').on('hidden.bs.modal', '.modal', function () {
			$(this).removeData('bs.modal');
			$('.modal-content').html('');
		});
	
		$(".select2").select2({
			theme:"bootstrap",
			placeholder:"select a value"
		});
		
		$('.autoNumeric').autoNumeric('init');
		
		$('.ajaxRab').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("rabDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		$('.ajaxSubcontractor').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("subcontractorDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		
		$("#rangepicker4s").daterangepicker({
			singleDatePicker: true,
			showDropdowns: true,
			startDate: '{{ date("mm/dd/YYYY", strtotime($serviceOrder->start_date)) }}',
		});
		
		$('.ajaxMaster').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("masterDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		$('input[name=price]').keyup(function(){
			var unit_price = $(this).autoNumeric('get') !='' ? $(this).autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $('input[name=qty]').autoNumeric('get');
			$('input[name=total]').autoNumeric('set',unit_total_price);
		});
		
		$('input[name=qty]').keyup(function(){
			var unit_price = $('input[name=price]').autoNumeric('get') !='' ? $('input[name=price]').autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $(this).autoNumeric('get');
			$('input[name=total]').autoNumeric('set',unit_total_price);
		});
		
		var whereNotIn = [];
		@if(!empty($purchaseOrderItem))
			@foreach ($purchaseOrderItem as $key => $data)
				whereNotIn.push('{{ $data->rab_item_id }}');
			@endforeach
		@endif
		
		
		$('.ajaxRabItemMaster').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("rabItemMasterDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
						rabId: $('.ajaxRab').val(),
						masterId: 1,
						whereNotIn: (whereNotIn.length > 0) ? JSON.stringify(whereNotIn) : ''
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		$('input[name=price]').keyup(function(){
			var unit_price = $(this).autoNumeric('get') !='' ? $(this).autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $('input[name=qty]').val();
			$('input[name=total]').autoNumeric('set',unit_total_price);
		});
		
		$('input[name=qty]').keyup(function(){
			var unit_price = $('input[name=price]').autoNumeric('get') !='' ? $('input[name=price]').autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $(this).val();
			$('input[name=total]').autoNumeric('set',unit_total_price);
		});
		
		var $no = parseInt('{{ count($purchaseOrderItem)+2 }}');
		$("#addPOItem").click(function(){
			var $master_id = $('#master option:selected').val(),
				$master = $('#master option:selected').text(),
				$rab_item_id = $master.split(' / '),
				$qty = $('#qty').val(),
				$qty1 = $('#qty').autoNumeric('get'),
				$price = $('#price').val(),
				$price1 = $('#price').autoNumeric('get'),
				$total = $('#total').val(),
				$total1 = $('#total').autoNumeric('get')
				$valMaster = $master_id.trim().length,
				$valQty = $qty.trim().length,
				$valPrice = $price1.trim().length;
				
			($valMaster < 1) ? $('.select2-selection--single').css('border-color','#EF6F6C') : $('.select2-selection--single').css('border-color','');
			($valQty < 1) ? $('#qty').css('border-color','#EF6F6C') : $('#qty').css('border-color','');
			($valPrice < 1) ? $('#price').css('border-color','#EF6F6C') : $('#price').css('border-color','');
			
			if($valMaster < 1 || $valQty < 1 || $valPrice < 1) return false;
			whereNotIn.push($rab_item_id[0]);
			
			var $row = $('<tr>'+
							'<td><input type="hidden" name="purchase_order_item['+$no+'][rab_item_id]" value="'+$rab_item_id[0]+'" class="rab_item_id"><input type="hidden" name="purchase_order_item['+$no+'][master_data_id]" value="'+$master_id+'">'+$master+'</td>'+
							'<td class="text-right"><input type="hidden" name="purchase_order_item['+$no+'][quantity]" value="'+$qty1+'">'+$qty+'</td>'+
							'<td class="text-right"><input type="hidden" name="purchase_order_item['+$no+'][unit_price]" value="'+$price1+'">'+$price+'</td>'+
							'<td class="text-right"><input type="hidden" name="purchase_order_item['+$no+'][unit_total_price]" value="'+$total1+'">'+$total+'</td>'+
							'<td><button type="button" class="btn btn-responsive btn-danger btn-sm deletePOItem" style="margin-top: -5px; margin-bottom: -4px !important" title="delete"><i class="fa fa-fw fa-trash-o"></i></button></td>'+
						'</tr>');
						
			$("table>tbody").append($row);
			$no = parseInt($no) + 1;
			
			$('table select, table input[type=text]').val('');
			$('table #select2-master-container').html('');
		});
		
		$("table>tbody").on('click', 'button.deletePOItem', function(){
			var rab_item_id = $(this).closest('tr').find('input.rab_item_id').val();
			whereNotIn = whereNotIn.filter(x=>x != rab_item_id);
			$(this).closest('tr').remove();
		});
		
		$('select[name=rab_id]').prop('disabled', true);
		
		if("{{ Sentinel::inRole('admin') }}" == ''){
			$('input, select, textarea').prop('disabled', true);
		}
	});
	</script>
	<!-- end of page level js -->
@stop
