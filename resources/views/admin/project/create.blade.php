@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('project/title.create') @lang('project/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
    <link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('project/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
		<li>
			<a href="{{ route('projects') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('project/title.title')
            </a>
		</li>
        <li class="active">
            @lang('project/title.create')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('project/title.create') @lang('project/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/project/create'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'project') !!}
					<div class="form-group {{ $errors->first('id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('project/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', $id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
						<div class="col-sm-8">
                            {!! $errors->first('id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->first('project_name', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('project/form.name')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::text('project_name', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('project_name', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('client_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('project/form.client')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('client_id',$client ,'', array('class' => 'form-control ajaxClient', 'placeholder'=>trans('project/form.select-client'))) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('client_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            Start Date - End Date
                        </label>
                        <div class="col-sm-5">
							<div class="input-group">
                                <div class="input-group-addon">
                                    <i class="livicon" data-name="calendar" data-size="16" data-c="#555555" data-hc="#555555" data-loop="true"></i>
                                </div>
                                <input type="text" name="start_end_date" class="form-control" id="daterange1" />
                            </div>
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('address', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            Address
                        </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('address', null, array('class' => 'form-control', 'placeholder'=>'', 'rows'=>'3')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('address', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/project') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
<!-- begining of page level js -->
<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/js/pages/datepicker.js') }}" type="text/javascript"></script>
<script>
$(function($) {
	$('.ajaxClient').select2({
		theme:"bootstrap",
		placeholder: 'Select an item',
		minimumInputLength: 1,
		ajax: {
			dataType: 'json',
			url: '{{ route("clientDataSearchAjax") }}',
			delay: 250,
			data: function(params) {
				return {
					term: params.term
				}
			},
			processResults: function (data, page) {
				return {
					results: data
				};
			},
		}
	});
});
</script>
<!-- end of page level js -->
@stop