@extends('admin/layouts/default')

{{-- Page title --}}
@section('title')
    @lang('project/title.list') :: @parent
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/datatables/css/dataTables.bootstrap.css') }}" />
    <link href="{{ asset('assets/css/pages/tables.css') }}" rel="stylesheet" type="text/css" />
	<style>
		#table_wrapper .row {
			padding: 0 15px !important;
		}
	</style>
@stop

{{-- Page content --}}
@section('content')
<section class="content-header">
    <h1>@lang('project/title.groups')</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('general.dashboard')
            </a>
        </li>
		<li>@lang('project/title.title')</li>
    </ol>
</section>

<!-- Main content -->
<section class="content paddingleft_right15">
    <div class="row">
        <div class="panel panel-primary ">
            <div class="panel-heading clearfix">
                <h3 class="panel-title pull-left"> <i class="livicon" data-name="list-ul" data-size="18" data-loop="true" data-c="#fff" data-hc="white"></i>
                    @lang('project/title.list')
                </h3>
				<div class="btn-group pull-right">
					<a href="project/create" class="btn btn-xs btn-primary gm-preview">
						<span class="glyphicon glyphicon-plus"></span> Add New
					</a>
					<button type="button" class="btn  btn-xs  btn-primary dropdown-toggle" title="Add New Service Type" data-toggle="dropdown" aria-expanded="false">
						Export File
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li>
							<a title="CSV File" href="project/csv" class="gm-save"><span class="glyphicon glyphicon-download-alt"></span> CSV</a>
						</li>
						<li>
							<a title="Excel File" href="project/xlsx" class="gm-save"><span class="glyphicon glyphicon-cloud-download"></span> Excel</a>
						</li>
						<li>
							<a title="PDF File" href="project/pdf" class="gm-save"><span class="glyphicon glyphicon-save"></span> PDF</a>
						</li>
					</ul>
				</div>
				
            </div>
            <br />
            <div class="panel-body">
                <table class="table table-bordered" id="table">
                    <thead>
                        <tr class="filters">
                            <th>ID</th>
                            <th>Project Name</th>
                            <th>Client</th>
                            <th>Start Date</th>
                            <th>End Date</th>
                            <th>Created At</th>
                            <th>Updated At</th>
                            <th>Updated By</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                    @if(!empty($dataProject))
                        @foreach ($dataProject as $data)
                            <tr>
                                <td>{{ $data->id }}</td>
                                <td>{{ $data->project_name }}</td>
                                <td>{{ $data->client_name }}</td>
                                <td>{{ date('j M, Y', strtotime($data->project_start_date)) }}</td>
                                <td>{{ date('j M, Y', strtotime($data->project_end_date)) }}</td>
                                <td>{{ $data->created_at->toDayDateTimeString() }}</td>
                                <td>{{ $data->updated_at->toDayDateTimeString() }}</td>
                                <td>{{ $data->first_name }} {{ $data->last_name }}</td>
                                <td>
                                    <a href="{{ URL::to('admin/project/' . $data->id . '/edit' ) }}" title="update data">
										<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="update data"></i>
									</a>
                                    <a href="{{ route('confirm-delete/project', $data->id) }}" data-toggle="modal" data-target="#delete_confirm" title="delete data">
									   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954" title="delete data"></i>
									</a>
                                </td>
                            </tr>
                        @endforeach
                    @endif
                    </tbody>
                </table>
            </div>
        </div>
    </div>    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/jquery.dataTables.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/datatables/js/dataTables.bootstrap.js') }}"></script>

    <script>
        $(document).ready(function() {
            var table = $('#table').DataTable({
				processing: true,
				serverSide: true,
				pageLength: 10,
				ajax: "{!! route('project.data') !!}",
				columns: [
					{ data: 'id', name: 'id' },
					{ data: 'project_name', name: 'project_name' },
					{ data: 'client_name', name: 'client_name' },
					{ data: 'project_start_date', name: 'project_start_date' },
					{ data: 'project_end_date', name: 'project_end_date' },
					{ data: 'created_at', name: 'created_at' },
					{ data: 'updated_at', name: 'updated_at' },
					{ data: 'first_name', name: 'user'},
					{ data: 'actions', name: 'actions', orderable: false, searchable: false }
				]
			});
			table.on( 'draw', function () {
				$('.livicon').each(function(){
					$(this).updateLivicon();
				});
			} );
        });
    </script>

<div class="modal fade" id="delete_confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
<script>
$(function () {
	$('body').on('hidden.bs.modal', '.modal', function () {
		$(this).removeData('bs.modal');
	});
});
</script>
@stop