@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('deliveryReceipt/title.create') @lang('deliveryReceipt/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
	<style>
		@media screen and (max-width: 500px) {
		   .mediaCss{
			   width: 100% !important;
		   }
		}
	</style>
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('deliveryReceipt/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
		<li>
			<a href=""> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('deliveryReceipt/title.title')
            </a>
		</li>
        <li class="active">
            @lang('deliveryReceipt/title.create')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('deliveryReceipt/title.create') @lang('deliveryReceipt/title.title')
                    </h4>
                </div>
                <div class="panel-body">
                    {!! Form::open(array('url' => URL::to('admin/delivery-receipt/create'), 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
						{!! Form::hidden('rulesValidation', 'deliveryReceipt') !!}
						{!! Form::hidden('id', $id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
					<!--<div class="form-group">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('deliveryReceipt/form.id')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('id', $id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
                    </div>-->
					<div class="form-group {{ $errors->first('delivery_receipt_number', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('deliveryReceipt/form.drid')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('delivery_receipt_number', '', array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
						<div class="col-sm-4">
                            {!! $errors->first('delivery_receipt_number', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('purchase_order_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label changeLabel">
                            @lang('deliveryReceipt/form.poid')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::select('purchase_order_id', $po, null, array('class' => 'form-control ajaxPO', 'placeholder'=>trans('deliveryReceipt/form.select-po'))) !!}
							<!--<select class="ajaxMaster form-control" name="itemName">
							</select>-->
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('purchase_order_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('notes', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('deliveryReceipt/form.notes')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('notes', null, array('class' => 'form-control', 'placeholder'=>'', 'rows'=>'3')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('notes', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label changeLabel2">
                            Purchase Order Item
                        </label>
                        <div class="col-sm-7">
                            <div id="expandible-output">
								<table class="table table-bordered table-hover" id="poItemTable">
									<thead>
										<tr>
											<th width="15%">ID</th>
											<th width="55%">Master Name</th>
											<th width="15%">Qty</th>
											<th width="15%">Input Qty</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td colspan="4" class="text-center">PO Item is Empty</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/delivery-receipt') }}">
                                @lang('button.cancel')
                            </a>
                            <button type="submit" class="btn btn-success">
                                @lang('button.save')
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
	<!-- begining of page level js -->
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
	<script src="{{ asset('assets/vendors/autoNumeric.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
	<script src="{{ asset('assets/js/pages/form_examples.js') }}"></script>
	<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
	<script>
	$(function($) {
		$('.autoNumeric').autoNumeric('init');
		
		$('.ajaxPO').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("purchaseOrderDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		$('.ajaxPO').on("change", function(e) { 
			var $term = $(this).val(),
				$token = '{{ csrf_token() }}';
			$.ajax({
				dataType: 'json',
				url: '{{ route("deliveryPOChangeAjax") }}',
				data: { '_token': $token, 'term': $term },
				success: function (response) {
					console.log(response);
					$("table>tbody").html('');
					if(response.length > 0){
						$.each(response, function(i, item){
							var $row = $('<tr>'+
											'<td><input type="hidden" name="delivery_receipt_item['+i+'][purchase_order_item_id]" value="'+item.id+'">'+item.id+'</td>'+
											'<td><input type="hidden" name="delivery_receipt_item['+i+'][master_data_id]" value="'+item.master_id+'">'+item.master_name+'</td>'+
											'<td class="text-right"><input type="hidden" name="delivery_receipt_item['+i+'][qty]" value="'+item.sisa+'">'+item.sisa+'</td>'+
											'<td class="text-right"><input name="delivery_receipt_item['+i+'][quantity]" class="form-control autoNumericx" placeholder="" style="text-align: right;" data-a-sep=""  data-v-min="0" name="qty" type="text"></td>'+
										'</tr>');//data-v-max="'+item.sisa+'"
							$("table>tbody").append($row);
						});
						$('.autoNumericx').autoNumeric('init');
					}else{
						$("table>tbody").append('<tr><td colspan="4" class="text-center">PO Item is Empty</td></tr>');
					}
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.responseText);
				}
			});
		});		
	});
	</script>
	<!-- end of page level js -->
@stop
