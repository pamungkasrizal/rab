@extends('admin/layouts/default')

{{-- Web site Title --}}

@section('title')
    @lang('InvoicePurchaseOrder/title.edit') @lang('InvoicePurchaseOrder/title.title') :: @parent
@stop

{{-- page level styles --}}
@section('header_styles')
	<link href="{{ asset('assets/vendors/select2/css/select2.min.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/select2/css/select2-bootstrap.css') }}" rel="stylesheet" />
	<link href="{{ asset('assets/vendors/jasny-bootstrap/css/jasny-bootstrap.css') }}"  rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/iCheck/css/all.css') }}"  rel="stylesheet" type="text/css" />
	<link href="{{ asset('assets/vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
@stop

{{-- Content --}}

@section('content')
<section class="content-header">
    <h1>
        @lang('InvoicePurchaseOrder/title.groups')
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ route('dashboard') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i> Dashboard
            </a>
        </li>
		<li>
			<a href="{{ route('purchaseOrder') }}"> <i class="livicon" data-name="home" data-size="16" data-color="#000"></i>
                @lang('InvoicePurchaseOrder/title.title')
            </a>
		</li>
        <li class="active">
            @lang('InvoicePurchaseOrder/title.edit')
        </li>
    </ol>
</section>

<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary ">
                <div class="panel-heading">
                    <h4 class="panel-title"> <i class="livicon" data-name="plus" data-size="22" data-loop="true" data-c="#fff" data-hc="white"></i>
                        @lang('InvoicePurchaseOrder/title.edit') @lang('InvoicePurchaseOrder/title.title')
                    </h4>
                </div>
                <div class="panel-body">
						{!! Form::hidden('rulesValidation', 'invoicePurchaseOrder') !!}
						{!! Form::model($invoicePurchaseOrder, array('url' => URL::to('admin/invoice/purchase-order') . '/' . $invoicePurchaseOrder->id.'/edit', 'method' => 'post', 'class' => 'form-horizontal', 'files'=> true)) !!}
							{!! Form::hidden('id', $invoicePurchaseOrder->id, array('readonly' => 'true', 'class' => 'form-control', 'placeholder'=>'')) !!}
					
					@if($invoicePurchaseOrder->status == '2')
					<div class="alert alert-danger alert-dismissable margin5">
						<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
						<strong>Reject Messages:</strong> {{ $invoicePurchaseOrder->status_msg }}
					</div>
					@endif
					<div class="form-group {{ $errors->first('invoice_number', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('InvoicePurchaseOrder/form.drid')
                        </label>
                        <div class="col-sm-2">
                            {!! Form::text('invoice_number', null, array('class' => 'form-control', 'placeholder'=>'')) !!}
                        </div>
						<div class="col-sm-4">
                            {!! $errors->first('invoice_number', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group {{ $errors->first('purchase_order_id', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('InvoicePurchaseOrder/form.poid')
                        </label>
                        <div class="col-sm-5">
							{!! Form::select('purchase_order_id', $po, old('purchase_order_id',$invoicePurchaseOrder->purchase_order_id), array('class' => 'form-control ajaxPODR', 'placeholder'=>trans('purchaseOrder/form.select-po'))) !!}
							<!--<select class="ajaxMaster form-control" name="itemName">
							</select>-->
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('purchase_order_id', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<span id="onChangePO">
						<!-- onChange PO -->
					</span>
					<div class="form-group {{ $errors->first('notes', 'has-error') }}">
                        <label for="title" class="col-sm-2 control-label">
                            @lang('InvoicePurchaseOrder/form.notes')
                        </label>
                        <div class="col-sm-5">
                            {!! Form::textarea('notes', null, array('class' => 'form-control', 'placeholder'=>'', 'rows'=>'3')) !!}
                        </div>
                        <div class="col-sm-4">
                            {!! $errors->first('notes', '<span class="help-block">:message</span> ') !!}
                        </div>
                    </div>
					<div class="form-group">
                        <label for="title" class="col-sm-2 control-label changeLabel2">
                            Kuitansi PO Item
                        </label>
					</div>
					<div class="form-group">
                        <div class="col-sm-9">
                            <div id="expandible-output">
								<table class="table table-bordered table-hover" id="poItemTable">
									<thead>
										<tr>
											<th width="35%">Surat Jalan ID / Master Barang</th>
											<th width="9%">Qty Item</th>
											<th width="16%">Price Item</th>
											<th width="9%">Qty</th>
											<th width="18%">Total Price</th>
											@if(Sentinel::inRole('admin')) <th width="5%"></th> @endif
										</tr>
									</thead>
									<tbody>
										@if(Sentinel::inRole('admin'))
										<tr>
											<td>
												{!! Form::select('delivery_receipt', $po, null, array('id' => 'delivery_receipt', 'class' => 'form-control ajaxPODRItem', 'placeholder'=>trans('invoicePurchaseOrder/form.select-po'))) !!}
											</td>
											<td>
												{!! Form::text('qtyDelRec', null, array('id' => 'qtyDelRec', 'readonly'=>'true', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => '', 'data-a-dec' => '.')) !!}
											</td>
											<td>
												{!! Form::text('priceDelRec', null, array('id' => 'priceDelRec', 'readonly'=>'true', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
											</td>
											<td>
												{!! Form::text('qty', null, array('id' => 'qty', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => '', 'data-a-dec' => '.')) !!}
											</td>
											<td>
												{!! Form::text('price', null, array('id' => 'price', 'class' => 'form-control autoNumeric mediaCss', 'placeholder'=>'', 'style' => 'text-align: right;', 'data-a-sep' => ',', 'data-a-dec' => '.', 'data-a-sign' =>'Rp ')) !!}
											</td>
											<td>
												<button type="button" id="addPOItem" class="btn btn-responsive btn-primary btn-sm" style="margin-top: 2px;">Add</button>
											</td>
										</tr>
										@endif
										
										@if(!empty($invoicePurchaseOrderItem))
											@foreach ($invoicePurchaseOrderItem as $key => $data)
												<tr>
													<td>
														<input type="hidden" name="invoice_purchase_order_item[{{ $key }}][id]" value="{{ $data->id }}">
														<input type="hidden" name="invoice_purchase_order_item[{{ $key }}][delivery_receipt_item_id]" value="{{ $data->delivery_receipt_item_id }}" class="delivery_receipt_id">
														{{ $data->text}}
													</td>
													<td class="text-right">{{ $data->qty }}</td>
													<td class="text-right">Rp {{ number_format($data->unit_price,'2','.',',') }}</td>
													<td class="text-right">{{ $data->quantity }}</td>
													<td class="text-right">Rp {{ number_format($data->total_price,'2','.',',') }}</td>
													@if(Sentinel::inRole('admin'))
													<td>
														<button type="button" class="btn btn-responsive btn-danger btn-sm deletePOItem" style="margin-top: -5px; margin-bottom: -4px !important" title="delete"><i class="fa fa-fw fa-trash-o"></i></button>
													</td>
													@endif
												</tr>
											@endforeach
										@endif
									</tbody>
								</table>
							</div>
						</div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-4">
                            <a class="btn btn-danger" href="{{ URL::to('admin/invoice/purchase-order') }}">
                                @lang('button.cancel')
                            </a>
							
							@if(Sentinel::inRole('admin'))
								<button type="submit" class="btn btn-success">
									@lang('button.edit')
								</button>
							@endif
							
							@if(Sentinel::inRole('admin') || Sentinel::inRole('logistik_kantor') || Sentinel::inRole('finance'))
								<a href="{{ route('confirm-approve/invoicePurchaseOrder', $invoicePurchaseOrder->id) }}" class="btn btn-success" data-toggle="modal" data-target="#confirm" title="approve data">
								   Approve
								</a>
								<a href="{{ route('confirm-reject/invoicePurchaseOrder', $invoicePurchaseOrder->id) }}" class="btn btn-primary" data-toggle="modal" data-target="#confirm" title="reject data">
								   Reject
								</a>
							@endif
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <!-- row-->
</section>
<div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="user_delete_confirm_title" aria-hidden="true">
	<div class="modal-dialog">
    	<div class="modal-content"></div>
  </div>
</div>
@stop

{{-- page level scripts --}}
@section('footer_scripts')
	<!-- begining of page level js -->
	<script language="javascript" type="text/javascript" src="{{ asset('assets/vendors/select2/js/select2.js') }}"></script>
	<script src="{{ asset('assets/vendors/autoNumeric.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/vendors/jasny-bootstrap/js/jasny-bootstrap.js') }}" ></script>
    <script src="{{ asset('assets/vendors/iCheck/js/icheck.js') }}"></script>
	<script src="{{ asset('assets/js/pages/form_examples.js') }}"></script>
	<script src="{{ asset('assets/vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
	<script>
	$(function($) {
		$('body').on('hidden.bs.modal', '.modal', function () {
			$(this).removeData('bs.modal');
			$('.modal-content').html('');
		});
	
		$(".select2").select2({
			theme:"bootstrap",
			placeholder:"select a value"
		});
		
		$('input[name=qty]').keyup(function(){
			var unit_price = $('input[name=priceDelRec]').autoNumeric('get') !='' ? $('input[name=priceDelRec]').autoNumeric('get') : 0;
			var unit_total_price = parseFloat(unit_price) * $(this).val();
			$('input[name=price]').autoNumeric('set',unit_total_price);
		});
		
		$.ajax({
			dataType: 'json',
			url: '{{ route("deliveryItemChangeAjax") }}',
			data: { '_token': '{{ csrf_token() }}', 'term': $('select[name=purchase_order_id]').val() },
			success: function (response) {
				$("#onChangePO").html('');
				if(response.length > 0){
					$.each(response, function(i, item){
						var $ppn =  (item.vat == 0 ? 'No' : 'Yes');
						var $row = $('<div class="form-group" style="margin-bottom: 0px !important;">'+
										'<label for="title" class="col-sm-2 control-label"></label>'+
										'<div class="col-md-8">'+
											'<div class="box well well-sm">'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'RAB Name'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.rab_name+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Daftar Supplier'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.supplier_name+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'PPn'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														$ppn +
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Delivery Date'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.delivery_date+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Shipping Fee'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.shipping_fee+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Notes'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.notes+
													'</label>'+
												'</div>'+
												'<div class="form-group">'+
													'<label for="title" class="col-sm-3 control-label">'+
														'Term of Payment'+
													'</label>'+
													'<label for="title" class="col-sm-5 control-label" style="font-weight: unset; text-align:left;">'+
														item.term_of_payment+
													' Hari</label>'+
												'</div>'+
											'</div>'+
										'</div>'+
									'</div>');
						$("#onChangePO").html($row);
					});
					$('.autoNumericx').autoNumeric('init');
				}else{
					$("table>tbody").html('<div class="form-group">'+
												'<label for="title" class="col-sm-4 control-label">'+
													'DATA TIDAK TERSEDIA'+
												'</label>'+
											'</div>');
				}
			},
			error: function (xhr, ajaxOptions, thrownError) {
				console.log(xhr.responseText);
			}
		});
		
		$('.autoNumeric').autoNumeric('init');
		
		var whereNotIn = [];
		@if(!empty($invoicePurchaseOrderItem))
			@foreach ($invoicePurchaseOrderItem as $key => $data)
				whereNotIn.push('{{ $data->delivery_receipt_item_id }}');
			@endforeach
		@endif

		$('.ajaxPODRItem').select2({
			theme:"bootstrap",
			placeholder: 'Select an item',
			minimumInputLength: 1,
			ajax: {
                dataType: 'json',
                url: '{{ route("poDRItemDataSearchAjax") }}',
                delay: 250,
                data: function(params) {
                    return {
                        term: params.term,
						poId: $('.ajaxPODR').val(),
						whereNotIn: (whereNotIn.length > 0) ? JSON.stringify(whereNotIn) : ''
                    }
                },
                processResults: function (data, page) {
                    return {
                        results: data
                    };
                },
            }
		});
		
		$('.ajaxPODRItem').on("change", function(e) { 
			var $term = $(this).val(),
				$token = '{{ csrf_token() }}';
			$.ajax({
				dataType: 'json',
				url: '{{ route("poDRItemChangeAjax") }}',
				data: { '_token': $token, 'term': $term },
				success: function (response) {
					$('#qtyDelRec').val(response.quantity);
					$('#priceDelRec').autoNumeric('set', response.unit_price);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					console.log(xhr.responseText);
				}
			});
		});
		
		var $no = parseInt('{{ count($invoicePurchaseOrderItem)+2 }}');
		$("#addPOItem").click(function(){
			var $delivery_receipt_id = $('#delivery_receipt option:selected').val(),
				$delivery_receipt = $('#delivery_receipt option:selected').text(),
				$qty = $('#qty').val(),
				$price = $('#price').val(),
				$price1 = $('#price').autoNumeric('get'),
				$valDR = $delivery_receipt_id.trim().length,
				$valQty = $qty.trim().length,
				$valPrice = $price1.trim().length;
				
			($valDR < 1) ? $('.select2-selection--single').css('border-color','#EF6F6C') : $('.select2-selection--single').css('border-color','');
			($valQty < 1) ? $('#qty').css('border-color','#EF6F6C') : $('#qty').css('border-color','');
			($valPrice < 1) ? $('#price').css('border-color','#EF6F6C') : $('#price').css('border-color','');
			
			if($valDR < 1 || $valQty < 1 || $valPrice < 1) return false;
			whereNotIn.push($delivery_receipt_id);
			
			var $row = $('<tr>'+
							'<td><input type="hidden" name="invoice_purchase_order_item['+$no+'][delivery_receipt_item_id]" value="'+$delivery_receipt_id+'" class="delivery_receipt_id">'+$delivery_receipt+'</td>'+
							'<td class="text-right">'+$('#qtyDelRec').val()+'</td>'+
							'<td class="text-right">'+$('#priceDelRec').val()+'</td>'+
							'<td class="text-right"><input type="hidden" name="invoice_purchase_order_item['+$no+'][quantity]" value="'+$qty+'">'+$qty+'</td>'+
							'<td class="text-right"><input type="hidden" name="invoice_purchase_order_item['+$no+'][total_price]" value="'+$price1+'">'+$price+'</td>'+
							'<td><button type="button" class="btn btn-responsive btn-danger btn-sm deletePOItem" style="margin-top: -5px; margin-bottom: -4px !important" title="delete"><i class="fa fa-fw fa-trash-o"></i></button></td>'+
						'</tr>');
						
			$("table>tbody").append($row);
			$no = parseInt($no) + 1;
			$('table select, table input[type=text]').val('');
			$('table #select2-delivery_receipt-container').html('');
		});
		
		$("table>tbody").on('click', 'button.deletePOItem', function(){
			var delivery_receipt_id = $(this).closest('tr').find('input.delivery_receipt_id').val();
			whereNotIn = whereNotIn.filter(x=>x != delivery_receipt_id);
			$(this).closest('tr').remove();
		});
		
		$('select[name=purchase_order_id], input[name=invoice_number]').prop('disabled', true);
		if("{{ Sentinel::inRole('admin') }}" == ''){
			$('input, select, textarea').prop('disabled', true);
		}
	});
	</script>
	<!-- end of page level js -->
@stop
