<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'				=> 'ID',
    'rab'				=> 'RAB Name',
    'select-rab'		=> 'Select a RAB name',
	'subcontractor'		=> 'Daftar Subkon',
    'select-subcontractor'	=> 'Select a Daftar Subkon',
    'vat'				=> 'Retensi',
	'start_date' 		=> 'Start Date',
	'notes'				=> 'Notes',
	'term'				=> 'Term of Payment',
	'approved_by'		=> 'Approved By',
);
