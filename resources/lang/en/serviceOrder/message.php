<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'SPK Subkon was successfully created.',
        'update'    => 'SPK Subkon was successfully updated.',
        'delete'    => 'SPK Subkon was successfully deleted.',
        'approve'    => 'SPK Subkon was successfully approved.',
        'reject'    => 'SPK Subkon was successfully rejected.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the SPK Subkon. Please try again.',
        'update'    => 'There was an issue updating the SPK Subkon. Please try again.',
        'delete'    => 'There was an issue deleting the SPK Subkon. Please try again.',
        'approve'    => 'There was an issue approving the SPK Subkon. Please try again.',
        'reject'    => 'There was an issue rejecting the SPK Subkon. Please try again.',
    ),

);
