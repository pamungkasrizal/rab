<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Daftar Subkon was successfully created.',
        'update'    => 'Daftar Subkon was successfully updated.',
        'delete'    => 'Daftar Subkon was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Daftar Subkon. Please try again.',
        'update'    => 'There was an issue updating the Daftar Subkon. Please try again.',
        'delete'    => 'There was an issue deleting the Daftar Subkon. Please try again.',
    ),

);
