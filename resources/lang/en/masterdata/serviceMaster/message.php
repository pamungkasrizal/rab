<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceMaster_exists'              => 'Blog category already exists!',
    'serviceMaster_not_found'           => 'Blog category [:id] does not exist.',
    'serviceMaster_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Item Pekerjaan Subkon was successfully created.',
        'update'    => 'Item Pekerjaan Subkon successfully updated.',
        'delete'    => 'Item Pekerjaan Subkon successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Item Pekerjaan Subkon. Please try again.',
        'update'    => 'There was an issue updating the Item Pekerjaan Subkon. Please try again.',
        'delete'    => 'There was an issue deleting the Item Pekerjaan Subkon. Please try again.',
    ),

);
