<?php

/**
* Language file for blog delete modal
*
*/
return array(

    'body'			=> 'Are you sure to delete this Item Pekerjaan Subkon? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
    'title'         => 'Delete Item Pekerjaan Subkon',

);
