<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Material',
    'create'			=> 'Add New',
    'edit' 				=> 'Edit',
    'management' => 'Manage Material',
    'blogcategories' => 'Blog',
    'groups' => 'Master Data',
    'categories' => 'Categories',
    'list' => 'Material List',


    
);
