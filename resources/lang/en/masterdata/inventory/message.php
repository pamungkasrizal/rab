<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'inventory_exists'              => 'Blog category already exists!',
    'inventory_not_found'           => 'Blog category [:id] does not exist.',
    'inventory_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Material was successfully created.',
        'update'    => 'Material was successfully updated.',
        'delete'    => 'Material was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Material. Please try again.',
        'update'    => 'There was an issue updating the Material. Please try again.',
        'delete'    => 'There was an issue deleting the Material. Please try again.',
    ),

);
