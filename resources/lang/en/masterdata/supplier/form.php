<?php
/**
* Language file for group management form text
*
*/
return array(

    'name'			=> 'Daftar Supplier Name',
    'general' 		=> 'General',
    'categoryname' => 'Category name',
    'update-blog' => 'update blog',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
