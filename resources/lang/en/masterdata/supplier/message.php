<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Daftar Supplier was successfully created.',
        'update'    => 'Daftar Supplier was successfully updated.',
        'delete'    => 'Daftar Supplier was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Daftar Supplier. Please try again.',
        'update'    => 'There was an issue updating the Daftar Supplier. Please try again.',
        'delete'    => 'There was an issue deleting the Daftar Supplier. Please try again.',
    ),

);
