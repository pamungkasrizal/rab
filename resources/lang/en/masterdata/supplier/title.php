<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Daftar Supplier',
    'create'			=> 'Add New',
    'edit' 				=> 'Edit',
    'management' => 'Manage Daftar Supplier',
    'blogcategories' => 'Blog',
    'groups' => 'Master Data',
    'categories' => 'Categories',
    'list' => 'Daftar Supplier List',


    
);
