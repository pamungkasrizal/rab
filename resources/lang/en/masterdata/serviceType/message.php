<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Jenis Pekerjaan Subkon was successfully created.',
        'update'    => 'Jenis Pekerjaan Subkon was successfully updated.',
        'delete'    => 'Jenis Pekerjaan Subkon was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Jenis Pekerjaan Subkon. Please try again.',
        'update'    => 'There was an issue updating the Jenis Pekerjaan Subkon. Please try again.',
        'delete'    => 'There was an issue deleting the Jenis Pekerjaan Subkon. Please try again.',
    ),

);
