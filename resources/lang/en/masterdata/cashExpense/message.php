<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'cashExpense_exists'              => 'Blog category already exists!',
    'cashExpense_not_found'           => 'Blog category [:id] does not exist.',
    'cashExpense_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Bukti Kas was successfully created.',
        'update'    => 'Bukti Kas was successfully updated.',
        'delete'    => 'Bukti Kas was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Bukti Kas. Please try again.',
        'update'    => 'There was an issue updating the Bukti Kas. Please try again.',
        'delete'    => 'There was an issue deleting the Bukti Kas. Please try again.',
    ),

);
