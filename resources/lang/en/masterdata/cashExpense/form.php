<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'			=> 'ID',
    'name'			=> 'Name',
    'type'			=> 'Bukti Kas',
    'general' 		=> 'General',
    'categoryname' => 'Category name',
    'select-category' => 'Select a Jenis Pekerjaan Subkon',
    'blogcategoryexists' => 'Blog category exists',
    'deleteblogcategory' => 'Delete blog category',

);
