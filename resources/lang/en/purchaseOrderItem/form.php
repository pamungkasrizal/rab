<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'			=> 'ID',
    'purchase-order'			=> 'Purchase Order',
    'select-purchase-order'	=> 'Select a Purchase Order',
    'master_data'	=> 'Master Name',
	'select-master_data' => 'Select a master data',
	'qty'			=> 'Quantity',
	'price'			=> 'Price per Unit',
	'total_price'	=> 'Total Price',
);
