<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title'				=> 'Purchase Order Item',
    'create'			=> 'Add New',
    'edit' 				=> 'Edit',
    'management' 		=> 'Manage Purchase Order Item',
    'groups' 			=> 'Purchase Order Item',
    'list' 				=> 'Purchase Order Item List',


    
);
