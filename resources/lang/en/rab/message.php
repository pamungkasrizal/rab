<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'RAB was successfully created.',
        'update'    => 'RAB was successfully updated.',
        'delete'    => 'RAB was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the RAB. Please try again.',
        'update'    => 'There was an issue updating the RAB. Please try again.',
        'delete'    => 'There was an issue deleting the RAB. Please try again.',
    ),

);
