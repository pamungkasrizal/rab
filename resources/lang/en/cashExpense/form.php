<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'				=> 'ID',
    'rab'				=> 'RAB Name',
    'select-rab'		=> 'Select a RAB name',
	'notes'				=> 'Notes',
	'approved_by'		=> 'Approved By',
);
