<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Bukti Kas was successfully created.',
        'update'    => 'Bukti Kas was successfully updated.',
        'delete'    => 'Bukti Kas was successfully deleted.',
        'approve'    => 'Bukti Kas was successfully approved.',
        'reject'    => 'Bukti Kas was successfully rejected.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Bukti Kas. Please try again.',
        'update'    => 'There was an issue updating the Bukti Kas. Please try again.',
        'delete'    => 'There was an issue deleting the Bukti Kas. Please try again.',
        'approve'    => 'There was an issue approving the Bukti Kas. Please try again.',
        'reject'    => 'There was an issue rejecting the Bukti Kas. Please try again.',
    ),

);
