<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'			=> 'ID',
    'name'			=> 'Project Name',
    'client' 		=> 'Client',
    'start_date' => 'Start Date',
	'select-client' => 'Select a client',
    'end_date' => 'End Date'
);
