<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Project was successfully created.',
        'update'    => 'Project was successfully updated.',
        'delete'    => 'Project was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the project. Please try again.',
        'update'    => 'There was an issue updating the project. Please try again.',
        'delete'    => 'There was an issue deleting the project. Please try again.',
    ),

);
