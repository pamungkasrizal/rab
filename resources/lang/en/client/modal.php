<?php

/**
* Language file for blog delete modal
*
*/
return array(

    'body'			=> 'Are you sure to delete this client? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
    'title'         => 'Delete Client',

);
