<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Client was successfully created.',
        'update'    => 'Client was successfully updated.',
        'delete'    => 'Client was successfully deleted.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the client. Please try again.',
        'update'    => 'There was an issue updating the client. Please try again.',
        'delete'    => 'There was an issue deleting the client. Please try again.',
    ),

);
