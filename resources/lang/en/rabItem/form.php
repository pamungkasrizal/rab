<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'			=> 'ID',
    'rab'			=> 'RAB Name',
    'select-rab'	=> 'Select a RAB name',
    'master_data'	=> 'Master Name',
	'select-master_data' => 'Select a master data',
	'qty'			=> 'Quantity',
	'price'			=> 'Price per Unit',
	'total_price'	=> 'Total Price',
);
