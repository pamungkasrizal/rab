<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Kuitansi Purchase Order',
    'create'			=> 'Add New',
    'edit' 				=> 'Edit',
    'management' => 'Manage Kuitansi Purchase Order',
    'groups' => 'Kuitansi Purchase Order',
    'list' => 'Kuitansi Purchase Order List',


    
);
