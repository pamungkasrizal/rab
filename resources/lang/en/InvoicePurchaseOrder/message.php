<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Kuitansi Purchase Order was successfully created.',
        'update'    => 'Kuitansi Purchase Order was successfully updated.',
        'delete'    => 'Kuitansi Purchase Order was successfully deleted.',
        'approve'    => 'Kuitansi Purchase Order was successfully approved.',
        'reject'    => 'Kuitansi Purchase Order was successfully rejected.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Kuitansi Purchase Order. Please try again.',
        'update'    => 'There was an issue updating the Kuitansi Purchase Order. Please try again.',
        'delete'    => 'There was an issue deleting the Kuitansi Purchase Order. Please try again.',
        'approve'    => 'There was an issue approving the Kuitansi Purchase Order. Please try again.',
        'reject'    => 'There was an issue rejecting the Kuitansi Purchase Order. Please try again.',
    ),

);
