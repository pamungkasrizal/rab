<?php
/**
* Language file for blog section titles
*
*/

return array(

    'title' => 'Purchase Order',
    'create'			=> 'Add New',
    'edit' 				=> 'Edit',
    'management' => 'Manage Purchase Order',
    'groups' => 'Purchase Order',
    'list' => 'Purchase Order List',


    
);
