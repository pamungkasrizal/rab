<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Purchase Order was successfully created.',
        'update'    => 'Purchase Order was successfully updated.',
        'delete'    => 'Purchase Order was successfully deleted.',
        'approve'    => 'Purchase Order was successfully approved.',
        'reject'    => 'Purchase Order was successfully rejected.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Purchase Order. Please try again.',
        'update'    => 'There was an issue updating the Purchase Order. Please try again.',
        'delete'    => 'There was an issue deleting the Purchase Order. Please try again.',
        'approve'    => 'There was an issue approving the Purchase Order. Please try again.',
        'reject'    => 'There was an issue rejecting the Purchase Order. Please try again.',
    ),

);
