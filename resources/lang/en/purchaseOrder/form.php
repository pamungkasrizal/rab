<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'				=> 'ID',
    'rab'				=> 'RAB Name',
    'select-rab'		=> 'Select a RAB name',
	'supplier'			=> 'Daftar Supplier',
    'select-supplier'	=> 'Select a Daftar Supplier',
    'vat'				=> 'PPn',
	'delivery_date' 	=> 'Delivery Date',
	'shipping_fee'		=> 'Shipping Fee',
	'notes'				=> 'Notes',
	'term'				=> 'Term of Payment',
	'approved_by'		=> 'Approved By',
);
