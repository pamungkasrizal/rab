<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'				=> 'ID',
    'drid'				=> 'Kuitansi Number',
	'poid'				=> 'SPK Subkon ID',
    'select-po'			=> 'Select a SPK Subkon ID',
    'notes'				=> 'Notes'
);
