<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Kuitansi SPK Subkon was successfully created.',
        'update'    => 'Kuitansi SPK Subkon was successfully updated.',
        'delete'    => 'Kuitansi SPK Subkon was successfully deleted.',
        'approve'    => 'Kuitansi SPK Subkon was successfully approved.',
        'reject'    => 'Kuitansi SPK Subkon was successfully rejected.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Kuitansi SPK Subkon. Please try again.',
        'update'    => 'There was an issue updating the Kuitansi SPK Subkon. Please try again.',
        'delete'    => 'There was an issue deleting the Kuitansi SPK Subkon. Please try again.',
        'approve'    => 'There was an issue approving the Kuitansi SPK Subkon. Please try again.',
        'reject'    => 'There was an issue rejecting the Kuitansi SPK Subkon. Please try again.',
    ),

);
