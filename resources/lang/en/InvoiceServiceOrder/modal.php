<?php

/**
* Language file for blog delete modal
*
*/
return array(

    'body'			=> 'Are you sure to delete this Kuitansi SPK Subkon? This operation is irreversible.',
    'cancel'		=> 'Cancel',
    'confirm'		=> 'Delete',
    'title'         => 'Delete Kuitansi SPK Subkon',

);
