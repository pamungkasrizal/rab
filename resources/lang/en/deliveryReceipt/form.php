<?php
/**
* Language file for group management form text
*
*/
return array(

    'id'				=> 'ID',
    'drid'				=> 'Surat Jalan No',
	'poid'				=> 'Purchase Order ID',
    'select-po'			=> 'Select a Purchase Order ID',
    'notes'				=> 'Notes'
);
