<?php
/**
* Language file for blog error/success messages
*
*/

return array(

    'serviceType_exists'              => 'Blog category already exists!',
    'serviceType_not_found'           => 'Blog category [:id] does not exist.',
    'serviceType_have_blog'           => 'Blog category have blogs!',

    'success' => array(
        'create'    => 'Surat Jalan was successfully created.',
        'update'    => 'Surat Jalan was successfully updated.',
        'delete'    => 'Surat Jalan was successfully deleted.',
        'approve'    => 'Surat Jalan was successfully approved.',
        'reject'    => 'Surat Jalan was successfully rejected.',
    ),

    'error' => array(
        'create'    => 'There was an issue creating the Surat Jalan. Please try again.',
        'update'    => 'There was an issue updating the Surat Jalan. Please try again.',
        'delete'    => 'There was an issue deleting the Surat Jalan. Please try again.',
        'approve'    => 'There was an issue approving the Surat Jalan. Please try again.',
        'reject'    => 'There was an issue rejecting the Surat Jalan. Please try again.',
    ),

);
