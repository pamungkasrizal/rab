<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RabItem extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','rab_id','master_data_id','quantity','unit_price','unit_total_price'];
	#protected $dates = ['deleted_at'];
    protected $table = 'rab_item';
    protected $guarded = ['id'];

	public function masterData()
	{
		return $this->hasOne( 'App\MasterData', 'id', 'master_data_id');
	}
}