<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceType extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['service_type_name'];
	#protected $dates = ['deleted_at'];
    protected $table = 'service_type';
    protected $guarded = ['id'];
}