<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Subcontractor extends Model
{
    protected $fillable = ['subcontractor_name'];
	#protected $dates = ['deleted_at'];
    protected $table = 'subcontractor';
    protected $guarded = ['id'];
}
