<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrderItem extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','purchase_order_id','rab_id','rab_item_id','master_data_id','quantity','unit_price','unit_total_price'];
	#protected $dates = ['deleted_at'];
    protected $table = 'purchase_order_item';
    protected $guarded = ['id'];
	public $incrementing = false;
}