<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceServiceOrder extends Model
{
    protected $fillable = ['id','service_order_id','invoice_number','notes'];
	#protected $dates = ['deleted_at'];
    protected $table = 'invoice_service_order';
    protected $guarded = ['id'];
	public $incrementing = false;
}
