<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashExpenseItem extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','cash_expense_id','rab_item_id','master_data_id','progress','unit_price','unit_total_price'];
	#protected $dates = ['deleted_at'];
    protected $table = 'cash_expense_item';
    protected $guarded = ['id'];
}