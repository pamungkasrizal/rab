<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoicePurchaseOrderItem extends Model
{
    protected $fillable = ['id','invoice_id','delivery_receipt_item_id','quantity','total_price'];
	#protected $dates = ['deleted_at'];
    protected $table = 'invoice_purchase_order_item';
    protected $guarded = ['id'];
	public $incrementing = false;
}
