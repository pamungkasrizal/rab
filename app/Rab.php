<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Rab extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','project_id','name'];
	#protected $dates = ['deleted_at'];
    protected $table = 'rab';
    protected $guarded = ['id'];
	
	public function rabItem()
	{
		return $this->hasMany( 'App\RabItem');
	}
	
	public function purchaseOrder()
	{
		return $this->hasMany( 'App\PurchaseOrder');
	}


}