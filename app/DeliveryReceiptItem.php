<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryReceiptItem extends Model
{
    protected $fillable = ['id','delivery_receipt_id','purchase_order_id','purchase_order_item_id','quantity'];
	#protected $dates = ['deleted_at'];
    protected $table = 'delivery_receipt_item';
    protected $guarded = ['id'];
	public $incrementing = false;
}
