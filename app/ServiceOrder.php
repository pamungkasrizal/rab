<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceOrder extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','rab_id','subcontractor_id','vat','start_date','notes','term_of_payment','requested_by','requested_at','approved_by','approved_at'];
	#protected $dates = ['deleted_at'];
    protected $table = 'service_order';
    protected $guarded = ['id'];
	public $incrementing = false;
}