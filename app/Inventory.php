<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Inventory extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','delivery_receipt_item_id','master_data_id','quantity','location'];
	#protected $dates = ['deleted_at'];
    protected $table = 'inventory';
    protected $guarded = ['id'];
}
