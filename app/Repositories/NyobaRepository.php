<?php

namespace App\Repositories;

use App\Models\Nyoba;
use InfyOm\Generator\Common\BaseRepository;

class NyobaRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        'aSAsa',
        'sasasa'
    ];

    /**
     * Configure the Model
     **/
    public function model()
    {
        return Nyoba::class;
    }
}
