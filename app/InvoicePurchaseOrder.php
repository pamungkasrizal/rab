<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoicePurchaseOrder extends Model
{
    protected $fillable = ['id','purchase_order_id','invoice_number','notes'];
	#protected $dates = ['deleted_at'];
    protected $table = 'invoice_purchase_order';
    protected $guarded = ['id'];
	public $incrementing = false;
}
