<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Supplier extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['supplier_name','email','fax','telepon'];
	#protected $dates = ['deleted_at'];
    protected $table = 'supplier';
    protected $guarded = ['id'];
}