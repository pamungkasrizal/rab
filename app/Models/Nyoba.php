<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * @SWG\Definition(
 *      definition="Nyoba",
 *      required={"aSAsa", "sasasa"},
 *      @SWG\Property(
 *          property="id",
 *          description="id",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="aSAsa",
 *          description="aSAsa",
 *          type="integer",
 *          format="int32"
 *      ),
 *      @SWG\Property(
 *          property="sasasa",
 *          description="sasasa",
 *          type="string"
 *      )
 * )
 */
class Nyoba extends Model
{
    use SoftDeletes;

    public $table = 'nyobas';
    

    protected $dates = ['deleted_at'];

    protected $primaryKey = 'aSAsa';

    public $fillable = [
        'aSAsa',
        'sasasa'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'sasasa' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'aSAsa' => 'required',
        'sasasa' => 'required'
    ];
}
