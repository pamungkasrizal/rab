<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Client extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','client_name','client_address','created_at','updated_at'];
	#protected $dates = ['deleted_at'];
    protected $table = 'client';
    protected $guarded = ['id'];
}