<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class CashExpense extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','rab_id','notes','requested_by','requested_at','approved_by','approved_at'];
	#protected $dates = ['deleted_at'];
    protected $table = 'cash_expense';
    protected $guarded = ['id'];
}