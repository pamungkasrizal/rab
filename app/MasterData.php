<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MasterData extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','name','service_type','inventory_unit'];
	#protected $dates = ['deleted_at'];
    protected $table = 'master_data';
    protected $guarded = ['id'];
	public $incrementing = false;
}
