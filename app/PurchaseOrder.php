<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PurchaseOrder extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','rab_id','supplier_id','vat','delivery_date','shipping_fee','notes','term_of_payment','requested_by','requested_at','approved_by','approved_at'];
	#protected $dates = ['deleted_at'];
    protected $table = 'purchase_order';
    protected $guarded = ['id'];
	public $incrementing = false;
}