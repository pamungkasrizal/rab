<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DeliveryReceipt extends Model
{
    protected $fillable = ['id','delivery_receipt_number','master_data_id','purchase_order_id','notes'];
	#protected $dates = ['deleted_at'];
    protected $table = 'delivery_receipt';
    protected $guarded = ['id'];
	public $incrementing = false;
}
