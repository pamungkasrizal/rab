<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/**
 * Model binding into route
 */
Route::model('users', 'App\User');

Route::pattern('slug', '[a-z0-9- _]+');
#==================================================Start Admin Routes===============================================
Route::group(array('prefix' => 'admin'), function () {

	# Error pages should be shown without requiring login
	Route::get('404', function () {
		return View('admin/404');
	});
	Route::get('500', function () {
		return View::make('admin/500');
	});

    Route::post('secureImage', array('as' => 'secureImage','uses' => 'JoshController@secureImage'));

    # Lock screen
    Route::get('{id}/lockscreen', array('as' => 'lockscreen', 'uses' =>'UsersController@lockscreen'));
    Route::post('{id}/lockscreen', array('as' => 'lockscreen', 'uses' =>'UsersController@postLockscreen'));

	# All basic routes defined here
	Route::get('signin', array('as' => 'signin', 'uses' => 'AuthController@getSignin'));
	Route::post('signin', 'AuthController@postSignin');
	Route::post('signup', array('as' => 'signup', 'uses' => 'AuthController@postSignup'));
	Route::post('forgot-password', array('as' => 'forgot-password', 'uses' => 'AuthController@postForgotPassword'));
	Route::get('login2', function () {
		return View::make('admin/login2');
	});

	# Register2
	Route::get('register2', function () {
		return View::make('admin/register2');
	});
	Route::post('register2', array('as' => 'register2', 'uses' => 'AuthController@postRegister2'));

	# Forgot Password Confirmation
	Route::get('forgot-password/{userId}/{passwordResetCode}', array('as' => 'forgot-password-confirm', 'uses' => 'AuthController@getForgotPasswordConfirm'));
	Route::post('forgot-password/{userId}/{passwordResetCode}', 'AuthController@postForgotPasswordConfirm');

	# Logout
	Route::get('logout', array('as' => 'logout', 'uses' => 'AuthController@getLogout'));

	# Account Activation
	Route::get('activate/{userId}/{activationCode}', array('as' => 'activate', 'uses' => 'AuthController@getActivate'));
});
#==================================================Mid Admin Routes===============================================
Route::group(array('prefix' => 'admin'), function () {
    # Dashboard / Index
	Route::get('/', array('as' => 'dashboard','uses' => 'JoshController@showHome'));
	
	Route::get('unauthorized', function () {
		return View::make('admin/unauthorized');
	});

    # User Management
    Route::group(array('prefix' => 'users', 'middleware' => 'SentinelAllMenu'), function () {
        Route::get('/', array('as' => 'users', 'uses' => 'UsersController@index'));
        Route::get('data',['as' => 'users.data', 'uses' =>'UsersController@data']);
        Route::get('create', 'UsersController@create');
        Route::post('create', 'UsersController@store');
        Route::get('{userId}/delete', array('as' => 'delete/user', 'uses' => 'UsersController@destroy'));
        Route::get('{userId}/confirm-delete', array('as' => 'confirm-delete/user', 'uses' => 'UsersController@getModalDelete'));
        Route::get('{userId}/restore', array('as' => 'restore/user', 'uses' => 'UsersController@getRestore'));
        Route::get('{userId}', array('as' => 'users.show', 'uses' => 'UsersController@show'));
        Route::post('{userId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'UsersController@passwordreset'));
    });
    Route::resource('users', 'UsersController');
	
	Route::group(array('prefix' => 'groups', 'middleware' => 'SentinelAllMenu'), function () {
        Route::get('/', array('as' => 'groups', 'uses' => 'GroupsController@index'));
        Route::get('data',['as' => 'groups.data', 'uses' =>'GroupsController@data']);
        Route::get('create', array('as' => 'create/group', 'uses' => 'GroupsController@create'));
        Route::post('create', 'GroupsController@store');
        Route::get('{groupId}/delete', array('as' => 'delete/group', 'uses' => 'GroupsController@destroy'));
        Route::get('{groupId}/confirm-delete', array('as' => 'confirm-delete/group', 'uses' => 'GroupsController@getModalDelete'));
        Route::get('{groupId}/restore', array('as' => 'restore/group', 'uses' => 'GroupsController@getRestore'));
        Route::get('{groupId}', array('as' => 'groups.show', 'uses' => 'GroupsController@show'));
        Route::post('{groupId}/passwordreset', array('as' => 'passwordreset', 'uses' => 'GroupsController@passwordreset'));
		Route::get('{groupId}/edit', array('as' => 'update/group', 'uses' => 'GroupsController@edit'));
        Route::post('{groupId}/edit', 'GroupsController@update');
    });

	Route::get('deleted_users',array('as' => 'deleted_users','before' => 'Sentinel', 'uses' => 'UsersController@getDeletedUsers'));
	
	/* ============================================================================================================================== */
	# Master Data Management
	Route::group(array('prefix' => 'master'), function(){
		Route::get('/', array('as' => 'masters', 'uses' => 'MasterDataController@index'))->middleware('role:admin,kasir,logistik_kantor');
		Route::get('import/{importType}', array('as' => 'import/importType', 'uses' => 'JoshController@getModalImport'))->middleware('role:admin,kasir,logistik_kantor');
		Route::get('download-template/{typeFile}', array('as' => 'download-template/typeFile', 'uses' => 'JoshController@downloadTemplate'))->middleware('role:admin,kasir,logistik_kantor');
		Route::post('import/{importType}', 'JoshController@importData')->middleware('role:admin,kasir,logistik_kantor');
		#cash expense
		Route::get('cash-expense', array('as' => 'cashExpense', 'uses' => 'CashExpenseMasterController@index'))->middleware('role:admin,kasir');
		Route::get('cash-expense/data',['as' => 'master.cashExpense.data', 'uses' =>'CashExpenseMasterController@data'])->middleware('role:admin,kasir');
		Route::get('cash-expense/create', array('as' => 'create/masterData', 'uses' => 'CashExpenseMasterController@create'))->middleware('role:admin,kasir');
		Route::post('cash-expense/create', 'CashExpenseMasterController@store')->middleware('role:admin,kasir');
		Route::get('cash-expense/{masterData}/confirm-delete', array('as' => 'confirm-delete-ce/masterData', 'uses' => 'CashExpenseMasterController@getModalDelete'))->middleware('role:admin,kasir');
		Route::get('cash-expense/{masterData}/delete', array('as' => 'delete/masterCashExpense', 'uses' => 'CashExpenseMasterController@destroy'))->middleware('role:admin,kasir');
		Route::get('cash-expense/{masterData}/edit', array('as' => 'update/masterData', 'uses' => 'CashExpenseMasterController@edit'))->middleware('role:admin,kasir');
        Route::post('cash-expense/{masterData}/edit', 'CashExpenseMasterController@update')->middleware('role:admin,kasir');
		Route::get('cash-expense/xlsx', 'CashExpenseMasterController@exportFile')->middleware('role:admin,kasir');
		Route::get('cash-expense/csv', 'CashExpenseMasterController@exportFile')->middleware('role:admin,kasir');	
		#service type
		Route::get('service-type', array('as' => 'serviceType', 'uses' => 'ServiceTypeController@index'))->middleware('role:admin,logistik_kantor');
		Route::get('service-type/data',['as' => 'master.service-type.data', 'uses' =>'ServiceTypeController@data'])->middleware('role:admin,logistik_kantor');
		Route::get('service-type/create', array('as' => 'create/serviceType', 'uses' => 'ServiceTypeController@create'))->middleware('role:admin,logistik_kantor');
		Route::post('service-type/create', 'ServiceTypeController@store')->middleware('role:admin,logistik_kantor');
		Route::get('service-type/{serviceType}/confirm-delete', array('as' => 'confirm-delete-st/serviceType', 'uses' => 'ServiceTypeController@getModalDelete'))->middleware('role:admin,logistik_kantor');
		Route::get('service-type/{serviceType}/delete', array('as' => 'delete/serviceType', 'uses' => 'ServiceTypeController@destroy'))->middleware('role:admin,logistik_kantor');
		Route::get('service-type/{serviceType}/edit', array('as' => 'update/serviceType', 'uses' => 'ServiceTypeController@edit'))->middleware('role:admin,logistik_kantor');
        Route::post('service-type/{serviceType}/edit', 'ServiceTypeController@update')->middleware('role:admin,logistik_kantor');
		Route::get('service-type/xlsx', 'ServiceTypeController@exportFile')->middleware('role:admin,logistik_kantor');
		Route::get('service-type/csv', 'ServiceTypeController@exportFile')->middleware('role:admin,logistik_kantor');
		#service master
		Route::get('service-master', array('as' => 'serviceMaster', 'uses' => 'ServiceMasterController@index'))->middleware('role:admin,logistik_kantor');
		Route::get('service-master/data',['as' => 'master.service-master.data', 'uses' =>'ServiceMasterController@data'])->middleware('role:admin,logistik_kantor');
		Route::get('service-master/create', array('as' => 'create/masterData', 'uses' => 'ServiceMasterController@create'))->middleware('role:admin,logistik_kantor');
		Route::post('service-master/create', 'ServiceMasterController@store')->middleware('role:admin,logistik_kantor');
		Route::get('service-master/{masterData}/confirm-delete', array('as' => 'confirm-delete-sm/masterData', 'uses' => 'ServiceMasterController@getModalDelete'))->middleware('role:admin,logistik_kantor');
		Route::get('service-master/{masterData}/delete', array('as' => 'delete/masterServiceMaster', 'uses' => 'ServiceMasterController@destroy'))->middleware('role:admin,logistik_kantor');
		Route::get('service-master/{masterData}/edit', array('as' => 'update/masterData', 'uses' => 'ServiceMasterController@edit'))->middleware('role:admin,logistik_kantor');
        Route::post('service-master/{masterData}/edit', 'ServiceMasterController@update')->middleware('role:admin,logistik_kantor');
		Route::get('service-master/xlsx', 'ServiceMasterController@exportFile')->middleware('role:admin,logistik_kantor');
		Route::get('service-master/csv', 'ServiceMasterController@exportFile')->middleware('role:admin,logistik_kantor');
		#inventory
		Route::get('inventory', array('as' => 'inventory', 'uses' => 'InventoryController@index'))->middleware('role:admin,logistik_kantor');
		Route::get('inventory/data',['as' => 'master.inventory.data', 'uses' =>'InventoryController@data'])->middleware('role:admin,logistik_kantor');
		Route::get('inventory/create', array('as' => 'create/masterData', 'uses' => 'InventoryController@create'))->middleware('role:admin,logistik_kantor');
		Route::post('inventory/create', 'InventoryController@store')->middleware('role:admin,logistik_kantor');
		Route::get('inventory/{masterData}/confirm-delete', array('as' => 'confirm-delete-inventory/masterData', 'uses' => 'InventoryController@getModalDelete'))->middleware('role:admin,logistik_kantor');
		Route::get('inventory/{masterData}/delete', array('as' => 'delete/masterData', 'uses' => 'InventoryController@destroy'))->middleware('role:admin,logistik_kantor');
		Route::get('inventory/{masterData}/edit', array('as' => 'update/masterData', 'uses' => 'InventoryController@edit'))->middleware('role:admin,logistik_kantor');
        Route::post('inventory/{masterData}/edit', 'InventoryController@update')->middleware('role:admin,logistik_kantor');
		Route::get('inventory/xlsx', 'InventoryController@exportFile')->middleware('role:admin,logistik_kantor');
		Route::get('inventory/csv', 'InventoryController@exportFile')->middleware('role:admin,logistik_kantor');
		#supplier
		Route::get('supplier', array('as' => 'supplier', 'uses' => 'SupplierController@index'))->middleware('role:admin,logistik_kantor');
		Route::get('supplier/data',['as' => 'master.supplier.data', 'uses' =>'SupplierController@data'])->middleware('role:admin,logistik_kantor');
		Route::get('supplier/create', array('as' => 'create/supplier', 'uses' => 'SupplierController@create'))->middleware('role:admin,logistik_kantor');
		Route::post('supplier/create', 'SupplierController@store')->middleware('role:admin,logistik_kantor');
		Route::get('supplier/{supplier}/confirm-delete', array('as' => 'confirm-delete-st/supplier', 'uses' => 'SupplierController@getModalDelete'))->middleware('role:admin,logistik_kantor');
		Route::get('supplier/{supplier}/delete', array('as' => 'delete/supplier', 'uses' => 'SupplierController@destroy'))->middleware('role:admin,logistik_kantor');
		Route::get('supplier/{supplier}/edit', array('as' => 'update/supplier', 'uses' => 'SupplierController@edit'))->middleware('role:admin,logistik_kantor');
        Route::post('supplier/{supplier}/edit', 'SupplierController@update')->middleware('role:admin,logistik_kantor');
		Route::get('supplier/xlsx', 'SupplierController@exportFile')->middleware('role:admin,logistik_kantor');
		Route::get('supplier/csv', 'SupplierController@exportFile')->middleware('role:admin,logistik_kantor');
		#subcontractor
		Route::get('subcontractor', array('as' => 'subcontractor', 'uses' => 'SubcontractorController@index'))->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/data',['as' => 'master.subcontractor.data', 'uses' =>'SubcontractorController@data'])->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/create', array('as' => 'create/subcontractor', 'uses' => 'SubcontractorController@create'))->middleware('role:admin,logistik_kantor');
		Route::post('subcontractor/create', 'SubcontractorController@store')->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/{subcontractor}/confirm-delete', array('as' => 'confirm-delete-st/subcontractor', 'uses' => 'SubcontractorController@getModalDelete'))->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/{subcontractor}/delete', array('as' => 'delete/subcontractor', 'uses' => 'SubcontractorController@destroy'))->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/{subcontractor}/edit', array('as' => 'update/subcontractor', 'uses' => 'SubcontractorController@edit'))->middleware('role:admin,logistik_kantor');
        Route::post('subcontractor/{subcontractor}/edit', 'SubcontractorController@update')->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/xlsx', 'SubcontractorController@exportFile')->middleware('role:admin,logistik_kantor');
		Route::get('subcontractor/csv', 'SubcontractorController@exportFile')->middleware('role:admin,logistik_kantor');
	});
	
	# Invoice
	Route::group(array('prefix' => 'invoice'), function(){
		Route::get('/', array('as' => 'invoices', 'uses' => 'InvoiceController@index'))->middleware('role:admin,kasir,logistik_kantor,finance');
		#purchase order
		Route::get('purchase-order', array('as' => 'purchaseOrder', 'uses' => 'InvoicePurchaseOrderController@index'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('purchase-order/data',['as' => 'invoice.purchase-order.data', 'uses' =>'InvoicePurchaseOrderController@data'])->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('purchase-order/create', array('as' => 'create/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@create'))->middleware('role:admin,kasir');
		Route::post('purchase-order/create', 'InvoicePurchaseOrderController@store')->middleware('role:admin,kasir');
		Route::get('purchase-order/{invoicePurchaseOrder}/confirm-delete', array('as' => 'confirm-delete/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@getModalDelete'))->middleware('role:admin,kasir');
		Route::get('purchase-order/{invoicePurchaseOrder}/delete', array('as' => 'delete/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@destroy'))->middleware('role:admin,kasir');
		Route::get('purchase-order/{invoicePurchaseOrder}/confirm-approve', array('as' => 'confirm-approve/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@getModalApprove'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('purchase-order/{invoicePurchaseOrder}/approve', array('as' => 'approve/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@approve'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('purchase-order/{invoicePurchaseOrder}/confirm-reject', array('as' => 'confirm-reject/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@getModalReject'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::post('purchase-order/{invoicePurchaseOrder}/confirm-reject', 'InvoicePurchaseOrderController@reject')->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('purchase-order/{invoicePurchaseOrder}/edit', array('as' => 'update/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@edit'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('purchase-order/{invoicePurchaseOrder}/view', array('as' => 'update/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@edit'))->middleware('role:admin,kasir');
		Route::get('purchase-order/{invoicePurchaseOrder}/print', array('as' => 'print/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@getModalPrintInvoicePo'))->where('invoicePurchaseOrder', '(.*)')->middleware('role:admin,kasir');
		Route::post('purchase-order/{invoicePurchaseOrder}/print', array('as' => 'print/invoicePurchaseOrder', 'uses' => 'InvoicePurchaseOrderController@printInvoicePo'))->where('invoicePurchaseOrder', '(.*)')->middleware('role:admin,kasir');
        Route::post('purchase-order/{invoicePurchaseOrder}/edit', 'InvoicePurchaseOrderController@update')->middleware('role:admin,kasir');
		Route::get('xlsx', 'InvoicePurchaseOrderController@exportFile')->middleware('role:admin,kasir');
		Route::get('csv', 'InvoicePurchaseOrderController@exportFile')->middleware('role:admin,kasir');
		#service order
		Route::get('service-order', array('as' => 'serviceOrder', 'uses' => 'InvoiceServiceOrderController@index'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('service-order/data',['as' => 'invoice.service-order.data', 'uses' =>'InvoiceServiceOrderController@data'])->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('service-order/create', array('as' => 'create/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@create'))->middleware('role:admin,kasir');
		Route::post('service-order/create', 'InvoiceServiceOrderController@store')->middleware('role:admin,kasir');
		Route::get('service-order/{invoiceServiceOrder}/confirm-delete', array('as' => 'confirm-delete/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@getModalDelete'))->middleware('role:admin,kasir');
		Route::get('service-order/{invoiceServiceOrder}/delete', array('as' => 'delete/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@destroy'))->middleware('role:admin,kasir');
		Route::get('service-order/{invoiceServiceOrder}/confirm-approve', array('as' => 'confirm-approve/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@getModalApprove'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('service-order/{invoiceServiceOrder}/approve', array('as' => 'approve/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@approve'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('service-order/{invoiceServiceOrder}/confirm-reject', array('as' => 'confirm-reject/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@getModalReject'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::post('service-order/{invoiceServiceOrder}/confirm-reject', 'InvoiceServiceOrderController@reject')->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('service-order/{invoiceServiceOrder}/edit', array('as' => 'update/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@edit'))->middleware('role:admin,kasir,logistik_kantor,finance');
		Route::get('service-order/{invoiceServiceOrder}/view', array('as' => 'update/invoiceServiceOrder', 'uses' => 'InvoiceServiceOrderController@edit'))->middleware('role:admin,kasir');
        Route::post('service-order/{invoiceServiceOrder}/edit', 'InvoiceServiceOrderController@update')->middleware('role:admin,kasir');
		Route::get('xlsx', 'InvoiceServiceOrderController@exportFile')->middleware('role:admin,kasir');
		Route::get('csv', 'InvoiceServiceOrderController@exportFile')->middleware('role:admin,kasir');
	});
	
	# Client
	Route::group(array('prefix' => 'client'), function(){
		Route::get('/', array('as' => 'clients', 'uses' => 'ClientController@index'))->middleware('role:admin,administrasi');
		Route::get('data',['as' => 'client.data', 'uses' =>'ClientController@data'])->middleware('role:admin,administrasi');
		Route::get('create', array('as' => 'create/client', 'uses' => 'ClientController@create'))->middleware('role:admin,administrasi');
		Route::post('create', 'ClientController@store')->middleware('role:admin,administrasi');
		Route::get('{client}/confirm-delete', array('as' => 'confirm-delete/client', 'uses' => 'ClientController@getModalDelete'))->middleware('role:admin,administrasi');
		Route::get('{client}/delete', array('as' => 'delete/client', 'uses' => 'ClientController@destroy'))->middleware('role:admin,administrasi');
		Route::get('{client}/edit', array('as' => 'update/client', 'uses' => 'ClientController@edit'))->middleware('role:admin,administrasi');
		Route::get('{client}/view', array('as' => 'update/client', 'uses' => 'ClientController@edit'))->middleware('role:admin,administrasi');
        Route::post('{client}/edit', 'ClientController@update')->middleware('role:admin,administrasi');
		Route::get('xlsx', 'ClientController@exportFile')->middleware('role:admin,administrasi');
		Route::get('csv', 'ClientController@exportFile')->middleware('role:admin,administrasi');
	});
	
	# Project
	Route::group(array('prefix' => 'project'), function(){
		Route::get('/', array('as' => 'projects', 'uses' => 'ProjectController@index'))->middleware('role:admin,administrasi');
		Route::get('data', array('as' => 'project.data', 'uses' => 'ProjectController@data'))->middleware('role:admin,administrasi');
		Route::get('create', array('as' => 'create/project', 'uses' => 'ProjectController@create'))->middleware('role:admin,administrasi');
		Route::post('create', 'ProjectController@store')->middleware('role:admin,administrasi');
		Route::get('{project}/confirm-delete', array('as' => 'confirm-delete/project', 'uses' => 'ProjectController@getModalDelete'))->middleware('role:admin,administrasi');
		Route::get('{project}/delete', array('as' => 'delete/project', 'uses' => 'ProjectController@destroy'))->middleware('role:admin,administrasi');
		Route::get('{project}/edit', array('as' => 'update/project', 'uses' => 'ProjectController@edit'))->middleware('role:admin,administrasi');
		Route::get('{project}/view', array('as' => 'update/project', 'uses' => 'ProjectController@edit'))->middleware('role:admin,administrasi');
        Route::post('{project}/edit', 'ProjectController@update')->middleware('role:admin,administrasi');
		Route::get('xlsx', 'ProjectController@exportFile')->middleware('role:admin,administrasi');
		Route::get('csv', 'ProjectController@exportFile')->middleware('role:admin,administrasi');
	});
	
	# RAB
	Route::group(array('prefix' => 'rab'), function(){
		Route::get('/', array('as' => 'rabs', 'uses' => 'RabController@index'))->middleware('role:admin,qs');
		Route::get('data', array('as' => 'rab.data', 'uses' => 'RabController@data'))->middleware('role:admin,qs');
		Route::get('create', array('as' => 'create/rab', 'uses' => 'RabController@create'))->middleware('role:admin,qs');
		Route::post('create', 'RabController@store')->middleware('role:admin,qs');
		Route::get('{rab}/confirm-delete', array('as' => 'confirm-delete/rab', 'uses' => 'RabController@getModalDelete'))->middleware('role:admin');
		Route::get('{rab}/delete', array('as' => 'delete/rab', 'uses' => 'RabController@destroy'))->middleware('role:admin');
		Route::get('{rab}/edit', array('as' => 'update/rab', 'uses' => 'RabController@edit'))->middleware('role:admin');
		Route::get('{rab}/view', array('as' => 'update/rab', 'uses' => 'RabController@edit'))->middleware('role:admin');
        Route::post('{rab}/edit', 'RabController@update')->middleware('role:admin');
		Route::get('xlsx', 'RabController@exportFile')->middleware('role:admin,qs');
		Route::get('csv', 'RabController@exportFile')->middleware('role:admin,qs');
	});
	
	# RAB Item
	Route::group(array('prefix' => 'rab-item'), function(){
		Route::get('/', array('as' => 'rabItems', 'uses' => 'RabItemController@index'))->middleware('role:admin,qs');
		Route::get('data', array('as' => 'rab-item.data', 'uses' => 'RabItemController@data'))->middleware('role:admin,qs');
		Route::get('create', array('as' => 'create/rabItem', 'uses' => 'RabItemController@create'))->middleware('role:admin,qs');
		Route::post('create', 'RabItemController@store')->middleware('role:admin,qs');
		Route::get('{rabItem}/confirm-delete', array('as' => 'confirm-delete/rabItem', 'uses' => 'RabItemController@getModalDelete'))->middleware('role:admin');
		Route::get('{rabItem}/delete', array('as' => 'delete/rabItem', 'uses' => 'RabItemController@destroy'))->middleware('role:admin');
		Route::get('{rabItem}/edit', array('as' => 'update/rabItem', 'uses' => 'RabItemController@edit'))->middleware('role:admin');
        Route::post('{rabItem}/edit', 'RabItemController@update')->middleware('role:admin');
		Route::get('xlsx', 'RabItemController@exportFile')->middleware('role:admin,qs');
		Route::get('csv', 'RabItemController@exportFile')->middleware('role:admin,qs');
	});
	
	# Purchase Order
	Route::group(array('prefix' => 'purchase-order'), function(){
		Route::get('/', array('as' => 'purchaseOrders', 'uses' => 'PurchaseOrderController@index'))->middleware('role:admin,logistik_kantor');
		Route::get('data', array('as' => 'purchase-order.data', 'uses' => 'PurchaseOrderController@data'))->middleware('role:admin,logistik_kantor');
		Route::get('create', array('as' => 'create/purchaseOrder', 'uses' => 'PurchaseOrderController@create'))->middleware('role:admin,logistik_kantor');
		Route::post('create', 'PurchaseOrderController@store')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/confirm-delete', array('as' => 'confirm-delete/purchaseOrder', 'uses' => 'PurchaseOrderController@getModalDelete'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/delete', array('as' => 'delete/purchaseOrder', 'uses' => 'PurchaseOrderController@destroy'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/confirm-approve', array('as' => 'confirm-approve/purchaseOrder', 'uses' => 'PurchaseOrderController@getModalApprove'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/approve', array('as' => 'approve/purchaseOrder', 'uses' => 'PurchaseOrderController@approve'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/confirm-reject', array('as' => 'confirm-reject/purchaseOrder', 'uses' => 'PurchaseOrderController@getModalReject'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::post('{purchaseOrder}/confirm-reject', 'PurchaseOrderController@reject')->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/edit', array('as' => 'update/purchaseOrder', 'uses' => 'PurchaseOrderController@edit'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/print', array('as' => 'print/purchaseOrder', 'uses' => 'PurchaseOrderController@getModalPrintPo'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::post('{purchaseOrder}/print', array('as' => 'print/purchaseOrder', 'uses' => 'PurchaseOrderController@printPo'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('{purchaseOrder}/view', array('as' => 'update/purchaseOrder', 'uses' => 'PurchaseOrderController@edit'))->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
        Route::post('{purchaseOrder}/edit', 'PurchaseOrderController@update')->where('purchaseOrder', '(.*)')->middleware('role:admin,logistik_kantor');
		Route::get('xlsx', 'PurchaseOrderController@exportFile')->middleware('role:admin,logistik_kantor');
		Route::get('csv', 'PurchaseOrderController@exportFile')->middleware('role:admin,logistik_kantor');
	});
	
	# Service Order
	Route::group(array('prefix' => 'service-order'), function(){
		Route::get('/', array('as' => 'serviceOrders', 'uses' => 'ServiceOrderController@index'))->middleware('role:admin');
		Route::get('data', array('as' => 'service-order.data', 'uses' => 'ServiceOrderController@data'))->middleware('role:admin');
		Route::get('create', array('as' => 'create/serviceOrder', 'uses' => 'ServiceOrderController@create'))->middleware('role:admin');
		Route::post('create', 'ServiceOrderController@store')->middleware('role:admin');
		Route::get('{serviceOrder}/confirm-delete', array('as' => 'confirm-delete/serviceOrder', 'uses' => 'ServiceOrderController@getModalDelete'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/delete', array('as' => 'delete/serviceOrder', 'uses' => 'ServiceOrderController@destroy'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/confirm-approve', array('as' => 'confirm-approve/serviceOrder', 'uses' => 'ServiceOrderController@getModalApprove'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/approve', array('as' => 'approve/serviceOrder', 'uses' => 'ServiceOrderController@approve'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/confirm-reject', array('as' => 'confirm-reject/serviceOrder', 'uses' => 'ServiceOrderController@getModalReject'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::post('{serviceOrder}/confirm-reject', 'ServiceOrderController@reject')->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/edit', array('as' => 'update/serviceOrder', 'uses' => 'ServiceOrderController@edit'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/view', array('as' => 'update/serviceOrder', 'uses' => 'ServiceOrderController@edit'))->where('serviceOrder', '(.*)')->middleware('role:admin');
        Route::post('{serviceOrder}/edit', 'ServiceOrderController@update')->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('{serviceOrder}/print', array('as' => 'print/serviceOrder', 'uses' => 'ServiceOrderController@getModalPrintPo'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::post('{serviceOrder}/print', array('as' => 'print/serviceOrder', 'uses' => 'ServiceOrderController@printPo'))->where('serviceOrder', '(.*)')->middleware('role:admin');
		Route::get('xlsx', 'ServiceOrderController@exportFile')->middleware('role:admin');
		Route::get('csv', 'ServiceOrderController@exportFile')->middleware('role:admin');
	});
	
	# Cash Expense
	Route::group(array('prefix' => 'cash-expense'), function(){
		Route::get('/', array('as' => 'cashExpenses', 'uses' => 'CashExpenseController@index'))->middleware('role:admin,kasir');
		Route::get('data', array('as' => 'cash-expense.data', 'uses' => 'CashExpenseController@data'))->middleware('role:admin,kasir');
		Route::get('create', array('as' => 'create/cashExpense', 'uses' => 'CashExpenseController@create'))->middleware('role:admin,kasir');
		Route::get('data', array('as' => 'cash-expense.data', 'uses' => 'CashExpenseController@data'))->middleware('role:admin,kasir');
		Route::post('create', 'CashExpenseController@store')->middleware('role:admin,kasir');
		Route::get('{cashExpense}/confirm-delete', array('as' => 'confirm-delete/cashExpense', 'uses' => 'CashExpenseController@getModalDelete'))->middleware('role:admin,kasir');
		Route::get('{cashExpense}/delete', array('as' => 'delete/cashExpense', 'uses' => 'CashExpenseController@destroy'))->middleware('role:admin,kasir');
		Route::get('{cashExpense}/confirm-approve', array('as' => 'confirm-approve/cashExpense', 'uses' => 'CashExpenseController@getModalApprove'))->middleware('role:admin,kasir');
		Route::get('{cashExpense}/approve', array('as' => 'approve/cashExpense', 'uses' => 'CashExpenseController@approve'))->middleware('role:admin,kasir');
		Route::get('{cashExpense}/confirm-reject', array('as' => 'confirm-reject/cashExpense', 'uses' => 'CashExpenseController@getModalReject'))->middleware('role:admin,kasir');
		Route::post('{cashExpense}/confirm-reject', 'CashExpenseController@reject')->middleware('role:admin,kasir');
		Route::get('{cashExpense}/edit', array('as' => 'update/cashExpense', 'uses' => 'CashExpenseController@edit'))->middleware('role:admin,kasir');
		Route::get('{cashExpense}/view', array('as' => 'update/cashExpense', 'uses' => 'CashExpenseController@edit'))->middleware('role:admin,kasir');
        Route::post('{cashExpense}/edit', 'CashExpenseController@update')->middleware('role:admin,kasir');
		Route::get('{cashExpense}/print', array('as' => 'print/cashExpense', 'uses' => 'CashExpenseController@getModalPrintCashExpense'))->where('cashExpense', '(.*)')->middleware('role:admin,kasir');
		Route::post('{cashExpense}/print', array('as' => 'print/cashExpense', 'uses' => 'CashExpenseController@printCe'))->where('cashExpense', '(.*)')->middleware('role:admin,kasir');
		Route::get('xlsx', 'CashExpenseController@exportFile')->middleware('role:admin,kasir');
		Route::get('csv', 'CashExpenseController@exportFile')->middleware('role:admin,kasir');
	});
	
	# Purchase Order Item
	/* Route::group(array('prefix' => 'purchase-order-item'), function(){
		Route::get('/', array('as' => 'purchaseOrderItems', 'uses' => 'PurchaseOrderItemController@index'));
		Route::get('create', array('as' => 'create/purchaseOrderItem', 'uses' => 'PurchaseOrderItemController@create'));
		Route::post('create', 'PurchaseOrderItemController@store');
		Route::get('{purchaseOrderItem}/confirm-delete', array('as' => 'confirm-delete/purchaseOrderItem', 'uses' => 'PurchaseOrderItemController@getModalDelete'));
		Route::get('{purchaseOrderItem}/delete', array('as' => 'delete/purchaseOrderItem', 'uses' => 'PurchaseOrderItemController@destroy'));
		Route::get('{purchaseOrderItem}/edit', array('as' => 'update/purchaseOrderItem', 'uses' => 'PurchaseOrderItemController@edit'));
        Route::post('{purchaseOrderItem}/edit', 'PurchaseOrderItemController@update');
		Route::get('xlsx', 'PurchaseOrderItemController@exportFile');
		Route::get('csv', 'PurchaseOrderItemController@exportFile');
	}); */
	
	# Delivery
	Route::group(['prefix' => 'delivery-receipt'], function(){
		Route::get('/', 'DeliveryReceiptController@index')->middleware('role:admin,logistik_lapangan');
		Route::get('data', 'DeliveryReceiptController@data')->middleware('role:admin,logistik_lapangan');
		Route::get('create', 'DeliveryReceiptController@create')->middleware('role:admin,logistik_lapangan');
		Route::post('create', 'DeliveryReceiptController@store')->middleware('role:admin,logistik_lapangan');
		Route::get('{deliveryReceipt}/confirm-delete', ['as' => 'confirm-delete/deliveryReceipt', 'uses' => 'DeliveryReceiptController@getModalDelete'])->middleware('role:admin');
		Route::get('{deliveryReceipt}/delete', ['as' => 'delete/deliveryReceipt', 'uses' => 'DeliveryReceiptController@destroy'])->middleware('role:admin');
		Route::get('{deliveryReceipt}/edit', ['as' => 'update/deliveryReceipt', 'uses' => 'DeliveryReceiptController@edit'])->middleware('role:admin');
		Route::get('{deliveryReceipt}/view', ['as' => 'update/deliveryReceipt', 'uses' => 'DeliveryReceiptController@edit'])->middleware('role:admin');
        Route::post('{deliveryReceipt}/edit', 'DeliveryReceiptController@update')->middleware('role:admin');
		Route::get('{deliveryReceipt}/print', ['as' => 'print/deliveryReceipt', 'uses' => 'DeliveryReceiptController@getModalPrintDeliveryReceipt'])->where('deliveryReceipt', '(.*)')->middleware('role:admin');
		Route::post('{deliveryReceipt}/print', ['as' => 'print/deliveryReceipt', 'uses' => 'DeliveryReceiptController@printDr'])->where('deliveryReceipt', '(.*)')->middleware('role:admin');
		Route::get('xlsx', 'DeliveryReceiptController@exportFile')->middleware('role:admin,logistik_lapangan');
		Route::get('csv', 'DeliveryReceiptController@exportFile')->middleware('role:admin,logistik_lapangan');
	});
	
	#ajax autocomplete
	Route::get('masterDataSearchAjax',array('as'=>'masterDataSearchAjax','uses'=>'RabItemController@ajaxMaster'));
	Route::get('masterDataSearchAjaxSelectOne',array('as'=>'masterDataSearchAjaxSelectOne','uses'=>'AjaxController@masterDataSearchAjaxSelectOne'));
	Route::get('masterDataAjaxGetUnit',array('as'=>'masterDataAjaxGetUnit','uses'=>'AjaxController@masterDataAjaxGetUnit'));
	Route::get('rabDataSearchAjax',array('as'=>'rabDataSearchAjax','uses'=>'PurchaseOrderController@ajaxRab'));
	Route::get('supplierDataSearchAjax',array('as'=>'supplierDataSearchAjax','uses'=>'PurchaseOrderController@ajaxSupplier'));
	Route::get('purchaseOrderDataSearchAjax',array('as'=>'purchaseOrderDataSearchAjax','uses'=>'AjaxController@ajaxPurchaseOrder'));
	Route::get('purchaseOrderItemDataSearchAjax',array('as'=>'purchaseOrderItemDataSearchAjax','uses'=>'AjaxController@ajaxPurchaseOrderItem'));
	Route::get('purchaseOrderOnDeliveryReceipt',array('as'=>'purchaseOrderOnDeliveryReceipt','uses'=>'AjaxController@ajaxPurchaseOrderOnDeliveryReceipt'));
	Route::get('serviceOrderDataSearchAjax',array('as'=>'serviceOrderDataSearchAjax','uses'=>'AjaxController@ajaxServiceOrder'));
	Route::get('deliveryItemChangeAjax',array('as'=>'deliveryItemChangeAjax','uses'=>'AjaxController@ajaxDeliveryItemChange'));
	Route::get('serviceOrderItemChangeAjax',array('as'=>'serviceOrderItemChangeAjax','uses'=>'AjaxController@ajaxServiceOrderItemChange'));
	Route::get('poDRItemDataSearchAjax',array('as'=>'poDRItemDataSearchAjax','uses'=>'AjaxController@ajaxPODRItem'));
	Route::get('serviceTypeDataSearchAjax',array('as'=>'serviceTypeDataSearchAjax','uses'=>'ServiceMasterController@ajaxServiceType'));
	Route::get('clientDataSearchAjax',array('as'=>'clientDataSearchAjax','uses'=>'ProjectController@ajaxClient'));
	Route::get('projectDataSearchAjax',array('as'=>'projectDataSearchAjax','uses'=>'ProjectController@ajaxProject'));
	Route::get('subcontractorDataSearchAjax',array('as'=>'subcontractorDataSearchAjax','uses'=>'SubcontractorController@ajaxSubcontractor'));
	
	Route::get('deliveryPOChangeAjax',array('as'=>'deliveryPOChangeAjax','uses'=>'DeliveryReceiptController@ajaxPOChange'));
	Route::get('rabItemMasterDataSearchAjax',array('as'=>'rabItemMasterDataSearchAjax','uses'=>'AjaxController@ajaxRabItemMaster'));
	Route::get('poDRItemChangeAjax',array('as'=>'poDRItemChangeAjax','uses'=>'AjaxController@ajaxPODRItemChange'));
	/* ==============================================================================================================================s */
});
#==================================================End Admin Routes===============================================
#frontend views
Route::get('/', array('as' => 'home','uses' => 'JoshController@showHome'));
# End of frontend views