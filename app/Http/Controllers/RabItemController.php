<?php

namespace App\Http\Controllers;

use App\RabItem;
use App\Rab;
use App\MasterData;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;

class RabItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataRabItem = RabItem::select('rab_item.*', 'rab.name as rab_name', 'users.first_name', 'users.last_name', 'master_data.name')
										->join('users', 'users.id', '=', 'rab_item.updated_by')
										->join('rab', 'rab.id', '=', 'rab_item.rab_id')
										->join('master_data', 'master_data.id', '=', 'rab_item.master_data_id')
										->where('rab_item.deleted', '=', 0)
										->get();
		return view('admin.rabItem.index', compact('dataRabItem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '7'.date('md').mt_rand(1000,9999);
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$master_data = array();//MasterData::where('deleted', 0)->lists('name', 'id');
        return view('admin.rabItem.create', compact('id','rab','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $rabItem = new RabItem($request->all());
		$rabItem->unit_price = preg_replace('/[\D.]/', '', str_replace('.00','',$rabItem->unit_price));
		$rabItem->unit_total_price = preg_replace('/[\D.]/', '', str_replace('.00','',$rabItem->unit_total_price));
		$rabItem->updated_by = Sentinel::getUser()->id;
		
        if ($rabItem->save()) {
            return redirect('admin/rab-item')->with('success', trans('rabItem/message.success.create'));
        } else {
            return Redirect::route('admin/rab-item')->withInput()->with('error', trans('rabItem/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(RabItem $rabItem)
    {
        $model = 'rabItem';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/rabItem', ['id' => $rabItem->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('rabItem/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RabItem $rabItem)
    {
		$rabItem->deleted = 1;
        if ($rabItem->update($rabItem->toArray())) {
            return redirect('admin/rab-item')->with('success', trans('rabItem/message.success.delete'));
        } else {
            return Redirect::route('admin/rab-item')->withInput()->with('error', trans('rabItem/message.error.delete'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RabItem $rabItem)
    {
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$master_data = MasterData::where('deleted', 0)->where('id', $rabItem->master_data_id)->lists('name', 'id');
        return view('admin.rabItem.edit', compact('master_data', 'rab', 'rabItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, rabItem $rabItem)
    {
		$request->merge(array('unit_price' => preg_replace('/[\D.]/', '', str_replace('.00','',$request->unit_price))));
		$request->merge(array('unit_total_price' => preg_replace('/[\D.]/', '', str_replace('.00','',$request->unit_total_price))));
		$request->merge(array('updated_by' => Sentinel::getUser()->id));
		
        if ($rabItem->update($request->all())) {
            return redirect('admin/rab-item')->with('success', trans('rabItem/message.success.update'));
        } else {
            return Redirect::route('admin/rab-item')->withInput()->with('error', trans('rabItem/message.error.update'));
        }
    }
	
	public function ajaxMaster(AllRabRequest $request)
	{
		$term = $request->term;
		$results = MasterData::select('id', DB::raw("CONCAT(name,' - ',CASE WHEN (master_no = 1) THEN 'Item Pekerjaan Subkon' ELSE (CASE WHEN (master_no = 2) THEN 'Cash Expense' ELSE 'Inventory' END) END) as text"))
										->where('deleted', '=', 0)
										->where('name', 'LIKE', '%'.$term.'%')
										->orderBy('name', 'asc')
										->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = RabItem::select('rab_item.id as ID', 
									'rab.name as RAB_Name',
									'master_data.name as Master_Name',
									'rab_item.quantity as Qty',
									'rab_item.unit_price as Price',
									DB::raw("ROUND(REPLACE(rab_item.unit_total_price, ',', ''),3) as Total_Price"),
									DB::raw("DATE_FORMAT(rab_item.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(rab_item.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('master_data', 'master_data.id', '=', 'rab_item.master_data_id')
							->join('rab', 'rab.id', '=', 'rab_item.rab_id')
							->join('users', 'users.id', '=', 'rab_item.updated_by')
							->where(array('rab_item.deleted' => 0))
							->get();
										
		Excel::create('rab-item-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}
