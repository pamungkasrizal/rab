<?php
namespace App\Http\Controllers;

use App\MasterData;
use App\ServiceType;
use Illuminate\Http\Request;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\MasterDataRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class ServiceMasterController extends JoshController
{
    public function index()
	{
		return view('admin.master.serviceMaster.index');
	}
	
	public function data()
    {
        $data = MasterData::select('master_data.*', 'service_type.service_type_name', 'users.first_name', 'users.last_name')
										->join('service_type', 'service_type.id', '=', 'master_data.service_type')
										->join('users', 'users.id', '=', 'master_data.updated_by')
										->where(array('master_data.deleted' => 0, 'master_data.master_no' => 1))
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('checkbox',function(MasterData $data) {
				return '<input type="checkbox" name="idList" value="' .$data->id. '">';
            })
			->edit_column('created_at',function(MasterData $data) {
				return $data->created_at->toDayDateTimeString();
            })
			->edit_column('updated_at',function(MasterData $data) {
				return ($data->updated_at ? $data->updated_at->toDayDateTimeString() : '-');
            })
            ->add_column('actions',function($data) {
				$actions = '<a href="'.URL::to('admin/master/service-master/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete-sm/masterData', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
                return $actions;
				
            })->make(true);
		return $tables;
    }
	
	/* Add */
	public function create()
	{
		$id = '1'.date('md').mt_rand(1000,9999);
		$servicetype = array();//ServiceType::where('deleted', 0)->lists('service_type_name', 'id');
		return view('admin.master.serviceMaster.create', compact('servicetype','id'));
	}
	
	public function store(MasterDataRequest $request)
    {
        $masterData = new MasterData($request->all());
		$masterData->updated_by = Sentinel::getUser()->id;
		$masterData->master_no = 1;
		unset($masterData['rulesValidation']);
		
        if ($masterData->save()) {
            return redirect('admin/master/service-master')->with('success', trans('masterdata/serviceMaster/message.success.create'));
        } else {
            return Redirect::route('admin/master/service-master')->withInput()->with('error', trans('masterdata/serviceMaster/message.error.create'));
        }
    }
	
	/* Delete */
	public function getModalDelete($id)
    {
        $model = 'masterdata/serviceMaster';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/masterServiceMaster', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('masterdata/serviceMaster/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function destroy($id)
    {
		$id = explode('-', $id);
        if (DB::table('master_data')->whereIn('id', $id)->update(['deleted' => 1])) {
            return redirect('admin/master/service-master')->with('success', trans('masterdata/serviceMaster/message.success.delete'));
        } else {
            return Redirect::route('admin/master/service-master')->withInput()->with('error', trans('masterdata/serviceMaster/message.error.delete'));
        }
    }
	
	/* Update */
	public function edit(MasterData $masterData)
    {
		$servicetype = ServiceType::lists('service_type_name', 'id');
        return view('admin.master.serviceMaster.edit', compact('masterData','servicetype'));
    }
	
	public function update(MasterDataRequest $request, MasterData $masterData)
    {
		$masterData->updated_by = Sentinel::getUser()->id;
		
        if ($masterData->update($request->all())) {
            return redirect('admin/master/service-master')->with('success', trans('masterdata/serviceMaster/message.success.update'));
        } else {
            return Redirect::route('admin/master/service-master')->withInput()->with('error', trans('masterdata/serviceMaster/message.error.update'));
        }
    }
	
	public function ajaxServiceType(MasterDataRequest $request)
	{
		$term = $request->term;
		$results = ServiceType::select('id', 'service_type_name as text')
										->where('deleted', '=', 0)
										->where('service_type_name', 'LIKE', '%'.$term.'%')
										->orderBy('service_type_name', 'asc')
										->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = MasterData::select('master_data.id as ID', 
									'master_data.name as Name',
									'service_type.service_type_name as Jenis Pekerjaan Subkon',
									DB::raw("DATE_FORMAT(master_data.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(master_data.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('service_type', 'service_type.id', '=', 'master_data.updated_by')
							->join('users', 'users.id', '=', 'master_data.updated_by')
							->where(array('master_data.deleted' => 0, 'master_data.master_no' => 1))
							->get();
										
		Excel::create('service-master-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}