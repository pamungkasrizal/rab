<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Requests\CreateNyobaRequest;
use App\Http\Requests\UpdateNyobaRequest;
use App\Repositories\NyobaRepository;
use App\Http\Controllers\AppBaseController as InfyOmBaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Lang;
use App\Models\Nyoba;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class NyobaController extends InfyOmBaseController
{
    /** @var  NyobaRepository */
    private $nyobaRepository;

    public function __construct(NyobaRepository $nyobaRepo)
    {
        $this->nyobaRepository = $nyobaRepo;
    }

    /**
     * Display a listing of the Nyoba.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->nyobaRepository->pushCriteria(new RequestCriteria($request));
        $nyobas = $this->nyobaRepository->all();

        return view('admin.nyobas.index')
            ->with('nyobas', $nyobas);
    }

    /**
     * Show the form for creating a new Nyoba.
     *
     * @return Response
     */
    public function create()
    {
        return view('admin.nyobas.create');
    }

    /**
     * Store a newly created Nyoba in storage.
     *
     * @param CreateNyobaRequest $request
     *
     * @return Response
     */
    public function store(CreateNyobaRequest $request)
    {
        $input = $request->all();

        $nyoba = $this->nyobaRepository->create($input);

        Flash::success('Nyoba saved successfully.');

        return redirect(route('admin.nyobas.index'));
    }

    /**
     * Display the specified Nyoba.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $nyoba = $this->nyobaRepository->findWithoutFail($id);

        if (empty($nyoba)) {
            Flash::error('Nyoba not found');

            return redirect(route('nyobas.index'));
        }

        return view('admin.nyobas.show')->with('nyoba', $nyoba);
    }

    /**
     * Show the form for editing the specified Nyoba.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $nyoba = $this->nyobaRepository->findWithoutFail($id);

        if (empty($nyoba)) {
            Flash::error('Nyoba not found');

            return redirect(route('nyobas.index'));
        }

        return view('admin.nyobas.edit')->with('nyoba', $nyoba);
    }

    /**
     * Update the specified Nyoba in storage.
     *
     * @param  int              $id
     * @param UpdateNyobaRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateNyobaRequest $request)
    {
        $nyoba = $this->nyobaRepository->findWithoutFail($id);

        if (empty($nyoba)) {
            Flash::error('Nyoba not found');

            return redirect(route('nyobas.index'));
        }

        $nyoba = $this->nyobaRepository->update($request->all(), $id);

        Flash::success('Nyoba updated successfully.');

        return redirect(route('admin.nyobas.index'));
    }

    /**
     * Remove the specified Nyoba from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
      public function getModalDelete($id = null)
      {
          $error = '';
          $model = '';
          $confirm_route =  route('admin.nyobas.delete',['id'=>$id]);
          return View('admin.layouts/modal_confirmation', compact('error','model', 'confirm_route'));

      }

       public function getDelete($id = null)
       {
           $sample = Nyoba::destroy($id);

           // Redirect to the group management page
           return redirect(route('admin.nyobas.index'))->with('success', Lang::get('message.success.delete'));

       }

}
