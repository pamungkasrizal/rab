<?php
namespace App\Http\Controllers;

use App\ServiceType;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\MasterDataRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class ServiceTypeController extends JoshController
{
    public function index()
	{
		return view('admin.master.serviceType.index');
	}
	
	public function data()
    {
        $data = ServiceType::select('service_type.*', 'users.first_name', 'users.last_name')
										->join('users', 'users.id', '=', 'service_type.updated_by')
										->where('service_type.deleted', '=', 0)
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('checkbox',function(ServiceType $data) {
				return '<input type="checkbox" name="idList" value="' .$data->id. '">';
            })
			->edit_column('created_at',function(ServiceType $data) {
				return $data->created_at->toDayDateTimeString();
            })
			->edit_column('updated_at',function(ServiceType $data) {
				return ($data->updated_at ? $data->updated_at->toDayDateTimeString() : '-');
            })
            ->add_column('actions',function($data) {
				$actions = '<a href="'.URL::to('admin/master/service-type/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete-st/serviceType', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
                return $actions;
				
            })->make(true);
		return $tables;
    }
	
	/* Add */
	public function create()
	{
		return view('admin.master.serviceType.create');
	}
	
	public function store(MasterDataRequest $request)
    {
        $serviceType = new ServiceType($request->all());
		$serviceType->updated_by = Sentinel::getUser()->id;
		
        if ($serviceType->save()) {
            return redirect('admin/master/service-type')->with('success', trans('masterdata/serviceType/message.success.create'));
        } else {
            return Redirect::route('admin/master/service-type')->withInput()->with('error', trans('masterdata/serviceType/message.error.create'));
        }
    }
	
	/* Delete */
	public function getModalDelete($id)
    {
        $model = 'masterdata/serviceType';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/serviceType', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('masterdata/serviceType/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function destroy($id)
    {
		$id = explode('-', $id);
        if (ServiceType::whereIn('id', $id)->update(['deleted' => 1])) {
            return redirect('admin/master/service-type')->with('success', trans('masterdata/serviceType/message.success.delete'));
        } else {
            return Redirect::route('admin/master/service-type')->withInput()->with('error', trans('masterdata/serviceType/message.error.delete'));
        }
    }
	
	/* Update */
	public function edit(ServiceType $serviceType)
    {
        return view('admin.master.serviceType.edit', compact('serviceType'));
    }
	
	public function update(MasterDataRequest $request, ServiceType $serviceType)
    {
		$serviceType->updated_by = Sentinel::getUser()->id;
		
        if ($serviceType->update($request->all())) {
            return redirect('admin/master/service-type')->with('success', trans('masterdata/serviceType/message.success.update'));
        } else {
            return Redirect::route('admin/master/service-type')->withInput()->with('error', trans('masterdata/serviceType/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = ServiceType::select('service_type.id as ID', 
									'service_type.service_type_name as Service_Type',
									DB::raw("DATE_FORMAT(service_type.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(service_type.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('users', 'users.id', '=', 'service_type.updated_by')
							->where(array('service_type.deleted' => 0))
							->get();
										
		Excel::create('service-type-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}