<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItem;
use App\DeliveryReceipt;
use App\DeliveryReceiptItem;
use App\InvoicePurchaseOrder;
use App\InvoicePurchaseOrderItem;
use App\Inventory;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use PDF;
use URL;
use View;
use Datatables;
use Redirect;

class InvoicePurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
		
	} 
	 
    public function index()
    {
		return view('admin.invoicePurchaseOrder.index');
    }
	
	public function data()
    {
        $data = InvoicePurchaseOrder::select('invoice_purchase_order.*')
										->where('invoice_purchase_order.deleted', '=', '0')
										->orderBy('invoice_purchase_order.created_at', 'DESC')
										->get();
		
        $tables = Datatables::of($data)
            ->add_column('actions',function($data) {
				
				if(Sentinel::inRole('admin')){
				$actions = '<a href="'.route('print/invoicePurchaseOrder', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="print data">
								   <i class="glyphicon glyphicon-print" style="width: 18px; height: 18px;color: #337ab7;top: -2px !important;" title="print po"></i>
								</a>
							<a href="'.URL::to('admin/invoice/purchase-order/' . $data->id . '/edit' ).'" title="update/approve data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete/invoicePurchaseOrder', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
				}else{
				$actions = '<a href="'.URL::to('admin/invoice/purchase-order/' . $data->id . '/view' ).'" title="update data">
								<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
							</a>';
				}
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '14'.date('md').mt_rand(1000,9999);
		$po = array();
        return view('admin.invoicePurchaseOrder.create', compact('id','po'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $invoicePurchaseOrder = new InvoicePurchaseOrder($request->all());
		$invoicePurchaseOrder->created_by = Sentinel::getUser()->id;
		$invoicePurchaseOrder->requested_at = date('Y-m-d H:i:s'); // lap or log
		$invoicePurchaseOrder->requested_by = Sentinel::getUser()->id; // lap or log
		$invoice_id = $invoicePurchaseOrder->invoice_number;
		
        if ($invoicePurchaseOrder->save()) {
			if(isset($request->invoice_purchase_order_item)){
				$countStatusPO = array();
				foreach($request->invoice_purchase_order_item as $data)
				{ 
					//save InvoicePurchaseOrderItem
					$poItemId = '15'.date('md').mt_rand(1000,9999);
					
					$invoicePurchaseOrderItem = new InvoicePurchaseOrderItem($data);
					$invoicePurchaseOrderItem->id = $poItemId;
					$invoicePurchaseOrderItem->invoice_id = $invoice_id;
					$invoicePurchaseOrderItem->created_by = Sentinel::getUser()->id;
					$invoicePurchaseOrderItem->save();
				}
			}
            return redirect('admin/invoice/purchase-order')->with('success', trans('invoicePurchaseOrder/message.success.create'));
        } else {
            return Redirect::route('admin/invoice/purchase-order')->withInput()->with('error', trans('invoicePurchaseOrder/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(InvoicePurchaseOrder $invoicePurchaseOrder)
    {
        $model = 'invoicePurchaseOrder';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/invoicePurchaseOrder', ['id' => $invoicePurchaseOrder->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoicePurchaseOrder/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function getModalApprove(InvoicePurchaseOrder $invoicePurchaseOrder)
    {
        $model = 'invoicePurchaseOrder';
        $tittle = 'Kuitansi Purchase Order';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('approve/invoicePurchaseOrder', ['id' => $invoicePurchaseOrder->id]);
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoicePurchaseOrder/message.error.approve', compact('id'));
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        }
    }
	
	public function getModalReject(InvoicePurchaseOrder $invoicePurchaseOrder)
    {
        $model = 'invoicePurchaseOrder';
        $confirm_route = $error = null;
        try {
            return view('admin.layouts.modal_reject_io', compact('error', 'model', 'confirm_route', 'invoicePurchaseOrder'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoicePurchaseOrder/message.error.reject', compact('id'));
            return view('admin.layouts.modal_reject', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoicePurchaseOrder $invoicePurchaseOrder)
    {
		$invoicePurchaseOrder->deleted = 1;
        if ($invoicePurchaseOrder->update($invoicePurchaseOrder->toArray())) {
            return redirect('admin/invoice/purchase-order')->with('success', trans('invoicePurchaseOrder/message.success.delete'));
        } else {
            return Redirect::route('admin/invoice/purchase-order')->withInput()->with('error', trans('invoicePurchaseOrder/message.error.delete'));
        }
    }
	
	public function approve(InvoicePurchaseOrder $invoicePurchaseOrder)
    {
		$invoicePurchaseOrder->status = 1;
		$invoicePurchaseOrder->status_msg = '';
		$invoicePurchaseOrder->approved_by = Sentinel::getUser()->id;
		$invoicePurchaseOrder->approved_at = date('Y-m-d H:i:s');
        if ($invoicePurchaseOrder->update($invoicePurchaseOrder->toArray())) {
            return redirect('admin/invoice/purchase-order')->with('success', trans('invoicePurchaseOrder/message.success.approve'));
        } else {
            return Redirect::route('admin/invoice/purchase-order')->withInput()->with('error', trans('invoicePurchaseOrder/message.error.approve'));
        }
    }
	
	public function reject(AllRabRequest $request, InvoicePurchaseOrder $invoicePurchaseOrder)
    {
		$invoicePurchaseOrder->status = 2;
		$invoicePurchaseOrder->status_msg = $request->status_msg;
        if ($invoicePurchaseOrder->update($invoicePurchaseOrder->toArray())) {
            return redirect('admin/invoice/purchase-order')->with('success', trans('invoicePurchaseOrder/message.success.reject'));
        } else {
            return Redirect::route('admin/invoice/purchase-order')->withInput()->with('error', trans('invoicePurchaseOrder/message.error.reject'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoicePurchaseOrder $invoicePurchaseOrder)
    {
		$po = PurchaseOrder::where('id', $invoicePurchaseOrder->purchase_order_id)->lists('id', 'id');
		$podrId = array();	
		
		$invoicePurchaseOrderItem = InvoicePurchaseOrderItem::select('invoice_purchase_order_item.id',
										'invoice_purchase_order_item.delivery_receipt_item_id', 
										'invoice_purchase_order_item.quantity', 
										'invoice_purchase_order_item.total_price', 
										DB::raw('CONCAT(delivery_receipt_item.delivery_receipt_id, " / ", master_data.name) AS text'),
										'delivery_receipt_item.quantity AS qty',
										'purchase_order_item.unit_price')
										->where('invoice_purchase_order_item.invoice_id', $invoicePurchaseOrder->invoice_number)
										->join('delivery_receipt_item', 'delivery_receipt_item.id', '=', 'invoice_purchase_order_item.delivery_receipt_item_id')
										->join('purchase_order_item', 'purchase_order_item.id', '=', 'delivery_receipt_item.purchase_order_item_id')
										->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
										->orderBy('invoice_purchase_order_item.id', 'asc')
										->get();
        return view('admin.invoicePurchaseOrder.edit', compact('po', 'invoicePurchaseOrder', 'invoicePurchaseOrderItem', 'podrId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, InvoicePurchaseOrder $invoicePurchaseOrder)
    {
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		$request->merge(array('requested_at' => date('Y-m-d H:i:s')));
		
		$invoice_id = $invoicePurchaseOrder->invoice_number;
		
		if ($invoicePurchaseOrder->update($request->all())) {
			if(isset($request->invoice_purchase_order_item)){
				$poItemId = array();
				foreach($request->invoice_purchase_order_item as $data)
				{ //save PurchaseOrderItem
					$invoicePurchaseOrderItem = new InvoicePurchaseOrderItem($data);
					
					if(isset($invoicePurchaseOrderItem->id)){
						array_push($poItemId, $invoicePurchaseOrderItem->id);
					}else{
						$invoicePurchaseOrderItem->id = '15'.date('md').mt_rand(1000,9999);
						array_push($poItemId, $invoicePurchaseOrderItem->id);
						$invoicePurchaseOrderItem->invoice_id = $invoice_id;
						$invoicePurchaseOrderItem->created_by = Sentinel::getUser()->id;
						$invoicePurchaseOrderItem->save();
					}
				}
			}
			if(count($poItemId) > 0) InvoicePurchaseOrderItem ::where('invoice_id', '=', $invoice_id)->whereNotIn('id', $poItemId)->delete();//delete
            return redirect('admin/invoice/purchase-order')->with('success', trans('purchaseOrder/message.success.update'));
        } else {
            return Redirect::route('admin/invoice/purchase-order')->withInput()->with('error', trans('purchaseOrder/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = PurchaseOrder::select('purchase_order.id as ID', 
									'rab.name as RAB_Name',
									'supplier.supplier_name as Supplier_Name',
									DB::raw("(CASE WHEN purchase_order.vat = '0' THEN 'false' ELSE 'true' END) as PPn"),
									DB::raw("DATE_FORMAT(purchase_order.delivery_date, '%d %b, %Y') as Delivery_Date"),
									DB::raw("DATE_FORMAT(purchase_order.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(purchase_order.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("ROUND(REPLACE(purchase_order.shipping_fee, ',', ''),3) as Shipping_Fee"),
									'purchase_order.notes as Notes',
									'purchase_order.term_of_payment as Term_of_Payment',
									'purchase_order.requested_by as Requested_By',//acan
									DB::raw("DATE_FORMAT(purchase_order.requested_at, '%d %b, %Y %H:%i') as Requested_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By"),
									'purchase_order.approved_by as Approved_By',//acan
									'purchase_order.approved_at as Approved_At'//acan
							)
							->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
							->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
							->join('users', 'users.id', '=', 'purchase_order.created_by')
							->where(array('purchase_order.deleted' => 0))
							->get();
										
		Excel::create('delivery-receipt-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
	
	public function getModalPrintInvoicePo(InvoicePurchaseOrder $invoicePurchaseOrder)
	{
		$model = 'invoicePurchaseOrder';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('print/invoicePurchaseOrder', ['id' => $invoicePurchaseOrder->id]);
            return view('admin.layouts.modal_printInvoicePO', compact('error', 'model', 'confirm_route', 'invoicePurchaseOrder'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoicePurchaseOrder/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
	}
	
	public function printInvoicePo(AllRabRequest $request, InvoicePurchaseOrder $invoicePurchaseOrder)
	{
		if ($request->isMethod('post')) {
			$jmlTagihan = InvoicePurchaseOrder::select(DB::raw('SUM(invoice_purchase_order_item.total_price) as jml'))
									->join('invoice_purchase_order_item', 'invoice_purchase_order_item.invoice_id', '=', 'invoice_purchase_order.invoice_number')
									->where('invoice_purchase_order_item.invoice_id', $invoicePurchaseOrder->invoice_number)
									->groupBy('invoice_purchase_order_item.invoice_id')
									->first();
			$data = PurchaseOrder::select('project.project_name', 'project.address', 'supplier.supplier_name')
									->join('invoice_purchase_order', 'invoice_purchase_order.purchase_order_id', '=', 'purchase_order.id')
									->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
									->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
									->join('project', 'project.id', '=', 'rab.project_id')
									->where('purchase_order.id', $invoicePurchaseOrder->purchase_order_id)
									->first();
			$no = $request->no;
			$jnsTagihan = $request->jnsTagihan;
			//dd($data->toArray());
			PDF::AddPage();
			PDF::SetFont('helvetica', '', 8);
			
			$html2 = '
				<table width="100%" cellpadding="3">
					<tr>
						<td width="60%">
							<table width="100%">
								<tr>
									<td colspan="2">
										<img src="assets/images/company-logo.png" style="width: 280px; height:30px;"/>
									</td>
								</tr>
								<tr>
									<td width="25%" style="font-weight: bold; font-size:7px;"></td>
									<td width="75%" style="font-weight: bold; font-size:7px;">
										Ruko Taman Bougenville Estate Blok A No. 8
									</td>
								</tr>
								<tr>
									<td width="25%" style="font-weight: bold; font-size:7px;"></td>
									<td width="75%" style="font-weight: bold; font-size:7px;">
										Jl. Raya Kalimalang - Bekasi
									</td>
								</tr>
								<tr>
									<td width="25%" style="font-weight: bold; font-size:7px;"></td>
									<td width="75%" style="font-weight: bold; font-size:7px;">
										Telp. 86901475 - 86901476 - Fax. 8645879
									</td>
								</tr>
							</table>
							<br/>
							<br/>
							<table width="100%" cellspacing="3">
								<tr>
									<td width="40%">NAMA SUPPLIER</td>
									<td width="2%">:</td>
									<td>'.$data->supplier_name.'</td>
								</tr>
								<tr>
									<td width="40%">No. Faktur/Kwitansi/Nota/Order</td>
									<td width="2%">:</td>
									<td>'.$no.'</td>
								</tr>
								<tr>
									<td width="40%">Jumlah Tagihan</td>
									<td width="2%">:</td>
									<td>Rp. '.number_format($jmlTagihan->jml,'0','.',',').'</td>
								</tr>
							</table>
						</td>
						<td width="40%">
							<table width="100%" cellspacing="5">
								<tr>
									<td style="font-weight: bold; font-size: 20px;">No. :</td>
									<td></td>
									<td align="right"></td>
								</tr>
								<tr>
									<td width="35%"></td>
									<td width="2%"></td>
									<td></td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Tanggal terima</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td>'.date('d M, Y').'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Proyek</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td>'.$data->project_name.'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Lokasi</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td>'.$data->address.'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Jenis Tagihan</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td>'.$jnsTagihan.'</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br/>';
				
			$html2 .= '
				<table width="100%" style="border: 0.5px solid #000;" cellpadding="3">
					<tr>
						<td align="center" width="29%" style="font-weight: bold; border-bottom: 0.5px solid #000;" colspan="2">KELENGKAPAN</td>
						<td align="center" width="18%" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; font-weight: bold;"></td>
						<td align="center" width="18%" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; font-weight: bold;">PENERIMA</td>
						<td align="center" width="35%" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; font-weight: bold;">KETERANGAN</td>
					</tr>
					<tr>
						<td width="26%" style="border-bottom: 0.5px solid #000;">* KWITANSI/METERAI</td>
						<td width="3%" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td width="18%" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td width="18%" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;" rowspan="2"></td>
						<td rowspan="2" style="border-left: 0.5px solid #000; font-size:7.5px;">
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Pembayaran dilakukan kalau pengirim barang telah komplit sesuai dengan PO. yang diterbitkan serta sesuai dengan Speksifikasi.</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td colspan="3" rowspan="5" style="border-bottom: 0.5px solid #000; border-right: 0.5px solid #000;">* FAKTUR PAJAK NO. SERI<br></td>
					</tr>
					<tr>
						<td rowspan="2" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; border-right: 0.5px solid #000;">PURCHASING</td>
						<td style="font-size:7.5px;padding:10px 10px;">
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Sebelum ambil Giro/Cek di harapkan konfirmasi dulu melalui telephone hari Rabu siang.</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="font-size:7.5px;">
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Pengambilan Giro hari Senin s/d Jumat 10.00-16.00</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td rowspan="2" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; border-right: 0.5px solid #000;">DIREKTUR</td>
						<td style="font-size:7.5px;">
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Tukar Faktur hari Senin, Selasa, Rabu.</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="font-size:7.5px;">
						<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">E. Faktur harus dilampirkan dua Lembar dan tidak boleh di Email.</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>* SURAT JALAN</td>
						<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td rowspan="2" style="border-left: 0.5px solid #000; border-right: 0.5px solid #000;">DIREKTUR</td>
						<td style="border-top: 0.5px solid #000;">
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Giro/Cek atas nama</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td style="border-bottom: 0.5px solid #000;">* PURCHASE ORDER<br></td>
						<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td>
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Dibayar</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>* BUKTI TANDA TERIMA</td>
						<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000; border-top: 0.5px solid #000; border-right: 0.5px solid #000;" rowspan="2" valign="center">KEUANGAN</td>
						<td rowspan="2" style="border-top: 0.5px solid #000;">
							<table width="100%">
								<tr>
									<td width="4%" style="font-weight: bold;">*</td>
									<td width="96%">Penerima</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>* BERITA ACARA ST. PEKERJAAN<br></td>
						<td style="border-left: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000;"></td>
					</tr>
				</table>
			';
			
			$html = '<table><tr><td style="text-align: right;"><i style="color: red;">Office</i></td></tr>';
			$html .= $html2;
			
			$html .= '
				<br>
				<br>
				<br>
				<br>
				<br>
				<div style="border-top: 0.5px dashed black;"></div>
			';
			
			$html .= '<table><tr><td style="text-align: right;"><i style="color: red;">Client</i></td></tr>';
			$html .= $html2;
			
			// output the HTML content
			PDF::writeHTML($html, true, 0, true, 0);
			PDF::Output('printInvoicePO.pdf');
		}else{
			return redirect('admin/invoice/purchase-order');
		}
	}
}