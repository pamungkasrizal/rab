<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItem;
use App\ServiceOrder;
use App\CashExpense;
use App\Subcontractor;
use App\Rab;
use App\RabItem;
use App\DeliveryReceipt;
use App\DeliveryReceiptItem;
use App\MasterData;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;

class AjaxController extends Controller
{
    public function ajaxRabItemMaster(AllRabRequest $request)
	{
		$term = $request->term;
		$rabId = $request->rabId;
		$masterId = $request->masterId;
		$whereNotIn = (!empty($request->whereNotIn)) ? json_decode($request->whereNotIn) : [0];

		$results = RabItem::select(
								'rab_item.master_data_id as id', 
								DB::raw('CONCAT(rab_item.id, " / ", master_data.name) AS text'),
								DB::raw('IFNULL(rab_item.quantity,0) as qty_rab'),
								DB::raw('IFNULL(SUM(purchase_order_item.quantity),0) as qty_po')
							)
							->join('master_data', 'master_data.id', '=', 'rab_item.master_data_id')
							->leftJoin('purchase_order_item', 'purchase_order_item.rab_item_id', '=', 'rab_item.id')
							->where('master_data.name', 'LIKE', '%'.$term.'%')
							->whereNotIn('rab_item.id', $whereNotIn)
							->where('rab_item.deleted', '=', 0)
							->where('rab_item.rab_id', '=', $rabId)
							->where('master_data.master_no', '=', $masterId)
							->groupBy('rab_item.id')
							->orderBy('master_data.name', 'asc')
							->havingRaw('(qty_rab - qty_po) > 0')
							->get();
		return json_encode($results);
		/* ->where('rab_item.id', 'LIKE', '%'.$term.'%')
		->orWhere('master_data.name', 'LIKE', '%'.$term.'%') */
	}
	
	public function masterDataAjaxGetUnit(AllRabRequest $request)
	{
		$masterId = $request->term;
		$results = MasterData::select('inventory_unit')
									->where('id', $masterId)
									->first();
		return json_encode($results);
	}
	
	public function masterDataSearchAjaxSelectOne(AllRabRequest $request)
	{
		$term = $request->term;
		$whereNotIn = (!empty($request->whereNotIn)) ? json_decode($request->whereNotIn) : [0];

		$results = MasterData::select('id', DB::raw("CONCAT(name,' - ',CASE WHEN (master_no = 1) THEN 'Item Pekerjaan Subkon' ELSE (CASE WHEN (master_no = 2) THEN 'Cash Expense' ELSE 'Inventory' END) END) as text"))
										->where('name', 'LIKE', '%'.$term.'%')
										->whereNotIn('id', $whereNotIn)
										->where('deleted', '=', 0)
										->orderBy('name', 'asc')
										->get();
		return json_encode($results);
	}
	
	public function ajaxPurchaseOrder(AllRabRequest $request)
	{
		$term = $request->term;		
		$results = PurchaseOrder::select('id', 'id as text')
								->where('deleted', '=', 0)
								//->where('delivery_status', '!=', 'completed')
								//->where('status', '=', 1) //SERVER_MATIIN_DAHULU
								->where('id', 'LIKE', '%'.$term.'%')
								->orderBy('id', 'asc')
								->get();
								
		return json_encode($results);
	}
	
	public function ajaxServiceOrder(AllRabRequest $request)
	{
		$term = $request->term;		
		$results = ServiceOrder::select('id', 'id as text')
								->where('deleted', '=', 0)
								//->where('status', '=', 1)
								->where('id', 'LIKE', '%'.$term.'%')
								->orderBy('id', 'asc')
								->get();
								
		return json_encode($results);
	}
	
	public function ajaxPurchaseOrderItem(AllRabRequest $request)
	{
		$term = $request->term;
		$poId = $request->poId;
		$whereNotIn = (!empty($request->whereNotIn)) ? json_decode($request->whereNotIn) : [0];
		
		$results = PurchaseOrderItem::select('purchase_order_item.id as id', 
										DB::raw('CONCAT(purchase_order_item.id, " / ", master_data.name) AS text'))
										->where('purchase_order_item.purchase_order_id', '=', $poId)
										->whereNotIn('purchase_order_item.id', $whereNotIn)
										->where('purchase_order_item.id', 'LIKE', '%'.$term.'%')
										//->orWhere('master_data.name', 'LIKE', '%'.$term.'%')
										->where('purchase_order_item.status', '=', 1)
										->where('master_data.master_no', '=', 1)
										->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')									
										->orderBy('master_data.name', 'asc')
										->get();
		return json_encode($results);
	}
	
	public function ajaxPurchaseOrderOnDeliveryReceipt(AllRabRequest $request)
	{
		$term = $request->term;		
		$results = DeliveryReceiptItem::select('purchase_order_id as id', 'purchase_order_id as text')
								->where('purchase_order_id', 'LIKE', '%'.$term.'%')
								->groupBy('purchase_order_id')
								->orderBy('purchase_order_id', 'asc')
								->get();
								
		return json_encode($results);
	}
	
	public function ajaxDeliveryItemChange(AllRabRequest $request)
	{
		$term = $request->term;		
		$results = PurchaseOrder::select('purchase_order.*', 'supplier.supplier_name', 'rab.name as rab_name')
								->where('purchase_order.id', '=', $term)
								->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
								->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
								->get();
								
		return json_encode($results);
	}
	
	public function ajaxServiceOrderItemChange(AllRabRequest $request)
	{
		$term = $request->term;		
		$results = ServiceOrder::select('service_order.*', 'subcontractor.subcontractor_name', 'rab.name as rab_name', 
								DB::raw('(SELECT sum(invoice_service_order_item.total_price) FROM invoice_service_order_item WHERE invoice_service_order_item.service_order_id = '.$term.') AS jumlah'),
								DB::raw('(SELECT sum(purchase_order_item.unit_total_price) FROM purchase_order_item WHERE purchase_order_item.purchase_order_id = '.$term.') AS jml_total_onitem'))
								->where('service_order.id', '=', $term)
								->join('subcontractor', 'subcontractor.id', '=', 'service_order.subcontractor_id')
								->join('rab', 'rab.id', '=', 'service_order.rab_id')
								->get();
								
		return json_encode($results);
	}
	
	public function ajaxPODRItem(AllRabRequest $request)
	{
		$term = $request->term;
		$poId = $request->poId;
		$whereNotIn = (!empty($request->whereNotIn)) ? json_decode($request->whereNotIn) : [0];

		$results = DeliveryReceiptItem::select('delivery_receipt_item.id', 
										DB::raw('CONCAT(delivery_receipt_item.delivery_receipt_id, " / ", master_data.name) AS text'))
										->where('delivery_receipt_item.purchase_order_id', '=', $poId)
										->where('delivery_receipt_item.id', 'LIKE', '%'.$term.'%')
										->orWhere('master_data.name', 'LIKE', '%'.$term.'%')
										->whereNotIn('delivery_receipt_item.id', $whereNotIn)
										->join('purchase_order_item', 'purchase_order_item.id', '=', 'delivery_receipt_item.purchase_order_item_id')
										->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
										->orderBy('delivery_receipt_item.id', 'asc')
										->get();
		return json_encode($results);
	}
	
	public function ajaxPODRItemChange(AllRabRequest $request)
	{
		$term = $request->term;
		$results = DeliveryReceiptItem::select('delivery_receipt_item.quantity', 'purchase_order_item.unit_price')
										->where('delivery_receipt_item.id', '=', $term)
										->join('purchase_order_item', 'purchase_order_item.id', '=', 'delivery_receipt_item.purchase_order_item_id')
										->first();
		return json_encode($results);
	}
}
