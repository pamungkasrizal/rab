<?php namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\MessageBag;
use Securimage;
use Sentinel;
use Response;
use View;
use App\MasterData;
use App\ServiceType;
use App\Subcontractor;
use App\Supplier;
use App\Http\Requests\AllRabRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Excel;
use DB;

class JoshController extends Controller {

	protected $countries = array();
	/**
	 * Message bag.
	 *
	 * @var Illuminate\Support\MessageBag
	 */
	protected $messageBag = null;

	/**
	 * Initializer.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->messageBag = new MessageBag;

	}

	/**
	* Crop Demo
	*/
	public function crop_demo()
	{
		if ($_SERVER['REQUEST_METHOD'] == 'POST')
		{
			$targ_w = $targ_h = 150;
			$jpeg_quality = 99;

			$src = base_path().'/public/assets/img/cropping-image.jpg';
		//dd($src);
			$img_r = imagecreatefromjpeg($src);

			$dst_r = ImageCreateTrueColor( $targ_w, $targ_h );

			imagecopyresampled($dst_r,$img_r,0,0,intval($_POST['x']),intval($_POST['y']), $targ_w,$targ_h, intval($_POST['w']),intval($_POST['h']));

			header('Content-type: image/jpeg');
			imagejpeg($dst_r,null,$jpeg_quality);

			exit;
		}
	}

    public function showHome()
    {
    	if(Sentinel::check())
			return view('admin.index');
		else
			return Redirect::to('admin/signin')->with('error', 'You must be logged in!');
    }

    public function showView($name=null)
    {

    	if(View::exists('admin/'.$name))
		{
			if(Sentinel::check())
				return view('admin.'.$name);
			else
				return Redirect::to('admin/signin')->with('error', 'You must be logged in!');
		}
		else
		{
			return view('admin.404');
		}
    }

    public function showFrontEndView($name=null)
    {

        if(View::exists($name))
        {
            return view($name);
        }
        else
        {
            return view('admin.404');
        }
    }

	public function secureImage(Request $request)
	{
        session_start();
		include_once public_path()."/assets/vendors/secureimage/securimage.php";
		$securimage = new Securimage();
		if ($securimage->check($request->captcha_code) == false) {
			echo "The security code entered was incorrect.<br /><br />";
			echo "Please go <a href='javascript:history.go(-1)'>back</a> and try again.";
			exit;
		}
		else{
			echo "The security code entered was correct. <a href='javascript:history.go(-1)'>back</a><br /><br />";
			exit;
		}

	}
	
	public function getModalImport(Request $request)
	{
		$type = $request->importType;
		$model = 'masterdata/cashExpense';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/masterData', ['id' => $type]);
            return view('admin.layouts.modal_import', compact('error', 'model', 'confirm_route', 'type'));
        } catch (GroupNotFoundException $e) {

            $error = trans('masterdata/cashExpense/message.error.delete', compact('id'));
            return view('admin.layouts.modal_import', compact('error', 'model', 'confirm_route'));
        }
	}
	
	public function importData(Request $request)
	{
		if(Input::hasFile('name_file')){
			$type = $request->type_name;
			$path = Input::file('name_file')->getRealPath();
			$data = Excel::load($path, function($reader) {})->get();
			//dd($data->toArray());
			$date = date('Y-m-d H:i:s');
			$iduser = Sentinel::getUser()->id;
			$db = '';
			
			if(!empty($data) && $data->count()){
				
				if($type == 'cash-expense'){
					$db = 'master_data';
					foreach ($data as $key => $value) {
						$insert[] = [
										'id' => '2'.date('md').mt_rand(1000,9999), 
										'name' => $value->name, 
										'created_at' => $date, 
										'updated_by' => $iduser, 
										'master_no' => 2
									];
					}
				}else if($type == 'inventory'){
					$db = 'master_data';
					foreach ($data as $key => $value) {
						$insert[] = ['name' => $value->name, 
										'created_at' => $date, 
										'updated_by' => $iduser, 
										'inventory_unit' => $value->unit, 
										'master_no' => 3
									];
					}
				}else if($type == 'service-master'){
					$db = 'master_data';
					foreach ($data as $key => $value) {
						$insert[] = ['name' => $value->name, 
										'created_at' => $date, 
										'updated_by' => $iduser, 
										'service_type' => $value->jenis_pekerjaan_subkon, 
										'master_no' => 1
									];
					}
				}else if($type == 'service-type'){
					$db = 'service_type';
					foreach ($data as $key => $value) {
						$insert[] = ['service_type_name' => $value->name,
										'created_at' => $date, 
										'updated_by' => $iduser
									];
					}
				}else if($type == 'subcontractor'){
					$db = 'subcontractor';
					foreach ($data as $key => $value) {
						$insert[] = ['subcontractor_name' => $value->name, 
										'created_at' => $date, 
										'updated_by' => $iduser
									];
					}
				}else if($type == 'supplier'){
					$db = 'supplier';
					foreach ($data as $key => $value) {
						$insert[] = ['supplier_name' => $value->name, 
										'created_at' => $date, 
										'updated_by' => $iduser
									];
					}
				}
				
				
				if(!empty($insert)){
					if (DB::table($db)->insert($insert)) { //DB::table('items')->insert($insert);
						return redirect('admin/master/'.$type)->with('success', trans('auth/message.import_success'));
					} else {
						return Redirect::route('admin/master/'.$type)->withInput()->with('error', trans('auth/message.import_failed'));
					}
				}
			}
		}
	}
	
	public function downloadTemplate(Request $request)
	{
		$fileName = $request->typeFile.'.csv';
		$storagePath = storage_path('app/files/'.$fileName);
        if( ! \File::exists($storagePath)){
            return view('admin/404');
        }
        return Response::download($storagePath, $fileName, [
            'Content-Length: '. filesize($storagePath)
        ]);
	}

}