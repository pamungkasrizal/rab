<?php

namespace App\Http\Controllers;

use App\ServiceOrder;
use App\PurchaseOrderItem;
use App\Subcontractor;
use App\Rab;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use PDF;
use URL;
use View;
use Datatables;
use Redirect;

class ServiceOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
		
	}
	 
    public function index()
    {
		return view('admin.serviceOrder.index');
    }
	
	public function data()
    {
        $data = ServiceOrder::select('service_order.id', 'rab.name as rab_name', 'subcontractor.subcontractor_name', 'service_order.notes', 'service_order.requested_by','service_order.requested_at','service_order.start_date','service_order.status', 'users.first_name')
										->join('rab', 'rab.id', '=', 'service_order.rab_id')
										->join('subcontractor', 'subcontractor.id', '=', 'service_order.subcontractor_id')
										->join('users', 'users.id', '=', 'service_order.requested_by')
										->where('service_order.deleted', '=', '0')
										->orderBy('service_order.created_at', 'desc')
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('start_date',function(ServiceOrder $data) {
				return date('j M, Y', strtotime($data->start_date));
            })
			->edit_column('requested_at',function(ServiceOrder $data) {
				return date('j M, Y', strtotime($data->requested_at));
            })
            ->add_column('actions',function($data) {
				
				if(Sentinel::inRole('admin')){
				$actions = '<a href="'.route('confirm-delete/serviceOrder', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
				}else{
				$actions = '<a href="'.URL::to('admin/service-order/' . $data->id . '/view' ).'" title="update data">
								<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
							</a>';
				}
				
				if($data->status == 1)
				{
					$actions .= '<a href="'.URL::to('admin/service-order/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>';
				}
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$subkon = DB::table('count_subkon')->select('number','year')->orderBy('number', 'desc')->first();
		$id = sprintf('%03d', (isset($subkon->number) ? $subkon->number : 0) + 1);
		
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$subcontractor = Subcontractor::where('deleted', 0)->orderBy('subcontractor_name', 'asc')->lists('subcontractor_name', 'id');
		$master_data = array();
        return view('admin.serviceOrder.create', compact('id','rab','subcontractor','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $serviceOrder = new ServiceOrder($request->all());
		$serviceOrder->status_created = (Sentinel::inRole('kasir') || Sentinel::inRole('kasir') || Sentinel::inRole('pm')) ? 'created' : 'request';
		
		if(Sentinel::inRole('kasir') || Sentinel::inRole('kasir') || Sentinel::inRole('pm')){// if  input logistik
			$serviceOrder->log_app_by = Sentinel::getUser()->id;
			$serviceOrder->log_app_at = date('Y-m-d H:i:s');
		}
		
		$serviceOrder->start_date = date('Y-m-d', strtotime($serviceOrder->start_date));
		$serviceOrder->created_by = Sentinel::getUser()->id;
		$serviceOrder->requested_at = date('Y-m-d H:i:s'); // lap or log
		$serviceOrder->requested_by = Sentinel::getUser()->id; // lap or log
		
		$purchase_order_id = $serviceOrder->id;
		
        if ($serviceOrder->save()) {
			if(isset($request->purchase_order_item)){
				foreach($request->purchase_order_item as $data)
				{ //save purchaseOrderItem
					$purchaseOrderItem = new purchaseOrderItem($data);
					$purchaseOrderItem->id = '9'.date('md').mt_rand(1000,9999);
					$purchaseOrderItem->purchase_order_id = $purchase_order_id;
					$purchaseOrderItem->created_by = Sentinel::getUser()->id;
					$purchaseOrderItem->status = 1;
					$purchaseOrderItem->save();
				}
			}
			DB::table('count_subkon')->insert(['year' => date('Y')]);
            return redirect('admin/service-order')->with('success', trans('serviceOrder/message.success.create'));
        } else {
            return Redirect::route('admin/service-order')->withInput()->with('error', trans('serviceOrder/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(ServiceOrder $serviceOrder)
    {
        $model = 'serviceOrder';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/serviceOrder', ['id' => $serviceOrder->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('serviceOrder/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function getModalApprove(ServiceOrder $serviceOrder)
    {
        $model = 'serviceOrder';
		$tittle = 'SPK Subkon';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('approve/serviceOrder', ['id' => $serviceOrder->id]);
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        } catch (GroupNotFoundException $e) {

            $error = trans('serviceOrder/message.error.approve', compact('id'));
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        }
    }
	
	public function getModalReject(ServiceOrder $serviceOrder)
    {
        $model = 'serviceOrder';
        $confirm_route = $error = null;
        try {
            return view('admin.layouts.modal_reject_spk', compact('error', 'model', 'confirm_route', 'serviceOrder'));
        } catch (GroupNotFoundException $e) {

            $error = trans('serviceOrder/message.error.reject', compact('id'));
            return view('admin.layouts.modal_reject_spk', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(ServiceOrder $serviceOrder)
    {
		$serviceOrder->deleted = 1;
        if ($serviceOrder->update($serviceOrder->toArray())) {
            return redirect('admin/service-order')->with('success', trans('serviceOrder/message.success.delete'));
        } else {
            return Redirect::route('admin/service-order')->withInput()->with('error', trans('serviceOrder/message.error.delete'));
        }
    }
	
	public function approve(ServiceOrder $serviceOrder)
    {
		$serviceOrder->status = 1;
		$serviceOrder->status_msg = '';
		$serviceOrder->approved_by = Sentinel::getUser()->id;
		$serviceOrder->approved_at = date('Y-m-d H:i:s');
        if ($serviceOrder->update($serviceOrder->toArray())) {
            return redirect('admin/service-order')->with('success', trans('serviceOrder/message.success.approve'));
        } else {
            return Redirect::route('admin/service-order')->withInput()->with('error', trans('serviceOrder/message.error.approve'));
        }
    }
	
	public function reject(AllRabRequest $request, ServiceOrder $serviceOrder)
    {
		$serviceOrder->status = 2;
		$serviceOrder->status_msg = $request->status_msg;
        if ($serviceOrder->update($serviceOrder->toArray())) {
            return redirect('admin/service-order')->with('success', trans('serviceOrder/message.success.reject'));
        } else {
            return Redirect::route('admin/service-order')->withInput()->with('error', trans('serviceOrder/message.error.reject'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(ServiceOrder $serviceOrder)
    {
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$subcontractor = Subcontractor::where('deleted', 0)->orderBy('subcontractor_name', 'asc')->lists('subcontractor_name', 'id');
		$master_data = array();
		$purchaseOrderItem = purchaseOrderItem::select('purchase_order_item.*', 'master_data.name as master_data_name')
								->where('purchase_order_id', $serviceOrder->id)
								->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
								->get();
        return view('admin.serviceOrder.edit', compact('subcontractor', 'rab', 'serviceOrder', 'master_data', 'purchaseOrderItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, serviceOrder $serviceOrder)
    {
		$request->merge(array('start_date' => date('Y-m-d', strtotime($request->start_date))));
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		$request->merge(array('requested_at' => date('Y-m-d H:i:s')));
		$request->merge(array('log_app_by' => Sentinel::getUser()->id));
		$request->merge(array('log_app_at' => date('Y-m-d H:i:s')));
		
		$purchase_order_id = $request->id;
		
        if ($serviceOrder->update($request->all())) {
			
			$poItemId = array();
			if(isset($request->purchase_order_item)){
				foreach($request->purchase_order_item as $data)
				{ //save purchaseOrderItem
					$purchaseOrderItem = new purchaseOrderItem($data);
					
					if(isset($purchaseOrderItem->id)){
						array_push($poItemId, $purchaseOrderItem->id);
					}else{
						$purchaseOrderItem->id = '9'.date('md').mt_rand(1000,9999);
						array_push($poItemId, $purchaseOrderItem->id);
						$purchaseOrderItem->purchase_order_id = $purchase_order_id;
						$purchaseOrderItem->created_by = Sentinel::getUser()->id;
						$purchaseOrderItem->save();
					}
				}
			}
			if(count($poItemId) > 0) purchaseOrderItem ::where('purchase_order_id', '=', $purchase_order_id)->whereNotIn('id', $poItemId)->delete();//delete
            return redirect('admin/service-order')->with('success', trans('serviceOrder/message.success.update'));
        } else {
            return Redirect::route('admin/service-order')->withInput()->with('error', trans('serviceOrder/message.error.update'));
        }
    }
	
	public function ajaxServiceOrder(AllRabRequest $request)
	{
		$term = $request->term;
		$results = ServiceOrder::select('id', 'id as text')
										->where('deleted', '=', 0)
										->where('delivery_status', '!=', 'completed')
										->where('id', 'LIKE', '%'.$term.'%')
										->orderBy('id', 'asc')
										->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = ServiceOrder::select('service_order.id as ID', 
									'rab.name as RAB_Name',
									'subcontractor.subcontractor_name as Supplier_Name',
									DB::raw("(CASE WHEN service_order.vat = '0' THEN 'false' ELSE 'true' END) as PPn"),
									DB::raw("DATE_FORMAT(service_order.delivery_date, '%d %b, %Y') as Delivery_Date"),
									DB::raw("DATE_FORMAT(service_order.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(service_order.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("ROUND(REPLACE(service_order.shipping_fee, ',', ''),3) as Shipping_Fee"),
									'service_order.notes as Notes',
									'service_order.term_of_payment as Term_of_Payment',
									'service_order.requested_by as Requested_By',//acan
									DB::raw("DATE_FORMAT(service_order.requested_at, '%d %b, %Y %H:%i') as Requested_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By"),
									'service_order.approved_by as Approved_By',//acan
									'service_order.approved_at as Approved_At'//acan
							)
							->join('subcontractor', 'subcontractor.id', '=', 'service_order.subcontractor_id')
							->join('rab', 'rab.id', '=', 'service_order.rab_id')
							->join('users', 'users.id', '=', 'service_order.created_by')
							->where(array('service_order.deleted' => 0))
							->get();
										
		Excel::create('service-order-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
	
	public function printPo()
	{
		
	}
}