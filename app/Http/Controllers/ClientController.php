<?php

namespace App\Http\Controllers;

use App\Client;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class ClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
		
	}
	 
    public function index()
    {
		return view('admin.client.index');
    }
	
	public function data()
    {
        $data = Client::select('client.*', 'users.first_name', 'users.last_name')
										->join('users', 'users.id', '=', 'client.updated_by')
										->where('client.deleted', '=', 0)
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('created_at',function(Client $data) {
				return $data->created_at->toDayDateTimeString();
            })
			->edit_column('updated_at',function(Client $data) {
				return ($data->updated_at ? $data->updated_at->toDayDateTimeString() : '-');
            })
            ->add_column('actions',function($data) {
				if(Sentinel::inRole('admin') || Sentinel::inRole('administrasi')){
					$actions = '<a href="'.URL::to('admin/client/' . $data->id . '/edit' ).'" title="update data">
									<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
								</a>
								<a href="'.route('confirm-delete/client', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
								   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
								</a>';
				}else{
					$actions = '<a href="'.URL::to('admin/client/' . $data->id . '/view' ).'" title="update data">
									<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
								</a>';
				}
				return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {			
		$id = '4'.date('md').mt_rand(1000,9999);
        return view('admin.client.create', compact('id'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {		
        $client = new Client($request->all());
		$client->updated_by = Sentinel::getUser()->id;
		
        if ($client->save()) {
            return redirect('admin/client')->with('success', trans('client/message.success.create'));
        } else {
            return Redirect::route('admin/client')->withInput()->with('error', trans('client/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(Client $client)
    {
        $model = 'client';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/client', ['id' => $client->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('client/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Client $client)
    {
		$client->deleted = 1;
        if ($client->update($client->toArray())) {
            return redirect('admin/client')->with('success', trans('client/message.success.delete'));
        } else {
            return Redirect::route('admin/client')->withInput()->with('error', trans('client/message.error.delete'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Client $client)
    {
        return view('admin.client.edit', compact('client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, client $client)
    {		
		$client->updated_by = Sentinel::getUser()->id;
		
        if ($client->update($request->all())) {
            return redirect('admin/client')->with('success', trans('client/message.success.update'));
        } else {
            return Redirect::route('admin/client')->withInput()->with('error', trans('client/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = Client::select('client.id as ID', 
									'client.client_name as Client_Name',
									'client.client_address as Client_Address',
									DB::raw("DATE_FORMAT(client.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(client.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('users', 'users.id', '=', 'client.updated_by')
							->where(array('client.deleted' => 0))
							->get();
										
		Excel::create('client-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}
