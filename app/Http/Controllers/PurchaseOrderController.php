<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItem;
use App\Supplier;
use App\Rab;
use App\RabItem;
use App\Project;
use App\User;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use PDF;
use URL;
use View;
use Datatables;
use Redirect;

class PurchaseOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	 
	public function __construct()
	{
		
	}
	
    public function index()
    {
		return view('admin.purchaseOrder.index');
    }
	
	public function data()
    {
        $data = PurchaseOrder::select('purchase_order.id', 'rab.name as rab_name', 'supplier.supplier_name', 'purchase_order.notes', 'purchase_order.requested_by','purchase_order.requested_at','purchase_order.delivery_date','purchase_order.status', 'users.first_name')
										->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
										->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
										->join('users', 'users.id', '=', 'purchase_order.requested_by')
										->where('purchase_order.deleted', '=', '0')
										->orderBy('purchase_order.created_at', 'desc')
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('delivery_date',function(PurchaseOrder $data) {
				return date('j M, Y', strtotime($data->delivery_date));
            })
			->edit_column('requested_at',function(PurchaseOrder $data) {
				return date('j M, Y', strtotime($data->requested_at));
            })
            ->add_column('actions',function($data) {
				
				if(Sentinel::inRole('admin')){
				$actions = '<a href="'.URL::to('admin/purchase-order/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete/purchaseOrder', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
				}else{
				$actions = '<a href="'.URL::to('admin/purchase-order/' . $data->id . '/view' ).'" title="update data">
								<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
							</a>';
				}
				if($data->status == 1){
					$actions .= '<a href="'.route('print/purchaseOrder', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="print data">
								   <i class="glyphicon glyphicon-print" style="width: 18px; height: 18px;color: #337ab7;top: -2px !important;" title="print po"></i>
								</a>';
				}
				
                return $actions;
				
            })
			->order(function ($query) {
				if(request()->has('purchase_order.created_at'))
					$query->orderBy('purchase_order.created_at', 'desc');
					
			})
			->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {								
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$supplier = Supplier::where('deleted', 0)->orderBy('supplier_name', 'asc')->lists('supplier_name', 'id');
		$master_data = array();
        return view('admin.purchaseOrder.create', compact('rab','supplier','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {		
        $purchaseOrder = new PurchaseOrder($request->all());
		$purchaseOrder->status_created = (Sentinel::inRole('logistik_kantor')) ? 'created' : 'request';
		
		if(Sentinel::inRole('logistik_kantor')){
			$purchaseOrder->log_app_by = Sentinel::getUser()->id;
			$purchaseOrder->log_app_at = date('Y-m-d H:i:s');
		}
		
		$po = DB::table('count_po')->where('year', date('Y'))->first();
		$arrMonth = array(1=>"I","II","III", "IV", "V","VI","VII","VIII","IX","X", "XI","XII");
		$month = $arrMonth[date('n')];
		$number = sprintf('%03d', (isset($po->number) ? $po->number : 0) + 1);
		$purchase_order_id = $number.'/UTD/'.$month.'/'.date('y');
		
		$purchaseOrder->id = $purchase_order_id;
		$purchaseOrder->shipping_fee = preg_replace('/[\D.]/', '', str_replace('.00','',$purchaseOrder->shipping_fee));
		$purchaseOrder->delivery_date = date('Y-m-d', strtotime($purchaseOrder->delivery_date));
		$purchaseOrder->created_by = Sentinel::getUser()->id;
		$purchaseOrder->requested_at = date('Y-m-d H:i:s'); // lap or log
		$purchaseOrder->requested_by = Sentinel::getUser()->id; // lap or log
		
        if ($purchaseOrder->save()) {
			if(isset($request->purchase_order_item)){
				foreach($request->purchase_order_item as $data)
				{ //save PurchaseOrderItem
					$purchaseOrderItem = new PurchaseOrderItem($data);
					$purchaseOrderItem->id = '9'.date('md').mt_rand(1000,9999);
					$purchaseOrderItem->purchase_order_id = $purchase_order_id;
					$purchaseOrderItem->rab_id = $purchaseOrder->rab_id;
					$purchaseOrderItem->created_by = Sentinel::getUser()->id;
					$purchaseOrderItem->save();
				}
			}
			if($po){
				DB::table('count_po')->where('year', date('Y'))->update(['number' => $number]);
			}else{
				DB::table('count_po')->insert(['year' => date('Y'), 'number' => $number]);
			}
            return redirect('admin/purchase-order')->with('success', trans('purchaseOrder/message.success.create'));
        } else {
            return Redirect::route('admin/purchase-order')->withInput()->with('error', trans('purchaseOrder/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(PurchaseOrder $purchaseOrder)
    {
        $model = 'purchaseOrder';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/purchaseOrder', ['id' => $purchaseOrder->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('purchaseOrder/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function getModalPrintPo(PurchaseOrder $purchaseOrder)
    {
        $model = 'purchaseOrder';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('print/purchaseOrder', ['id' => $purchaseOrder->id]);
            return view('admin.layouts.modal_printPO', compact('error', 'model', 'confirm_route', 'purchaseOrder'));
        } catch (GroupNotFoundException $e) {

            $error = trans('purchaseOrder/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	
	public function getModalApprove(PurchaseOrder $purchaseOrder)
    {
        $model = 'purchaseOrder';
        $tittle = 'Purchase Order';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('approve/purchaseOrder', ['id' => $purchaseOrder->id]);
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        } catch (GroupNotFoundException $e) {

            $error = trans('purchaseOrder/message.error.approve', compact('id'));
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        }
    }
	
	public function getModalReject(PurchaseOrder $purchaseOrder)
    {
        $model = 'purchaseOrder';
        $confirm_route = $error = null;
        try {
            return view('admin.layouts.modal_reject', compact('error', 'model', 'confirm_route', 'purchaseOrder'));
        } catch (GroupNotFoundException $e) {

            $error = trans('purchaseOrder/message.error.reject', compact('id'));
            return view('admin.layouts.modal_reject', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $purchaseOrder)
    {
		$purchaseOrder->deleted = 1;
        if ($purchaseOrder->update($purchaseOrder->toArray())) {
            return redirect('admin/purchase-order')->with('success', trans('purchaseOrder/message.success.delete'));
        } else {
            return Redirect::route('admin/purchase-order')->withInput()->with('error', trans('purchaseOrder/message.error.delete'));
        }
    }
	
	public function approve(PurchaseOrder $purchaseOrder)
    {		
		$purchaseOrder->status = 1;
		$purchaseOrder->status_msg = '';
		$purchaseOrder->approved_by = Sentinel::getUser()->id;
		$purchaseOrder->approved_at = date('Y-m-d H:i:s');
        if ($purchaseOrder->update($purchaseOrder->toArray())) {
            return redirect('admin/purchase-order')->with('success', trans('purchaseOrder/message.success.approve'));
        } else {
            return Redirect::route('admin/purchase-order')->withInput()->with('error', trans('purchaseOrder/message.error.approve'));
        }
    }
	
	public function reject(AllRabRequest $request, PurchaseOrder $purchaseOrder)
    {		
		$purchaseOrder->status = 2;
		$purchaseOrder->status_msg = $request->status_msg;
        if ($purchaseOrder->update($purchaseOrder->toArray())) {
            return redirect('admin/purchase-order')->with('success', trans('purchaseOrder/message.success.reject'));
        } else {
            return Redirect::route('admin/purchase-order')->withInput()->with('error', trans('purchaseOrder/message.error.reject'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $purchaseOrder)
    {		
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$supplier = Supplier::where('deleted', 0)->orderBy('supplier_name', 'asc')->lists('supplier_name', 'id');
		$master_data = array();
		$purchaseOrderItem = PurchaseOrderItem::select('purchase_order_item.*', 'master_data.name as master_data_name')
								->where('purchase_order_id', $purchaseOrder->id)
								->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
								->get();
        return view('admin.purchaseOrder.edit', compact('supplier', 'rab', 'purchaseOrder', 'master_data', 'purchaseOrderItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, purchaseOrder $purchaseOrder)
    {
		$request->merge(array('shipping_fee' => preg_replace('/[\D.]/', '', str_replace('.00','',$request->shipping_fee))));
		$request->merge(array('delivery_date' => date('Y-m-d', strtotime($request->delivery_date))));
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		$request->merge(array('requested_at' => date('Y-m-d H:i:s')));
		$request->merge(array('log_app_by' => Sentinel::getUser()->id));
		$request->merge(array('log_app_at' => date('Y-m-d H:i:s')));
		
		$purchase_order_id = $request->id;
		
        if ($purchaseOrder->update($request->all())) {
			if(isset($request->purchase_order_item)){
				$poItemId = array();
				foreach($request->purchase_order_item as $data)
				{ //save PurchaseOrderItem
					$purchaseOrderItem = new PurchaseOrderItem($data);
					
					if(isset($purchaseOrderItem->id)){
						array_push($poItemId, $purchaseOrderItem->id);
					}else{
						$purchaseOrderItem->id = '9'.date('md').mt_rand(1000,9999);
						array_push($poItemId, $purchaseOrderItem->id);
						$purchaseOrderItem->purchase_order_id = $purchase_order_id;
						$purchaseOrderItem->created_by = Sentinel::getUser()->id;
						$purchaseOrderItem->save();
					}
				}
			}
			if(count($poItemId) > 0) PurchaseOrderItem ::where('purchase_order_id', '=', $purchase_order_id)->whereNotIn('id', $poItemId)->delete();//delete
            return redirect('admin/purchase-order')->with('success', trans('purchaseOrder/message.success.update'));
        } else {
            return Redirect::route('admin/purchase-order')->withInput()->with('error', trans('purchaseOrder/message.error.update'));
        }
    }
	
	public function ajaxRab(AllRabRequest $request)
	{
		$term = $request->term;
		$results = Rab::select('id', 'name as text')
							->where('deleted', '=', 0)
							->where('name', 'LIKE', '%'.$term.'%')
							->orderBy('name', 'asc')
							->get();
		return json_encode($results);
	}
	
	public function ajaxSupplier(AllRabRequest $request)
	{
		$term = $request->term;
		$results = Supplier::select('id', 'supplier_name as text')
								->where('deleted', '=', 0)
								->where('supplier_name', 'LIKE', '%'.$term.'%')
								->orderBy('supplier_name', 'asc')
								->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = PurchaseOrder::select('purchase_order.id as ID', 
									'rab.name as RAB_Name',
									'supplier.supplier_name as Supplier_Name',
									DB::raw("(CASE WHEN purchase_order.vat = '0' THEN 'false' ELSE 'true' END) as PPn"),
									DB::raw("DATE_FORMAT(purchase_order.delivery_date, '%d %b, %Y') as Delivery_Date"),
									DB::raw("DATE_FORMAT(purchase_order.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(purchase_order.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("ROUND(REPLACE(purchase_order.shipping_fee, ',', ''),3) as Shipping_Fee"),
									'purchase_order.notes as Notes',
									'purchase_order.term_of_payment as Term_of_Payment',
									'purchase_order.requested_by as Requested_By',//acan
									DB::raw("DATE_FORMAT(purchase_order.requested_at, '%d %b, %Y %H:%i') as Requested_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By"),
									'purchase_order.approved_by as Approved_By',//acan
									'purchase_order.approved_at as Approved_At'//acan
							)
							->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
							->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
							->join('users', 'users.id', '=', 'purchase_order.created_by')
							->where(array('purchase_order.deleted' => 0))
							->get();
										
		Excel::create('purchase-order-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
	
	public function printPo(AllRabRequest $request, PurchaseOrder $purchaseOrder)
	{	
		if ($request->isMethod('post')) {
			$purchaseOrderItem = PurchaseOrderItem::select('purchase_order_item.*', 'master_data.name as master_data_name', 'master_data.inventory_unit')
									->where('purchase_order_id', $purchaseOrder->id)
									->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
									->get();
			$project = PurchaseOrder::select('project.project_name', 'project.address', 'purchase_order.vat')
									->where('purchase_order.id', $purchaseOrder->id)
									->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
									->join('project', 'project.id', '=', 'rab.project_id')
									->first();
			$createdBy = User::whereId($purchaseOrder->created_by)->select('first_name','last_name')->first();
			$createdBy = $createdBy->first_name.' '.$createdBy->last_name;
			$approvedBy = User::whereId($purchaseOrder->approved_by)->select('first_name','last_name')->first();
			$approvedBy = ($approvedBy) ? $approvedBy->first_name.' '.$approvedBy->last_name : 'unapprove';
			$nameLO = $request->logistik;
			$namePM = $request->pm;
			$supplier = Supplier::whereId($purchaseOrder->supplier_id)->select('supplier_name','email','telepon','fax')->first();
			$jumlah = 0;
			$disc = 0;
			if($request->rad == 'lama'){
				$npwp = '01.321.677.5.043.000';
				#$alamat = 'Jl. Kelapa Puyuh IV Blok Kb 33-34 Kelapa Gading - Jakarta Utara';
				$alamat = 'Ruko Plaza Kelapa Gading (Ruko Inkopal) Blok A No 50 Lantai 1 Jalan Boulevard Barat Raya Kelapa Gading, Jakarta Utara';
			}else{
				$npwp = '01.321.677.5.447.001';
				$alamat = 'Ruko Taman Bougenville Estate Blok A No. 8 Jatibening - Bekasi';
			}
			//dd($createdBy->toArray());						
			
			/*========================pdf========================*/
			PDF::AddPage();
			PDF::SetFont('helvetica', '', 8);
			
			$html = '
				<table width="100%" style="border: 1px solid #000;" cellpadding="3">
					<tr>
						<td style="text-align: center;border-bottom: 1px solid #000;" colspan="7">
							<b style="font-size: 11px;">'.str_replace('-', ' ', env('COMPANY')).'</b><br/>
							Jl. Raya Kalimalang Komplek Ruko Bougenville Estate<br/>
							Blok A No. 8 Tlp. 86901475, Fax. 8645879<br/>
							Jatibening - Bekasi
						</td>
					</tr>
					<tr>
						<td style="text-align: center;" colspan="7">
							<p style="font-size:9px;font-weight:bold;"><u>PURCHASE ORDER</u></span>
						</td>
					</tr>
					<tr style="font-size:9px;">
						<td width="50%">
							KEPADA YTH.<br/>
							<br/>
							<br/>
							<b style="font-size: 11px;">'.$supplier->supplier_name.'</b><br/>
							UP : '.$request->up.'<br/>
							<br/>
							FAX : '.$supplier->fax.'<br/>
							TLP : '.$supplier->telepon.'<br/>
							Email : '.$supplier->email.'<br/>
						</td>
						<td width="50%">
							<table width="100%" cellspacing="3">
								<tr style="font-size:9px;">
									<td width="20%">No. PO</td>
									<td width="2%">:</td>
									<td width="78%">'.$purchaseOrder->id.'</td>
								</tr>
								<tr style="font-size:9px;">
									<td width="20%">Tanggal</td>
									<td width="2%">:</td>
									<td>'.date('d M, Y', strtotime($purchaseOrder->requested_at)).'</td>
								</tr>
								<tr style="font-size:9px;">
									<td colspan="3" width="100%">Harap dikirim barang-barang dibawah ini ke : </td>
								</tr>
								<tr style="font-size:9px;">
									<td width="20%">Proyek</td>
									<td width="2%">:</td>
									<td width="78%">'.$project->project_name.'</td>
								</tr>
								<tr style="font-size:9px;">
									<td width="20%">Lokasi</td>
									<td width="2%">:</td>
									<td>'.$project->address.'</td>
								</tr>
								<tr style="font-size:9px;">
									<td width="20%">Penerima</td>
									<td width="2%">:</td>
									<td>'.$request->penerima1.'</td>
								</tr>
								<tr style="font-size:9px;">
									<td width="20%"></td>
									<td width="2%"></td>
									<td>'.$request->penerima2.'</td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td align="center" width="4%" style="border-top: 1px solid #000; font-weight: bold;">No.</td>
						<td align="center" width="45%" style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;">Nama Barang / Merk</td>
						<td align="center" width="9%" style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;">VOL</td>
						<td align="center" width="9%" style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;">SAT</td>
						<td align="center" width="10%" style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;">Harga Satuan (Rp)</td>
						<td align="center" width="11%" style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;">Total Harga (Rp)</td>
						<td align="center" width="12%" style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;">Tanggal Kirim</td>
					</tr>';
			if($purchaseOrderItem){		
				foreach ($purchaseOrderItem as $key => $val){
				   $jumlah = $jumlah + $val->unit_total_price;
				   $html .= '<tr style="font-size:9px;">
								<td align="center" style="border-top: 1px solid #000;">'.($key+1).'</td>
								<td style="border-top: 1px solid #000; border-left: 1px solid #000;">'.$val->master_data_name.'</td>
								<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center">'.$val->quantity.'</td>
								<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center">'.$val->inventory_unit.'</td>
								<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right">'.number_format($val->unit_price,'0','.',',').'</td>
								<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right">'.number_format($val->unit_total_price,'0','.',',').'</td>
								<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center">'.date('d M, Y', strtotime($val->created_at)).'</td>
							</tr>';
				}
			}
			
			$jmlSetDisc = ($disc/100) == 0 ? $jumlah : $jumlah * ($disc/100);
			$ppn = ($project->vat == 0) ? 0 : 10;
			$jmlSetPpn = $jmlSetDisc * ($ppn/100);
			$total = $jmlSetDisc + $jmlSetPpn;
					
			$html .= '<tr>
						<td align="center" style="border-top: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
					</tr>
					<tr>
						<td align="center" style="border-top: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
					</tr>
					<tr>
						<td align="center" style="border-top: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
					</tr>
					<tr>
						<td align="center" style="border-top: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
					</tr>
					<tr>
						<td align="center" style="border-top: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-top: 1px solid #000; font-weight: bold;" align="center" width="20%">KETERANGAN</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;" align="center" width="20%">DIKETAHUI</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000; font-weight: bold;" align="center" colspan="2" width="27%">KONTRAKTOR</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%">Jumlah - 1</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%">'.number_format($jumlah,'0','.',',').'</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					<tr style="font-size:9px;">
						<td align="center" width="20%" rowspan="6">
							
						</td>
						<td align="center" width="20%" rowspan="6" style="border-left: 1px solid #000;">
							
						</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" colspan="2" width="27%" rowspan="6">
							<table width="100%" cellpadding="3">
								<tr style="font-size:9px;">
									<td width="50%" align="left">DIBUAT</td>
									<td width="50%" align="right">DISETUJUI</td>
								</tr>
								<tr>
									<td colspan="2"></td>
								</tr>
								<tr>
									<td colspan="2"></td>
								</tr>
								<tr>
									<td colspan="2"></td>
								</tr>
								<tr style="font-size:9px;">
									<td align="left"><u>'.$nameLO/*$createdBy*/.'</u></td>
									<td align="right"><u>'.$namePM/*$approvedBy*/.'</u></td>
								</tr>
								<tr style="font-size:9px;">
									<td align="left">Logistik</td>
									<td align="right">PM</td>
								</tr>
							</table>
						</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%">Disc.</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%">'.$disc.'%</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%">Jumlah - 2</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%">'.number_format($jmlSetDisc,'0','.',',').'</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%">-</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%">PPn 10%</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%">'.number_format($jmlSetPpn,'0','.',',').'</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%">Total</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%">'.number_format($total,'0','.',',').'</td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					<tr>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" width="10%"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="right" width="11%"></td>
						<td style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center" width="12%"></td>
					</tr>
					
					<tr style="font-size:9px;">
						<td width="49%" style="border-top: 1px solid #000;">Catatan : </td>
						<td width="18%" style="border-top: 1px solid #000; border-left: 1px solid #000;  font-weight: bold;" align="center">MENGETAHUI</td>
						<td width="33%" style="border-top: 1px solid #000; border-left: 1px solid #000;" align="center">Mengetahui & menyetujui</td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-left: 1px solid #000;">1. PO ini harus dilampirkan pada tagihan</td>
						<td style="border-left: 1px solid #000;"></td>
						<td style="border-left: 1px solid #000;" align="center">Supplier</td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-left: 1px solid #000;">2. Kwitansi bermaterai</td>
						<td style="border-left: 1px solid #000;"></td>
						<td style="border-left: 1px solid #000;"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-left: 1px solid #000;">3. Surat jalan Asli</td>
						<td style="border-left: 1px solid #000;"></td>
						<td style="border-left: 1px solid #000;"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-left: 1px solid #000;">4. Faktur Pajak</td>
						<td style="border-left: 1px solid #000;"></td>
						<td style="border-left: 1px solid #000;"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-left: 1px solid #000;">5. Bila PO ini sudah diterima harap ditanda tangan & di fax kembali</td>
						<td style="border-left: 1px solid #000;" align="center"><u>Rima Kiswanto</u></td>
						<td style="border-left: 1px solid #000;"></td>
					</tr>
					<tr style="font-size:9px;">
						<td style="border-left: 1px solid #000;">6. Penukaran invoice setiap hari senin s/d rabu</td>
						<td style="border-left: 1px solid #000;" align="center">Direktur</td>
						<td style="border-left: 1px solid #000;"></td>
					</tr>
				</table>
				
				<br/>
				<br/>
				
				<table width="100%" cellpadding="3" style="font-weight:bold">
					<tr style="font-size:9px;">
						<td width="50%">'.str_replace('-', ' ', env('COMPANY')).'</td>
						<td width="50%">: NPWP '.$npwp.'</td>
					</tr>
					<tr style="font-size:9px;">
						<td width="50%"></td>
						<td width="50%">: '.$alamat.'</td>
					</tr>
					<tr style="font-size:9px;">
						<td width="50%"></td>
						<td width="50%">: email1.pt_utama3diharja@yahoo.co.id</td>
					</tr>
				</table>
			';

			// output the HTML content
			PDF::writeHTML($html, true, 0, true, 0);
			PDF::Output('printPO.pdf');
		}else{
			return redirect('admin/purchase-order');
		}
	}
}
