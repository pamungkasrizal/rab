<?php

namespace App\Http\Controllers;

use App\Rab;
use App\RabItem;
use App\Project;
use App\MasterData;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class RabController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
		
	}  
	 
    public function index()
    {
		return view('admin.rab.index');
    }
	
	public function data()
    {
        $data = Rab::select('rab.*', 'users.first_name', 'users.last_name', 'project.project_name')
										->join('users', 'users.id', '=', 'rab.updated_by')
										->join('project', 'project.id', '=', 'rab.project_id')
										->where('rab.deleted', '=', 0)
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('created_at',function(Rab $data) {
				return $data->created_at->toDayDateTimeString();
            })
			->edit_column('updated_at',function(Rab $data) {
				return ($data->updated_at ? $data->updated_at->toDayDateTimeString() : '-');
            })
            ->add_column('actions',function($data) {
				if(Sentinel::inRole('admin')){
					$actions = '<a href="'.URL::to('admin/rab/' . $data->id . '/edit' ).'" title="update data">
									<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
								</a>
								<a href="'.route('confirm-delete/rab', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
								   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
								</a>';
				}else{
					$actions = '<a href="'.URL::to('admin/rab/' . $data->id . '/view' ).'" title="update data">
									<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
								</a>';
				}
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '6'.date('md').mt_rand(1000,9999);
		$project = $master_data = array();//Project::where('deleted', 0)->lists('project_name', 'id');
        return view('admin.rab.create', compact('id','project','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $rab = new Rab($request->all());
		$rab->updated_by = Sentinel::getUser()->id;
		
		$rab_id = $rab->id;
		
        if ($rab->save()) {
			if(isset($request->rab_item)){
				foreach($request->rab_item as $data)
				{ //save rabItem
					$rabItem = new RabItem($data);
					$rabItem->id = '7'.date('md').mt_rand(1000,9999);
					$rabItem->rab_id = $rab_id;
					$rabItem->created_by = Sentinel::getUser()->id;
					$rabItem->save();
				}
			}
            return redirect('admin/rab')->with('success', trans('rab/message.success.create'));
        } else {
            return Redirect::route('admin/rab')->withInput()->with('error', trans('rab/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(Rab $rab)
    {
        $model = 'rab';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/rab', ['id' => $rab->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('rab/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rab $rab)
    {
		$rab->deleted = 1;
        if ($rab->update($rab->toArray())) {
			DB::table('rab_item')->where('rab_id', $rab->id)->update(array('deleted' => 1));
            return redirect('admin/rab')->with('success', trans('rab/message.success.delete'));
        } else {
            return Redirect::route('admin/rab')->withInput()->with('error', trans('rab/message.error.delete'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Rab $rab)
    {
		$project = Project::where('deleted', 0)->lists('project_name', 'id');
		$master_data = MasterData::where('deleted', 0)->lists('name', 'id');
		$rabItem = RabItem::select('rab_item.*', 'master_data.name as master_data_name', 'master_data.inventory_unit')
								->where('rab_id', $rab->id)
								->join('master_data', 'master_data.id', '=', 'rab_item.master_data_id')
								->get();
        return view('admin.rab.edit', compact('project', 'rab', 'rabItem', 'master_data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, rab $rab)
    {
		$request->merge(array('updated_by' => Sentinel::getUser()->id));
		$rab_id = $rab->id;
		
        if ($rab->update($request->all())) {
			$rabItemId = array();
			if(isset($request->rab_item)){
				foreach($request->rab_item as $data)
				{ //save rab_item
					$rabItem = new RabItem($data);
					if(isset($rabItem->id)){
						array_push($rabItemId, $rabItem->id);
					}else{
						$rabItem->id = '7'.date('md').mt_rand(1000,9999);
						array_push($rabItemId, $rabItem->id);
						$rabItem->rab_id = $rab_id;
						$rabItem->updated_by = Sentinel::getUser()->id;
						$rabItem->save();
					}
				}
			}
			if(count($rabItemId) > 0) RabItem ::where('rab_id', '=', $rab_id)->whereNotIn('id', $rabItemId)->delete();//delete
            return redirect('admin/rab')->with('success', trans('rab/message.success.update'));
        } else {
            return Redirect::route('admin/rab')->withInput()->with('error', trans('rab/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = Rab::select('rab.id as ID', 
									'rab.name as RAB_Name',
									'project.project_name as Project_Name',
									DB::raw("DATE_FORMAT(rab.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(rab.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('project', 'project.id', '=', 'rab.project_id')
							->join('users', 'users.id', '=', 'rab.updated_by')
							->where(array('rab.deleted' => 0))
							->get();
										
		Excel::create('rab-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}
