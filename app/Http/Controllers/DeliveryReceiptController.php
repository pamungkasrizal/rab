<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItem;
use App\DeliveryReceipt;
use App\DeliveryReceiptItem;
use App\CashExpense;
use App\CashExpenseItem;
use App\Inventory;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use PDF;
use URL;
use View;
use Datatables;
use Redirect;

class DeliveryReceiptController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
	{		
	
	}
	 
    public function index()
    {
		return view('admin.deliveryReceipt.index');
    }
	
	public function data()
    {
        $data = DeliveryReceipt::select('delivery_receipt.*')
										->orderBy('delivery_receipt.created_at', 'desc')
										->get();
		//dd($data->toArray());
        $tables = Datatables::of($data)
            ->add_column('actions',function($data) {
				
				if(Sentinel::inRole('admin')){
				$actions = '<a href="'.route('print/deliveryReceipt', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="print data">
								   <i class="glyphicon glyphicon-print" style="width: 18px; height: 18px;color: #337ab7;top: -2px !important;" title="print delivery receipt"></i>
								</a>';
				}else{
					$actions = '';
				}
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {		
		$id = '10'.date('md').mt_rand(1000,9999);
		$po = PurchaseOrder::where('deleted', 0)->where('status', 1)->orderBy('ID', 'asc')->lists('id', 'id');
		$master_data = array();
        return view('admin.deliveryReceipt.create', compact('id','po','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $deliveryReceipt = new DeliveryReceipt($request->all());
		$deliveryReceipt->created_by = Sentinel::getUser()->id;
		
		$purchase_order_id = $deliveryReceipt->purchase_order_id;
		$delivery_receipt_id = $request->id;
		
        if ($deliveryReceipt->save()) {
			if(isset($request->delivery_receipt_item)){
				$countStatusPO = array();
				foreach($request->delivery_receipt_item as $data)
				{ 
					//save PurchaseOrderItem
					$qty = $data['qty'];
					$poItemId = '11'.date('md').mt_rand(1000,9999);
					
					$deliveryReceiptItem = new DeliveryReceiptItem($data);
					if($qty != $deliveryReceiptItem->quantity) array_push($countStatusPO, 'parsial');
					$deliveryReceiptItem->id = $poItemId;
					$deliveryReceiptItem->purchase_order_id = $purchase_order_id;
					$deliveryReceiptItem->delivery_receipt_id = $delivery_receipt_id;
					$deliveryReceiptItem->save();
					
					//save Inventory
					$inventory = new Inventory($data);
					$inventory->id = '12'.date('md').mt_rand(1000,9999);
					$inventory->delivery_receipt_item_id = $poItemId;
					$inventory->created_by = Sentinel::getUser()->id;
					$inventory->save();
				}
				
				$statusPO = (count($countStatusPO) > 0) ? 'parsial' : 'completed';
				//$table = (($request->statusDelivery == 'spk') ? 'service_order' : ($request->statusDelivery == 'ce' ? 'cash_expense' : 'purchase_order'));
				//PurchaseOrder::where('id', $purchase_order_id)->update(['delivery_status' => $statusPO]);
				//DeliveryReceipt::where('purchase_order_id', $purchase_order_id)->update(['status' => $statusPO]);
			}
            return redirect('admin/delivery-receipt')->with('success', trans('deliveryReceipt/message.success.create'));
        } else {
            return Redirect::route('admin/delivery-receipt')->withInput()->with('error', trans('deliveryReceipt/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(PurchaseOrder $deliveryReceipt)
    {
        $model = 'deliveryReceipt';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/deliveryReceipt', ['id' => $deliveryReceipt->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('deliveryReceipt/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrder $deliveryReceipt)
    {
		$deliveryReceipt->deleted = 1;
        if ($deliveryReceipt->update($deliveryReceipt->toArray())) {
            return redirect('admin/delivery-receipt')->with('success', trans('deliveryReceipt/message.success.delete'));
        } else {
            return Redirect::route('admin/delivery-receipt')->withInput()->with('error', trans('deliveryReceipt/message.error.delete'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrder $deliveryReceipt)
    {
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$supplier = Supplier::where('deleted', 0)->orderBy('supplier_name', 'asc')->lists('supplier_name', 'id');
		$master_data = array();
		$deliveryReceiptItem = PurchaseOrderItem::select('purchase_order_item.*', 'master_data.name as master_data_name')
								->where('purchase_order_id', $deliveryReceipt->id)
								->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
								->get();
        return view('admin.deliveryReceipt.edit', compact('supplier', 'rab', 'deliveryReceipt', 'master_data', 'deliveryReceiptItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, deliveryReceipt $deliveryReceipt)
    {
		
		$request->merge(array('shipping_fee' => preg_replace('/[\D.]/', '', str_replace('.00','',$request->shipping_fee))));
		$request->merge(array('delivery_date' => date('Y-m-d', strtotime($request->delivery_date))));
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		$request->merge(array('requested_at' => date('Y-m-d H:i:s')));
		$request->merge(array('log_app_by' => Sentinel::getUser()->id));
		$request->merge(array('log_app_at' => date('Y-m-d H:i:s')));
		
		$purchase_order_id = $request->id;
		
        if ($deliveryReceipt->update($request->all())) {
			
			$poItemId = array();
			if(isset($request->purchase_order_item)){
				foreach($request->purchase_order_item as $data)
				{ //save PurchaseOrderItem
					$deliveryReceiptItem = new PurchaseOrderItem($data);
					
					if(isset($deliveryReceiptItem->id)){
						array_push($poItemId, $deliveryReceiptItem->id);
					}else{
						$deliveryReceiptItem->id = '9'.date('md').mt_rand(1000,9999);
						array_push($poItemId, $deliveryReceiptItem->id);
						$deliveryReceiptItem->purchase_order_id = $purchase_order_id;
						$deliveryReceiptItem->created_by = Sentinel::getUser()->id;
						$deliveryReceiptItem->save();
					}
				}
			}
			if(count($poItemId) > 0) PurchaseOrderItem ::where('purchase_order_id', '=', $purchase_order_id)->whereNotIn('id', $poItemId)->delete();//delete
            return redirect('admin/delivery-receipt')->with('success', trans('deliveryReceipt/message.success.update'));
        } else {
            return Redirect::route('admin/delivery-receipt')->withInput()->with('error', trans('deliveryReceipt/message.error.update'));
        }
    }
	
	public function ajaxPOChange(AllRabRequest $request)
	{
		$term = $request->term;
		$results = PurchaseOrderItem::select('purchase_order_item.id', 
											'master_data.name as master_name', 
											'master_data.id as master_id',
											'purchase_order_item.quantity as sisa')
										->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
										->where('purchase_order_item.purchase_order_id', '=', $term)
										->where('purchase_order_item.status', '=', 0)
										->orderBy('purchase_order_item.created_at', 'asc')
										->get();
		return json_encode($results);
		/* $count = DeliveryReceipt::where('purchase_order_id', '=', $term)->count();
		
		if($request->statusDelivery == 'po' || $request->statusDelivery == 'spk'){
			if($count < 1){
				$results = PurchaseOrderItem::select('purchase_order_item.id', 
													'master_data.name as master_name', 
													'master_data.id as master_id',
													'purchase_order_item.quantity as sisa')
												->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
												->where('purchase_order_item.purchase_order_id', '=', $term)
												->orderBy('purchase_order_item.created_at', 'asc')
												->get();
			}else{
				$results = PurchaseOrderItem::select('purchase_order_item.id', 
												'master_data.name as master_name', 
												'master_data.id as master_id',
												DB::raw('SUM(purchase_order_item.quantity - delivery_receipt_item.quantity) as sisa'))
											->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
											->join('delivery_receipt_item', 'delivery_receipt_item.purchase_order_item_id', '=', 'purchase_order_item.id')
											->where('purchase_order_item.purchase_order_id', '=', $term)
											->having('sisa', '!=', 0)
											->groupBy('purchase_order_item.id')
											->orderBy('purchase_order_item.created_at', 'asc')
											->get();
			}
		}else{
			if($count < 1){
				$results = CashExpenseItem::select('cash_expense_item.id', 
													'master_data.name as master_name', 
													'master_data.id as master_id',
													'cash_expense_item.progress as sisa')
												->join('master_data', 'master_data.id', '=', 'cash_expense_item.master_data_id')
												->where('cash_expense_item.cash_expense_id', '=', $term)
												->orderBy('cash_expense_item.created_at', 'asc')
												->get();
			}else{
				$results = CashExpenseItem::select('cash_expense_item.id', 
												'master_data.name as master_name', 
												'master_data.id as master_id',
												DB::raw('SUM(cash_expense_item.progress - delivery_receipt_item.quantity) as sisa'))
											->join('master_data', 'master_data.id', '=', 'cash_expense_item.master_data_id')
											->join('delivery_receipt_item', 'delivery_receipt_item.purchase_order_id', '=', 'cash_expense_item.id')
											->where('cash_expense_item.purchase_order_id', '=', $term)
											->having('sisa', '!=', 0)
											->groupBy('cash_expense_item.id')
											->orderBy('cash_expense_item.created_at', 'asc')
											->get();
			}
		} */
	}
	//SELECT room.*, (SUM(`Status`)-SUM(`OfficeDescription`)) as total FROM `room` GROUP BY `OfficeDescription`HAVING total != 1
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = PurchaseOrder::select('purchase_order.id as ID', 
									'rab.name as RAB_Name',
									'supplier.supplier_name as Supplier_Name',
									DB::raw("(CASE WHEN purchase_order.vat = '0' THEN 'false' ELSE 'true' END) as PPn"),
									DB::raw("DATE_FORMAT(purchase_order.delivery_date, '%d %b, %Y') as Delivery_Date"),
									DB::raw("DATE_FORMAT(purchase_order.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(purchase_order.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("ROUND(REPLACE(purchase_order.shipping_fee, ',', ''),3) as Shipping_Fee"),
									'purchase_order.notes as Notes',
									'purchase_order.term_of_payment as Term_of_Payment',
									'purchase_order.requested_by as Requested_By',//acan
									DB::raw("DATE_FORMAT(purchase_order.requested_at, '%d %b, %Y %H:%i') as Requested_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By"),
									'purchase_order.approved_by as Approved_By',//acan
									'purchase_order.approved_at as Approved_At'//acan
							)
							->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
							->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
							->join('users', 'users.id', '=', 'purchase_order.created_by')
							->where(array('purchase_order.deleted' => 0))
							->get();
										
		Excel::create('delivery-receipt-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
	
	public function getModalPrintDeliveryReceipt(deliveryReceipt $deliveryReceipt)
	{
		$model = 'deliveryReceipt';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('print/deliveryReceipt', ['id' => $deliveryReceipt->id]);
            return view('admin.layouts.modal_printDeliveryReceipt', compact('error', 'model', 'confirm_route', 'deliveryReceipt'));
        } catch (GroupNotFoundException $e) {

            $error = trans('deliveryReceipt/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
	}
	
	public function printDr(AllRabRequest $request, deliveryReceipt $deliveryReceipt)
	{
		if ($request->isMethod('post')) {
			$purchaseOrderItem = PurchaseOrderItem::select('delivery_receipt_item.quantity', 'master_data.name', 'purchase_order_item.purchase_order_id', 'supplier.supplier_name','project.project_name', 'delivery_receipt.delivery_receipt_number')
											->join('delivery_receipt', 'delivery_receipt.purchase_order_id', '=', 'purchase_order_item.purchase_order_id')
											->join('delivery_receipt_item', 'delivery_receipt_item.purchase_order_item_id', '=', 'purchase_order_item.id')
											->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
											->join('purchase_order', 'purchase_order.id', '=', 'purchase_order_item.purchase_order_id')
											->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
											->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
											->join('project', 'project.id', '=', 'rab.project_id')
											->where('delivery_receipt.id', $deliveryReceipt->id)
											->get();
			$poId = ($purchaseOrderItem) ? $purchaseOrderItem[0]->purchase_order_id : '-';
			$supplierName = ($purchaseOrderItem) ? $purchaseOrderItem[0]->supplier_name : '-';
			$projectName = ($purchaseOrderItem) ? $purchaseOrderItem[0]->project_name : '-';
			$deliveryReceiptNumber = ($purchaseOrderItem) ? $purchaseOrderItem[0]->delivery_receipt_number : '-';
			$alamat1 = $request->alamat1;
			$alamat2 = $request->alamat2;
			$jam = $request->jam;
			$blok = $request->blok;
			$tgl = $request->tgl;
			$noKend = $request->noKend;
			$pm = $request->pm;
			$pl = $request->pl;
			$le = $request->le;
			$bl = $request->bl;
			
			PDF::AddPage();
			PDF::SetFont('helvetica', '', 8);
			
			$html2 = '
				<table width="100%" cellpadding="3">
					<tr>
						<td width="50%">
							<table width="100%">
								<tr>
									<td colspan="2">
										<img src="assets/images/company-logo.png" style="width: 280px; height:30px;"/>
									</td>
								</tr>
								<tr>
									<td width="25%" style="font-weight: bold; font-size:7px;"></td>
									<td width="75%" style="font-weight: bold; font-size:7px;">
										 
									</td>
								</tr>
								<tr>
									<td width="25%" style="font-weight: bold; font-size:7px;"></td>
									<td width="75%" style="font-weight: bold; font-size:7px;">
										
									</td>
								</tr>
								<tr>
									<td width="25%" style="font-weight: bold; font-size:7px;"></td>
									<td width="75%" style="font-weight: bold; font-size:7px;">
										
									</td>
								</tr>
							</table>
							<br/>
							<br/>
							<table width="100%" cellspacing="3">
								<tr>
									<td style="font-weight:bold; font-size:11px;">PENERIMAAN BARANG</td>
								</tr>
							</table>
						</td>
						<td width="50%">
							<table width="100%" cellspacing="5">
								<tr>
									<td width="35%" style="font-weight: bold;">No. </td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%" style="border-bottom: 0.5px dotted #000;">'.$deliveryReceiptNumber.'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Pengirim</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%" style="border-bottom: 0.5px dotted #000;">'.$supplierName.'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Alamat</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%" style="border-bottom: 0.1px dotted #000;">'.$alamat1.'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;"></td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%" style="border-bottom: 0.5px dotted #000;">'.$alamat2.'</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">No. Kendaraan</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%">
										<table>
											<tr>
												<td width="55%" style="border-bottom: 0.5px dotted #000;">'.$noKend.'</td>
												<td width="4%"></td>
												<td width="13%" style="font-weight: bold;">Jam</td>
												<td width="3%" style="font-weight: bold;">:</td>
												<td width="24%" style="border-bottom: 0.5px dotted #000;">'.$jam.'</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Proyek</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%">
										<table>
											<tr>
												<td width="55%" style="border-bottom: 0.5px dotted #000;">'.$projectName.'</td>
												<td width="4%"></td>
												<td width="13%" style="font-weight: bold;">Blok</td>
												<td width="3%" style="font-weight: bold;">:</td>
												<td width="24%" style="border-bottom: 0.5px dotted #000;">'.$blok.'</td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td width="35%" style="font-weight: bold;">Usul pembelian No.</td>
									<td width="2%" style="font-weight: bold;">:</td>
									<td width="63%">
										<table>
											<tr>
												<td width="55%" style="border-bottom: 0.5px dotted #000;">'.$poId.'</td>
												<td width="4%"></td>
												<td width="13%" style="font-weight: bold;">Tgl</td>
												<td width="3%" style="font-weight: bold;">:</td>
												<td width="24%" style="border-bottom: 0.5px dotted #000;">'.$tgl.'</td>
											</tr>
										</table>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				<br/>';
				
			$html2 .= '
				<table width="100%" style="border: 0.5px solid #000;" cellpadding="3">
					<tr>
						<td align="center" style="font-weight: bold; border-bottom: 0.5px solid #000;" colspan="4">
							Barang/bahan yang diterima dari pengirim tersebut diatas<br>
							telah diperiksa/lihat/ukur/dihitung dengan hasil sebagai berikut :
						</td>
					</tr>
					<tr>
						<td width="6%" align="center" style="font-weight: bold; border-bottom: 0.5px solid #000;">No. Urut</td>
						<td width="51%" align="center" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; font-weight: bold;">Nama barang/bahan</td>
						<td width="15%" align="center" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; font-weight: bold;">Jumlah</td>
						<td width="28%" align="center" valign="middle" style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000; font-weight: bold;">Keterangan</td>
					</tr>
			';
			
			if($purchaseOrderItem){
				foreach ($purchaseOrderItem as $key => $val){
				$html2 .= '<tr>
								<td style="border-bottom: 0.5px solid #000;" align="center">'.($key+1).'</td>
								<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;">'.$val->name.'</td>
								<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;" align="right">'.number_format($val->quantity,'0','.',',').'</td>
								<td style="border-left: 0.5px solid #000; border-bottom: 0.5px solid #000;"></td>
							</tr>';
				}
			}
					
			$html2 .= '					
					<tr>
						<td></td>
						<td style="border-left: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000;"></td>
						<td style="border-left: 0.5px solid #000;"></td>
					</tr>
				</table>
				<table width="100%" cellpadding="3">
					<tr>
						<td colspan="2"></td>
					</tr>
					<tr>
						<td width="80%" align="right" style="font-weight: bold;">Jakarta,</td>
						<td width="20%" style="border-bottom: 0.5px dotted #000;">'.date('d F Y').'</td>
					</tr>
				</table>
				<table width="100%" cellpadding="3">
					<tr>
						<td colspan="4"></td>
					</tr>
					<tr>
						<td width="25%" align="center" style="font-weight: bold;">Menyetujui,</td>
						<td width="25%" align="center" style="font-weight: bold;">Mengetahui,</td>
						<td width="25%" align="center" style="font-weight: bold;">Yang Menyerahkan,</td>
						<td width="25%" align="center" style="font-weight: bold;">Yang Menerima/memeriksa,</td>
					</tr>
					<tr>
						<td colspan="4"></td>
					</tr>
					<tr>
						<td align="center">'.$pm.'</td>
						<td align="center">'.$pl.'</td>
						<td align="center">'.$le.'</td>
						<td align="center">'.$bl.'</td>
					</tr>
					<tr>
						<td align="center" style="font-weight: bold;">Proyek Manager</td>
						<td align="center" style="font-weight: bold;">Pelaksana</td>
						<td align="center" style="font-weight: bold;">Leveransir</td>
						<td align="center" style="font-weight: bold;">Bag. Gudang/logistik</td>
					</tr>
				</table>
			';
			
			$html = '<table><tr><td style="text-align: right;"><i style="color: red;">Office</i></td></tr>';
			$html .= $html2;
			
			$html .= '
				<br>
				<br>
				<div style="border-top: 0.5px dashed black;"></div>
			';
			
			$html .= '<table><tr><td style="text-align: right;"><i style="color: red;">Client</i></td></tr>';
			$html .= $html2;
			
			// output the HTML content
			PDF::writeHTML($html, true, 0, true, 0);
			PDF::Output('printDeliveryReceipt.pdf');
		}else{
			return redirect('admin/delivery-receipt');
		}
	}
}