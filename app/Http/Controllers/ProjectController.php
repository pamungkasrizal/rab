<?php
namespace App\Http\Controllers;

use App\Project;
use App\Client;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
		
	} 
	
    public function index()
    {
		return view('admin.project.index');
    }
	
	public function data()
    {
        $data = Project::select('project.*', 'client.client_name', 'users.first_name', 'users.last_name')
										->join('users', 'users.id', '=', 'project.updated_by')
										->join('client', 'client.id', '=', 'project.client_id')
										->where('project.deleted', '=', 0)
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('project_start_date',function(Project $data) {
				return date('j M, Y', strtotime($data->project_start_date));
            })
			->edit_column('project_end_date',function(Project $data) {
				return date('j M, Y', strtotime($data->project_end_date));
            })
			->edit_column('created_at',function(Project $data) {
				return $data->created_at->toDayDateTimeString();
            })
			->edit_column('updated_at',function(Project $data) {
				return ($data->updated_at ? $data->updated_at->toDayDateTimeString() : '-');
            })
            ->add_column('actions',function($data) {
				if(Sentinel::inRole('admin') || Sentinel::inRole('administrasi')){
					$actions = '<a href="'.URL::to('admin/project/' . $data->id . '/edit' ).'" title="update data">
									<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
								</a>
								<a href="'.route('confirm-delete/project', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
								   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
								</a>';
				}else{
					$actions = '<a href="'.URL::to('admin/project/' . $data->id . '/view' ).'" title="update data">
									<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
								</a>';
				}
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '5'.date('md').mt_rand(1000,9999);
		$client = array();//Client::where('deleted', 0)->lists('client_name', 'id');
        return view('admin.project.create', compact('id','client'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $project = new Project($request->all());
		
		$explodeDate = explode(' - ', $request->start_end_date);
		$project->project_start_date = date('Y-m-d', strtotime($explodeDate[0]));
		$project->project_end_date = date('Y-m-d', strtotime($explodeDate[1]));
		$project->updated_by = Sentinel::getUser()->id;
		
        if ($project->save()) {
            return redirect('admin/project')->with('success', trans('project/message.success.create'));
        } else {
            return Redirect::route('admin/project')->withInput()->with('error', trans('project/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(Project $project)
    {
        $model = 'project';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/project', ['id' => $project->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('project/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
		$project->deleted = 1;
        if ($project->update($project->toArray())) {
            return redirect('admin/project')->with('success', trans('project/message.success.delete'));
        } else {
            return Redirect::route('admin/project')->withInput()->with('error', trans('project/message.error.delete'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
		$client = Client::where('deleted', 0)->lists('client_name', 'id');
        return view('admin.project.edit', compact('project', 'client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, project $project)
    {
		//dd($request->toArray());
		$explodeDate = explode(' - ', $request->start_end_date);
		$project->project_start_date = date('Y-m-d', strtotime($explodeDate[0]));
		$project->project_end_date = date('Y-m-d', strtotime($explodeDate[1]));
		$project->updated_by = Sentinel::getUser()->id;
		
        if ($project->update($request->all())) {
            return redirect('admin/project')->with('success', trans('project/message.success.update'));
        } else {
            return Redirect::route('admin/project')->withInput()->with('error', trans('project/message.error.update'));
        }
    }
	
	public function ajaxClient(AllRabRequest $request)
	{
		$term = $request->term;
		$results = Client::select('id', 'client_name as text')
										->where('deleted', '=', 0)
										->where('client_name', 'LIKE', '%'.$term.'%')
										->orderBy('client_name', 'asc')
										->get();
		return json_encode($results);
	}
	
	public function ajaxProject(AllRabRequest $request)
	{
		$term = $request->term;
		$results = Project::select('id', 'project_name as text')
										->where('deleted', '=', 0)
										->where('project_name', 'LIKE', '%'.$term.'%')
										->orderBy('project_name', 'asc')
										->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = Project::select('project.id as ID', 
									'project.project_name as Project_Name',
									'client.client_name as Client_Name',
									DB::raw("DATE_FORMAT(project.project_start_date, '%d %b, %Y') as Start_Date"),
									DB::raw("DATE_FORMAT(project.project_end_date, '%d %b, %Y') as End_Date"),
									DB::raw("DATE_FORMAT(project.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(project.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('client', 'client.id', '=', 'project.client_id')
							->join('users', 'users.id', '=', 'project.updated_by')
							->where(array('project.deleted' => 0))
							->get();
										
		Excel::create('project-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}
