<?php
namespace App\Http\Controllers;

use App\Subcontractor;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\MasterDataRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class SubcontractorController extends JoshController
{
    public function index()
	{
		return view('admin.master.subcontractor.index');
	}
	
	public function data()
    {
        $data = Subcontractor::select('subcontractor.*', 'users.first_name', 'users.last_name')
										->join('users', 'users.id', '=', 'subcontractor.updated_by')
										->where('subcontractor.deleted', '=', 0)
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('checkbox',function(Subcontractor $data) {
				return '<input type="checkbox" name="idList" value="' .$data->id. '">';
            })
			->edit_column('created_at',function(Subcontractor $data) {
				return $data->created_at->toDayDateTimeString();
            })
			->edit_column('updated_at',function(Subcontractor $data) {
				return ($data->updated_at ? $data->updated_at->toDayDateTimeString() : '-');
            })
            ->add_column('actions',function($data) {
				$actions = '<a href="'.URL::to('admin/master/subcontractor/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete-st/subcontractor', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
                return $actions;
				
            })->make(true);
		return $tables;
    }
	
	/* Add */
	public function create()
	{
		return view('admin.master.subcontractor.create');
	}
	
	public function store(MasterDataRequest $request)
    {
        $subcontractor = new Subcontractor($request->all());
		$subcontractor->updated_by = Sentinel::getUser()->id;
		
        if ($subcontractor->save()) {
            return redirect('admin/master/subcontractor')->with('success', trans('masterdata/subcontractor/message.success.create'));
        } else {
            return Redirect::route('admin/master/subcontractor')->withInput()->with('error', trans('masterdata/subcontractor/message.error.create'));
        }
    }
	
	/* Delete */
	public function getModalDelete($id)
    {
        $model = 'masterdata/subcontractor';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/subcontractor', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('masterdata/subcontractor/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function destroy($id)
    {
		$id = explode('-', $id);
        if (Subcontractor::whereIn('id', $id)->update(['deleted' => 1])) {
            return redirect('admin/master/subcontractor')->with('success', trans('masterdata/subcontractor/message.success.delete'));
        } else {
            return Redirect::route('admin/master/subcontractor')->withInput()->with('error', trans('masterdata/subcontractor/message.error.delete'));
        }
    }
	
	/* Update */
	public function edit(Subcontractor $subcontractor)
    {
        return view('admin.master.subcontractor.edit', compact('subcontractor'));
    }
	
	public function update(MasterDataRequest $request, Subcontractor $subcontractor)
    {
		$subcontractor->updated_by = Sentinel::getUser()->id;
		
        if ($subcontractor->update($request->all())) {
            return redirect('admin/master/subcontractor')->with('success', trans('masterdata/subcontractor/message.success.update'));
        } else {
            return Redirect::route('admin/master/subcontractor')->withInput()->with('error', trans('masterdata/subcontractor/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = Subcontractor::select('subcontractor.id as ID', 
									'subcontractor.subcontractor_name as subcontractor',
									DB::raw("DATE_FORMAT(subcontractor.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(subcontractor.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('users', 'users.id', '=', 'subcontractor.updated_by')
							->where(array('subcontractor.deleted' => 0))
							->get();
										
		Excel::create('subcontractor-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
	
	public function ajaxSubcontractor(MasterDataRequest $request)
	{
		$term = $request->term;
		$results = Subcontractor::select('id', 'subcontractor_name as text')
										->where('deleted', '=', 0)
										->where('subcontractor_name', 'LIKE', '%'.$term.'%')
										->orderBy('subcontractor_name', 'asc')
										->get();
		return json_encode($results);
	}
}