<?php

namespace App\Http\Controllers;

use App\PurchaseOrder;
use App\PurchaseOrderItem;
use App\ServiceOrder;
use App\ServiceOrderItem;
use App\DeliveryReceipt;
use App\DeliveryReceiptItem;
use App\InvoiceServiceOrder;
use App\InvoiceServiceOrderItem;
use App\Inventory;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use PDF;
use URL;
use View;
use Datatables;
use Redirect;

class InvoiceServiceOrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	public function __construct()
	{
		
	} 
	 
    public function index()
    {
		return view('admin.invoiceServiceOrder.index');
    }
	
	public function data()
    {
        $data = InvoiceServiceOrder::select('invoice_service_order.*')
										->where('invoice_service_order.deleted', '=', '0')
										->orderBy('invoice_service_order.created_at', 'desc')
										->get();
		
        $tables = Datatables::of($data)
            ->add_column('actions',function($data) {
				
				if(Sentinel::inRole('admin')){
				$actions = '<a href="'.URL::to('admin/invoice/service-order/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete/invoiceServiceOrder', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
				}else{
				$actions = '<a href="'.URL::to('admin/invoice/service-order/' . $data->id . '/view' ).'" title="update data">
								<i class="livicon" data-name="external-link" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA" title="view data"></i>
							</a>';
				}
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '16'.date('md').mt_rand(1000,9999);
		$po = array();
        return view('admin.invoiceServiceOrder.create', compact('id','po'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $invoiceServiceOrder = new InvoiceServiceOrder($request->all());
		$invoiceServiceOrder->created_by = Sentinel::getUser()->id;
		$invoiceServiceOrder->requested_at = date('Y-m-d H:i:s'); // lap or log
		$invoiceServiceOrder->requested_by = Sentinel::getUser()->id; // lap or log
		$invoice_id = $invoiceServiceOrder->invoice_number;
		$service_order_id = $invoiceServiceOrder->service_order_id;
		
        if ($invoiceServiceOrder->save()) {
			if(isset($request->invoice_service_order_item)){
				$countStatusPO = array();
				foreach($request->invoice_service_order_item as $data)
				{ 
					//save InvoiceServiceOrderItem
					$poItemId = '17'.date('md').mt_rand(1000,9999);
					
					$invoiceServiceOrderItem = new InvoiceServiceOrderItem($data);
					$invoiceServiceOrderItem->id = $poItemId;
					$invoiceServiceOrderItem->invoice_id = $invoice_id;
					$invoiceServiceOrderItem->service_order_id = $service_order_id;
					$invoiceServiceOrderItem->created_by = Sentinel::getUser()->id;
					$invoiceServiceOrderItem->save();
				}
			}
            return redirect('admin/invoice/service-order')->with('success', trans('invoiceServiceOrder/message.success.create'));
        } else {
            return Redirect::route('admin/invoice/service-order')->withInput()->with('error', trans('invoiceServiceOrder/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(InvoiceServiceOrder $invoiceServiceOrder)
    {
        $model = 'invoiceServiceOrder';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/invoiceServiceOrder', ['id' => $invoiceServiceOrder->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoiceServiceOrder/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function getModalApprove(InvoiceServiceOrder $invoiceServiceOrder)
    {
        $model = 'invoiceServiceOrder';
        $tittle = 'Kuitansi SPK Subkon';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('approve/invoiceServiceOrder', ['id' => $invoiceServiceOrder->id]);
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoiceServiceOrder/message.error.approve', compact('id'));
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        }
    }
	
	public function getModalReject(InvoiceServiceOrder $invoiceServiceOrder)
    {
        $model = 'invoiceServiceOrder';
        $confirm_route = $error = null;
        try {
            return view('admin.layouts.modal_reject_is', compact('error', 'model', 'confirm_route', 'invoiceServiceOrder'));
        } catch (GroupNotFoundException $e) {

            $error = trans('invoiceServiceOrder/message.error.reject', compact('id'));
            return view('admin.layouts.modal_reject', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(InvoiceServiceOrder $invoiceServiceOrder)
    {
		$invoiceServiceOrder->deleted = 1;
        if ($invoiceServiceOrder->update($invoiceServiceOrder->toArray())) {
            return redirect('admin/invoice/service-order')->with('success', trans('invoiceServiceOrder/message.success.delete'));
        } else {
            return Redirect::route('admin/invoice/service-order')->withInput()->with('error', trans('invoiceServiceOrder/message.error.delete'));
        }
    }
	
	public function approve(InvoiceServiceOrder $invoiceServiceOrder)
    {
		
		$invoiceServiceOrder->status = 1;
		$invoiceServiceOrder->status_msg = '';
		$invoiceServiceOrder->approved_by = Sentinel::getUser()->id;
		$invoiceServiceOrder->approved_at = date('Y-m-d H:i:s');
        if ($invoiceServiceOrder->update($invoiceServiceOrder->toArray())) {
            return redirect('admin/invoice/service-order')->with('success', trans('invoiceServiceOrder/message.success.approve'));
        } else {
            return Redirect::route('admin/invoice/service-order')->withInput()->with('error', trans('invoiceServiceOrder/message.error.approve'));
        }
    }
	
	public function reject(AllRabRequest $request, InvoiceServiceOrder $invoiceServiceOrder)
    {
		$invoiceServiceOrder->status = 2;
		$invoiceServiceOrder->status_msg = $request->status_msg;
        if ($invoiceServiceOrder->update($invoiceServiceOrder->toArray())) {
            return redirect('admin/invoice/service-order')->with('success', trans('invoiceServiceOrder/message.success.reject'));
        } else {
            return Redirect::route('admin/invoice/service-order')->withInput()->with('error', trans('invoiceServiceOrder/message.error.reject'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(InvoiceServiceOrder $invoiceServiceOrder)
    {
		$po = ServiceOrder::where('id', $invoiceServiceOrder->service_order_id)->lists('id', 'id');
		$podrId = array();
		$invoiceServiceOrderItem = InvoiceServiceOrderItem::select('invoice_service_order_item.*')
								->where('invoice_id', $invoiceServiceOrder->invoice_number)
								->get();//dd($invoiceServiceOrderItem->toArray());
        return view('admin.invoiceServiceOrder.edit', compact('po', 'invoiceServiceOrder', 'invoiceServiceOrderItem', 'podrId'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, InvoiceServiceOrder $invoiceServiceOrder)
    {
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		$request->merge(array('requested_at' => date('Y-m-d H:i:s')));
		
		$invoice_id = $invoiceServiceOrder->invoice_number;
		
		if ($invoiceServiceOrder->update($request->all())) {
			if(isset($request->invoice_service_order_item)){
				$poItemId = array();
				foreach($request->invoice_service_order_item as $data)
				{ //save PurchaseOrderItem
					$invoiceServiceOrderItem = new InvoiceServiceOrderItem($data);
					
					if(isset($invoiceServiceOrderItem->id)){
						array_push($poItemId, $invoiceServiceOrderItem->id);
					}else{
						$invoiceServiceOrderItem->id = '17'.date('md').mt_rand(1000,9999);
						array_push($poItemId, $invoiceServiceOrderItem->id);
						$invoiceServiceOrderItem->invoice_id = $invoice_id;
						$invoiceServiceOrderItem->created_by = Sentinel::getUser()->id;
						$invoiceServiceOrderItem->save();
					}
				}
			}
			if(count($poItemId) > 0) InvoiceServiceOrderItem ::where('invoice_id', '=', $invoice_id)->whereNotIn('id', $poItemId)->delete();//delete
            return redirect('admin/invoice/service-order')->with('success', trans('purchaseOrder/message.success.update'));
        } else {
            return Redirect::route('admin/invoice/service-order')->withInput()->with('error', trans('purchaseOrder/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = PurchaseOrder::select('purchase_order.id as ID', 
									'rab.name as RAB_Name',
									'supplier.supplier_name as Supplier_Name',
									DB::raw("(CASE WHEN purchase_order.vat = '0' THEN 'false' ELSE 'true' END) as PPn"),
									DB::raw("DATE_FORMAT(purchase_order.delivery_date, '%d %b, %Y') as Delivery_Date"),
									DB::raw("DATE_FORMAT(purchase_order.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(purchase_order.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("ROUND(REPLACE(purchase_order.shipping_fee, ',', ''),3) as Shipping_Fee"),
									'purchase_order.notes as Notes',
									'purchase_order.term_of_payment as Term_of_Payment',
									'purchase_order.requested_by as Requested_By',//acan
									DB::raw("DATE_FORMAT(purchase_order.requested_at, '%d %b, %Y %H:%i') as Requested_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By"),
									'purchase_order.approved_by as Approved_By',//acan
									'purchase_order.approved_at as Approved_At'//acan
							)
							->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
							->join('rab', 'rab.id', '=', 'purchase_order.rab_id')
							->join('users', 'users.id', '=', 'purchase_order.created_by')
							->where(array('purchase_order.deleted' => 0))
							->get();
										
		Excel::create('delivery-receipt-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}
