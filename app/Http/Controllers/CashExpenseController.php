<?php

namespace App\Http\Controllers;

use App\CashExpense;
use App\CashExpenseItem;
use App\Rab;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use PDF;
use URL;
use View;
use Datatables;
use Redirect;

class CashExpenseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
	
	public function __construct()
	{
		
	}
	
    public function index()
    {		
		return view('admin.cashExpense.index');
    }
	
	public function data()
    {
        $data = CashExpense::select('cash_expense.id', 'rab.name as rab_name', 'cash_expense.notes', 'cash_expense.requested_by','cash_expense.requested_at','cash_expense.status', 'users.first_name')
										->join('rab', 'rab.id', '=', 'cash_expense.rab_id')
										->join('users', 'users.id', '=', 'cash_expense.requested_by')
										->where('cash_expense.deleted', '=', '0')
										->orderBy('cash_expense.created_at', 'desc')
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('requested_at',function(CashExpense $data) {
				return date('j M, Y', strtotime($data->requested_at));
            })
            ->add_column('actions',function($data) {
				
				$actions = '<a href="'.route('print/cashExpense', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="print data">
								   <i class="glyphicon glyphicon-print" style="width: 18px; height: 18px;color: #337ab7;top: -2px !important;" title="print po"></i>
								</a>
							<a href="'.URL::to('admin/cash-expense/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete/cashExpense', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
                return $actions;
				
            })->make(true);
		return $tables;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '13'.date('md').mt_rand(1000,9999);
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$master_data = array();
        return view('admin.cashExpense.create', compact('id','rab','supplier','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $cashExpense = new CashExpense($request->all());
		$cashExpense->status_created = (Sentinel::inRole('kasir') || Sentinel::inRole('finance')) ? 'created' : 'request';
		
		if(Sentinel::inRole('kasir') || Sentinel::inRole('finance')){
			$cashExpense->log_app_by = Sentinel::getUser()->id;
			$cashExpense->log_app_at = date('Y-m-d H:i:s');
		}
		
		$cashExpense->created_by = Sentinel::getUser()->id;
		$cashExpense->requested_at = date('Y-m-d H:i:s'); // lap or log
		$cashExpense->requested_by = Sentinel::getUser()->id; // lap or log
		
		$cash_expense_id = $cashExpense->id;
		
        if ($cashExpense->save()) {
			if(isset($request->cash_expense_item)){
				foreach($request->cash_expense_item as $data)
				{ //save CashExpenseItem
					$cashExpenseItem = new CashExpenseItem($data);
					$cashExpenseItem->id = '9'.date('md').mt_rand(1000,9999);
					$cashExpenseItem->cash_expense_id = $cash_expense_id;
					$cashExpenseItem->unit_total_price = $cashExpenseItem->progress * $cashExpenseItem->unit_price;
					$cashExpenseItem->created_by = Sentinel::getUser()->id;
					$cashExpenseItem->save();
				}
			}
            return redirect('admin/cash-expense')->with('success', trans('cashExpense/message.success.create'));
        } else {
            return Redirect::route('admin/cash-expense')->withInput()->with('error', trans('cashExpense/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(CashExpense $cashExpense)
    {
        $model = 'cashExpense';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/cashExpense', ['id' => $cashExpense->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('cashExpense/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function getModalApprove(CashExpense $cashExpense)
    {
        $model = 'cashExpense';
        $tittle = 'Bukti Kas';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('approve/cashExpense', ['id' => $cashExpense->id]);
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        } catch (GroupNotFoundException $e) {

            $error = trans('cashExpense/message.error.approve', compact('id'));
            return view('admin.layouts.modal_approve', compact('error', 'model', 'confirm_route', 'tittle'));
        }
    }
	
	public function getModalReject(CashExpense $cashExpense)
    {
        $model = 'cashExpense';
        $confirm_route = $error = null;
        try {
            return view('admin.layouts.modal_reject_ce', compact('error', 'model', 'confirm_route', 'cashExpense'));
        } catch (GroupNotFoundException $e) {

            $error = trans('cashExpense/message.error.reject', compact('id'));
            return view('admin.layouts.modal_reject_ce', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(CashExpense $cashExpense)
    {
		$cashExpense->deleted = 1;
        if ($cashExpense->update($cashExpense->toArray())) {
            return redirect('admin/cash-expense')->with('success', trans('cashExpense/message.success.delete'));
        } else {
            return Redirect::route('admin/cash-expense')->withInput()->with('error', trans('cashExpense/message.error.delete'));
        }
    }
	
	public function approve(CashExpense $cashExpense)
    {
		$cashExpense->status = 1;
		$cashExpense->status_msg = '';
		$cashExpense->approved_by = Sentinel::getUser()->id;
		$cashExpense->approved_at = date('Y-m-d H:i:s');
        if ($cashExpense->update($cashExpense->toArray())) {
            return redirect('admin/cash-expense')->with('success', trans('cashExpense/message.success.approve'));
        } else {
            return Redirect::route('admin/cash-expense')->withInput()->with('error', trans('cashExpense/message.error.approve'));
        }
    }
	
	public function reject(AllRabRequest $request, CashExpense $cashExpense)
    {
		$cashExpense->status = 2;
		$cashExpense->status_msg = $request->status_msg;
        if ($cashExpense->update($cashExpense->toArray())) {
            return redirect('admin/cash-expense')->with('success', trans('cashExpense/message.success.reject'));
        } else {
            return Redirect::route('admin/cash-expense')->withInput()->with('error', trans('cashExpense/message.error.reject'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(CashExpense $cashExpense)
    {
		$rab = Rab::where('deleted', 0)->orderBy('name', 'asc')->lists('name', 'id');
		$master_data = array();
		$cashExpenseItem = CashExpenseItem::select('cash_expense_item.*', 'master_data.name as master_data_name')
								->where('cash_expense_id', $cashExpense->id)
								->join('master_data', 'master_data.id', '=', 'cash_expense_item.master_data_id')
								->get();
        return view('admin.cashExpense.edit', compact('supplier', 'rab', 'cashExpense', 'master_data', 'cashExpenseItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, cashExpense $cashExpense)
    {
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		$request->merge(array('requested_at' => date('Y-m-d H:i:s')));
		$request->merge(array('log_app_by' => Sentinel::getUser()->id));
		$request->merge(array('log_app_at' => date('Y-m-d H:i:s')));
		
		$cash_expense_id = $request->id;
		
        if ($cashExpense->update($request->all())) {
			
			$poItemId = array();
			if(isset($request->cash_expense_item)){
				foreach($request->cash_expense_item as $data)
				{ //save CashExpenseItem
					$cashExpenseItem = new CashExpenseItem($data);
					
					if(isset($cashExpenseItem->id)){
						array_push($poItemId, $cashExpenseItem->id);
					}else{
						$cashExpenseItem->id = '9'.date('md').mt_rand(1000,9999);
						array_push($poItemId, $cashExpenseItem->id);
						$cashExpenseItem->cash_expense_id = $cash_expense_id;
						$cashExpenseItem->unit_total_price = $cashExpenseItem->progress * $cashExpenseItem->unit_price;
						$cashExpenseItem->created_by = Sentinel::getUser()->id;
						$cashExpenseItem->save();
					}
				}
			}
			if(count($poItemId) > 0) CashExpenseItem ::where('cash_expense_id', '=', $cash_expense_id)->whereNotIn('id', $poItemId)->delete();//delete
            return redirect('admin/cash-expense')->with('success', trans('cashExpense/message.success.update'));
        } else {
            return Redirect::route('admin/cash-expense')->withInput()->with('error', trans('cashExpense/message.error.update'));
        }
    }
	
	public function ajaxRab(AllRabRequest $request)
	{
		$term = $request->term;
		$results = Rab::select('id', 'name as text')
							->where('deleted', '=', 0)
							->where('name', 'LIKE', '%'.$term.'%')
							->orderBy('name', 'asc')
							->get();
		return json_encode($results);
	}
	
	public function ajaxSupplier(AllRabRequest $request)
	{
		$term = $request->term;
		$results = Supplier::select('id', 'supplier_name as text')
								->where('deleted', '=', 0)
								->where('supplier_name', 'LIKE', '%'.$term.'%')
								->orderBy('supplier_name', 'asc')
								->get();
		return json_encode($results);
	}
	
	public function ajaxCashExpense(AllRabRequest $request)
	{
		$term = $request->term;
		$results = CashExpense::select('id', 'id as text')
								->where('deleted', '=', 0)
								->where('delivery_status', '!=', 'completed')
								->where('id', 'LIKE', '%'.$term.'%')
								->orderBy('id', 'asc')
								->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = CashExpense::select('cash_expense.id as ID', 
									'rab.name as RAB_Name',
									'supplier.supplier_name as Supplier_Name',
									DB::raw("(CASE WHEN cash_expense.vat = '0' THEN 'false' ELSE 'true' END) as PPn"),
									DB::raw("DATE_FORMAT(cash_expense.delivery_date, '%d %b, %Y') as Delivery_Date"),
									DB::raw("DATE_FORMAT(cash_expense.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(cash_expense.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("ROUND(REPLACE(cash_expense.shipping_fee, ',', ''),3) as Shipping_Fee"),
									'cash_expense.notes as Notes',
									'cash_expense.term_of_payment as Term_of_Payment',
									'cash_expense.requested_by as Requested_By',//acan
									DB::raw("DATE_FORMAT(cash_expense.requested_at, '%d %b, %Y %H:%i') as Requested_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By"),
									'cash_expense.approved_by as Approved_By',//acan
									'cash_expense.approved_at as Approved_At'//acan
							)
							->join('supplier', 'supplier.id', '=', 'cash_expense.supplier_id')
							->join('rab', 'rab.id', '=', 'cash_expense.rab_id')
							->join('users', 'users.id', '=', 'cash_expense.created_by')
							->where(array('cash_expense.deleted' => 0))
							->get();
										
		Excel::create('cash-expense-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
	
	public function getModalPrintCashExpense(cashExpense $cashExpense)
	{
		$model = 'cashExpense';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('print/cashExpense', ['id' => $cashExpense->id]);
            return view('admin.layouts.modal_printCashExpense', compact('error', 'model', 'confirm_route', 'cashExpense'));
        } catch (GroupNotFoundException $e) {

            $error = trans('cashExpense/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
	}
	
	public function printCe(AllRabRequest $request, cashExpense $cashExpense)
	{	
		//if ($request->isMethod('post')) {
			$data = Rab::select('supplier.supplier_name', 'cash_expense.notes', 'project.project_name', DB::raw('SUM(cash_expense_item.unit_total_price) as jml'))
									->join('cash_expense', 'cash_expense.rab_id', '=', 'rab.id')
									->join('cash_expense_item', 'cash_expense_item.cash_expense_id', '=', 'cash_expense.id')
									->join('project', 'project.id', '=', 'rab.project_id')
									->join('purchase_order', 'purchase_order.rab_id', '=', 'rab.id')
									->join('supplier', 'supplier.id', '=', 'purchase_order.supplier_id')
									->where('cash_expense.id', $cashExpense->id)
									->first();
			//dd($data->toArray());						
			
			/*========================pdf========================*/
			PDF::AddPage();
			PDF::SetFont('helvetica', '', 8);
			
			$html = '
				<table width="100%" cellpadding="3">
					<tr>
						<td style="text-align: center;" colspan="3">
							<b style="font-size: 13px; color: #005ce6">BUKTI KAS</b>
						</td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold; color: #005ce6"><i>Sudah Terima Dari</i></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%" style="border-bottom: 0.5px dotted #005ce6"><i>'.$data->supplier_name.'</i></td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold; color: #005ce6"><i>Untuk</i></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%" style="border-bottom: 0.5px dotted #005ce6"><i>'.$data->notes.'</i></td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold;"></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%" style="border-bottom: 0.5px dotted #005ce6"></td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold;"></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%" style="border-bottom: 0.5px dotted #005ce6"></td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold; color: #005ce6"><i>Jumlah</i></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%" style="border-bottom: 0.5px dotted #005ce6"><i>Rp. '.number_format($data->jml,'0','.',',').'</i></td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold; color: #005ce6"><i>Nama Proyek</i></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%" style="border-bottom: 0.5px dotted #005ce6"><i>'.$data->project_name.'</i></td>
					</tr>
					<tr>
						<td width="15%" style="font-weight: bold; color: #005ce6"><i>Dibayar Dengan</i></td>
						<td width="2%" style="color: #005ce6">:</td>
						<td width="83%">'.$request->dibayar.'</td>
					</tr>
				</table>
				<br>
				<br>
				<br>
				<table width="100%" cellpadding="3">
					<tr>
						<td width="18%" style="font-size: 9px; color: #005ce6; border: 0.5px solid #005ce6" align="center">KEP. BAG.</td>
						<td width="18%" style="font-size: 9px; color: #005ce6; border-top: 0.5px solid #005ce6; border-bottom: 0.5px solid #005ce6" align="center">DIREKSI</td>
						<td width="18%" style="font-size: 9px; color: #005ce6; border: 0.5px solid #005ce6" align="center">KEUANGAN</td>
						<td width="18%"></td>
						<td width="28%" style="font-weight: bold; color: #005ce6"><i>Jakarta, </i>'.date('d F Y').'</td>
					</tr>
					<tr>
						<td height="50" style="border-left: 0.5px solid #005ce6; border-right: 0.5px solid #005ce6; border-bottom: 0.5px solid #005ce6"></td>
						<td height="50" style="border-right: 0.5px solid #005ce6; border-bottom: 0.5px solid #005ce6"></td>
						<td height="50" style="border-right: 0.5px solid #005ce6; border-bottom: 0.5px solid #005ce6"></td>
						<td height="50"></td>
						<td height="50"></td>
					</tr>
				</table>
				';

			// output the HTML content
			PDF::writeHTML($html, true, 0, true, 0);
			PDF::Output('printCashExpense.pdf');
		/* }else{
			return redirect('admin/cash-expense');
		} */
	}
}