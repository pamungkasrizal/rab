<?php

namespace App\Http\Controllers;

use App\PurchaseOrderItem;
use App\PurchaseOrder;
use App\MasterData;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\AllRabRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;

class PurchaseOrderItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $dataPurchaseOrderItem = PurchaseOrderItem::select('purchase_order_item.*', 'users.first_name', 'users.last_name', 'master_data.name')
										->join('users', 'users.id', '=', 'purchase_order_item.created_by')
										->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
										->where('purchase_order_item.deleted', '=', 0)
										->get();
		return view('admin.purchaseOrderItem.index', compact('dataPurchaseOrderItem'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$id = '9'.date('md').mt_rand(1000,9999);
		$purchaseOrder = PurchaseOrder::where('deleted', 0)->orderBy('id', 'asc')->lists('id', 'id');
		$master_data = array();//MasterData::where('deleted', 0)->lists('name', 'id');
        return view('admin.purchaseOrderItem.create', compact('id','purchaseOrder','master_data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AllRabRequest $request)
    {
        $purchaseOrderItem = new PurchaseOrderItem($request->all());
		$purchaseOrderItem->unit_price = preg_replace('/[\D.]/', '', str_replace('.00','',$purchaseOrderItem->unit_price));
		$purchaseOrderItem->unit_total_price = preg_replace('/[\D.]/', '', str_replace('.00','',$purchaseOrderItem->unit_total_price));
		$purchaseOrderItem->created_by = Sentinel::getUser()->id;
		
        if ($purchaseOrderItem->save()) {
            return redirect('admin/purchase-order-item')->with('success', trans('purchaseOrderItem/message.success.create'));
        } else {
            return Redirect::route('admin/purchase-order-item')->withInput()->with('error', trans('purchaseOrderItem/message.error.create'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getModalDelete(PurchaseOrderItem $purchaseOrderItem)
    {
        $model = 'purchaseOrderItem';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/purchaseOrderItem', ['id' => $purchaseOrderItem->id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('purchaseOrderItem/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PurchaseOrderItem $purchaseOrderItem)
    {
		$purchaseOrderItem->deleted = 1;
        if ($purchaseOrderItem->update($purchaseOrderItem->toArray())) {
            return redirect('admin/purchase-order-item')->with('success', trans('purchaseOrderItem/message.success.delete'));
        } else {
            return Redirect::route('admin/purchase-order-item')->withInput()->with('error', trans('purchaseOrderItem/message.error.delete'));
        }
    }
	
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PurchaseOrderItem $purchaseOrderItem)
    {
		$purchaseOrder = PurchaseOrder::where('deleted', 0)->orderBy('id', 'asc')->lists('id', 'id');
		$master_data = MasterData::where('deleted', 0)->where('id', $purchaseOrderItem->master_data_id)->lists('name', 'id');
        return view('admin.purchaseOrderItem.edit', compact('master_data', 'purchaseOrder', 'purchaseOrderItem'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(AllRabRequest $request, purchaseOrderItem $purchaseOrderItem)
    {
		$request->merge(array('unit_price' => preg_replace('/[\D.]/', '', str_replace('.00','',$request->unit_price))));
		$request->merge(array('unit_total_price' => preg_replace('/[\D.]/', '', str_replace('.00','',$request->unit_total_price))));
		$request->merge(array('created_by' => Sentinel::getUser()->id));
		
        if ($purchaseOrderItem->update($request->all())) {
            return redirect('admin/purchase-order-item')->with('success', trans('purchaseOrderItem/message.success.update'));
        } else {
            return Redirect::route('admin/purchase-order-item')->withInput()->with('error', trans('purchaseOrderItem/message.error.update'));
        }
    }
	
	public function ajaxMaster(AllRabRequest $request)
	{
		$term = $request->term;
		$results = MasterData::select('id', 'name as text')
										->where('deleted', '=', 0)
										->where('name', 'LIKE', '%'.$term.'%')
										->orderBy('name', 'asc')
										->get();
		return json_encode($results);
	}
	
	public function ajaxPurchaseOrder(AllRabRequest $request)
	{
		$term = $request->term;
		$results = PurchaseOrder::select('id', 'id as text')
										->where('deleted', '=', 0)
										->where('id', 'LIKE', '%'.$term.'%')
										->orderBy('id', 'asc')
										->get();
		return json_encode($results);
	}
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = PurchaseOrderItem::select('purchase_order_item.id as ID', 
									'purchase_order_item.purchase_order_id as Purchase_Order_Item',
									'master_data.name as Master_Name',
									'purchase_order_item.quantity as Qty',
									'purchase_order_item.unit_price as Price',
									DB::raw("ROUND(REPLACE(purchase_order_item.unit_total_price, ',', ''),3) as Total_Price"),
									DB::raw("DATE_FORMAT(purchase_order_item.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(purchase_order_item.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Created_By")
							)
							->join('master_data', 'master_data.id', '=', 'purchase_order_item.master_data_id')
							->join('users', 'users.id', '=', 'purchase_order_item.created_by')
							->where(array('purchase_order_item.deleted' => 0))
							->get();
										
		Excel::create('purchase-order-item-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}
