<?php
namespace App\Http\Controllers;

use App\Supplier;
use App\Http\Requests;
use Sentinel;
use App\Http\Requests\MasterDataRequest;
use Illuminate\Support\Facades\Route;
use Excel;
use DB;
use URL;
use View;
use Datatables;

class SupplierController extends JoshController
{
    public function index()
	{
		return view('admin.master.supplier.index');
	}
	
	public function data()
    {
        $data = Supplier::select('supplier.*', 'users.first_name', 'users.last_name')
										->join('users', 'users.id', '=', 'supplier.updated_by')
										->where('supplier.deleted', '=', 0)
										->get();
		
        $tables = Datatables::of($data)
			->edit_column('checkbox',function(Supplier $data) {
				return '<input type="checkbox" name="idList" value="' .$data->id. '">';
            })
			->edit_column('created_at',function(Supplier $data) {
				return $data->created_at->toDayDateTimeString();
            })
            ->add_column('actions',function($data) {
				$actions = '<a href="'.URL::to('admin/master/supplier/' . $data->id . '/edit' ).'" title="update data">
								<i class="livicon" data-name="edit" data-size="18" data-loop="true" data-c="#428BCA" data-hc="#428BCA"></i>
							</a>
							<a href="'.route('confirm-delete-st/supplier', $data->id).'" data-toggle="modal" data-target="#delete_confirm" title="delete data">
							   <i class="livicon" data-name="remove-alt" data-size="18" data-loop="true" data-c="#f56954" data-hc="#f56954"></i>
							</a>';
                return $actions;
				
            })->make(true);
		return $tables;
    }
	
	/* Add */
	public function create()
	{
		return view('admin.master.supplier.create');
	}
	
	public function store(MasterDataRequest $request)
    {
        $supplier = new Supplier($request->all());
		$supplier->updated_by = Sentinel::getUser()->id;
		
        if ($supplier->save()) {
            return redirect('admin/master/supplier')->with('success', trans('masterdata/supplier/message.success.create'));
        } else {
            return Redirect::route('admin/master/supplier')->withInput()->with('error', trans('masterdata/supplier/message.error.create'));
        }
    }
	
	/* Delete */
	public function getModalDelete($id)
    {
        $model = 'masterdata/supplier';
        $confirm_route = $error = null;
        try {
            $confirm_route = route('delete/supplier', ['id' => $id]);
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        } catch (GroupNotFoundException $e) {

            $error = trans('masterdata/supplier/message.error.delete', compact('id'));
            return view('admin.layouts.modal_confirmation', compact('error', 'model', 'confirm_route'));
        }
    }
	
	public function destroy($id)
    {
		$id = explode('-', $id);
        if (Supplier::whereIn('id', $id)->update(['deleted' => 1])) {
            return redirect('admin/master/supplier')->with('success', trans('masterdata/supplier/message.success.delete'));
        } else {
            return Redirect::route('admin/master/supplier')->withInput()->with('error', trans('masterdata/supplier/message.error.delete'));
        }
    }
	
	/* Update */
	public function edit(Supplier $supplier)
    {
        return view('admin.master.supplier.edit', compact('supplier'));
    }
	
	public function update(MasterDataRequest $request, Supplier $supplier)
    {
		$supplier->updated_by = Sentinel::getUser()->id;
		
        if ($supplier->update($request->all())) {
            return redirect('admin/master/supplier')->with('success', trans('masterdata/supplier/message.success.update'));
        } else {
            return Redirect::route('admin/master/supplier')->withInput()->with('error', trans('masterdata/supplier/message.error.update'));
        }
    }
	
	/* export */
	public function exportFile()
    {
		$getRoute = Route::current()->uri();
		$ext = explode('/', $getRoute);
		$ext = $ext[count($ext)-1];
		
        $data = Supplier::select('supplier.id as ID', 
									'supplier.supplier_name as supplier',
									DB::raw("DATE_FORMAT(supplier.created_at, '%d %b, %Y %H:%i') as Created_At"),
									DB::raw("DATE_FORMAT(supplier.updated_at, '%d %b, %Y %H:%i') as Updated_At"),
									DB::raw("CONCAT(users.first_name,' ',users.last_name) as Updated_By")
							)
							->join('users', 'users.id', '=', 'supplier.updated_by')
							->where(array('supplier.deleted' => 0))
							->get();
										
		Excel::create('supplier-file-'.date('dmyHi'), function($excel) use($data) {
			$excel->sheet('Sheet 1', function($sheet) use($data) {
				$sheet->fromArray($data);
			});
		})->export($ext);
    }
}