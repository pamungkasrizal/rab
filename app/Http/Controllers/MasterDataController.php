<?php
namespace App\Http\Controllers;

use App\Http\Requests\MasterDataRequest;
use App\User;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use File;
use Hash;
use Illuminate\Http\Request;
use Lang;
use Mail;
use Redirect;
use Sentinel;
use URL;
use View;
use Datatables;
use Validator;

class MasterDataController extends Controller
{
    public function index()
	{
		return view('admin.master.index', compact('users'));
	}
}
