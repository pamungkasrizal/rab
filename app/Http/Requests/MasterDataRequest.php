<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MasterDataRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$type = $this->toArray();
		//dd($type);
		$type = isset($type['rulesValidation']) ? $type['rulesValidation'] : '';
		
		Switch($type){
			case 'serviceType':
				return [
					'service_type_name' => 'required|min:1'
				];
				break;
			case 'serviceMaster':
				return [
					'id' => 'required|min:1',
					'name' => 'required|min:1',
					'service_type' => 'required'
				];
				break;
			case 'cashExpense':
				return [
					'id' => 'required|min:1',
					'name' => 'required|min:1'
				];
				break;
			case 'inventory':
				return [
					'id' => 'required|min:1',
					'name' => 'required|min:1',
					'inventory_unit' => 'required|min:1'
				];
				break;
			case 'supplier':
				return [
					'supplier_name' => 'required|min:1'
				];
				break;
			case 'subcontractor':
				return [
					'subcontractor_name' => 'required|min:1'
				];
				break;
			default:
				return [''];
		}
    }
}
