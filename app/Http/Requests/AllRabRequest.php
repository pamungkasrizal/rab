<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class AllRabRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
		$type = $this->toArray();
		$type = isset($type['rulesValidation']) ? $type['rulesValidation'] : '';
		
		Switch($type){
			case 'client':
				return [
					'id' => 'required|min:1',
					'client_name' => 'required|min:1',
					'client_address' => 'required|min:1'
				];
				break;
			case 'project':
				return [
					'id' => 'required|min:1',
					'project_name' => 'required|min:1',
					'client_id' => 'required|min:1',
					'address' => 'required|min:1'
				];
				break;
			case 'rab':
				return [
					'name' => 'required|min:1',
					'project_id' => 'required|min:1'
				];
				break;
			case 'rabItem':
				return [
					'rab_id' => 'required|min:1',
					'master_data_id' => 'required|min:1',
					'quantity' => 'required|min:1',
					'unit_price' => 'required|min:1',
					'unit_total_price' => 'required|min:1',
				];
				break;
			case 'purchaseOrder':
				return [
					'rab_id' => 'required|min:1',
					'supplier_id' => 'required|min:1',
					//'shipping_fee' => 'required|min:1',
					//'notes' => 'required|min:1',
					'term_of_payment' => 'required|min:1',
				];
				break;
			case 'purchaseOrderItem':
				return [
					'purchase_order_id' => 'required|min:1',
					'master_data_id' => 'required|min:1',
					'quantity' => 'required|min:1',
					'unit_price' => 'required|min:1',
					'unit_total_price' => 'required|min:1',
				];
				break;
			case 'deliveryReceipt':
				return [
					'delivery_receipt_number' => 'required|min:1',
					'purchase_order_id' => 'required|min:1',
					//'notes' => 'required|min:1',
				];
				break;
			case 'serviceOrder':
				return [
					'rab_id' => 'required|min:1',
					'subcontractor_id' => 'required|min:1',
					//'notes' => 'required|min:1',
					'term_of_payment' => 'required|min:1',
				];
				break;
			case 'cashExpense':
				return [
					'rab_id' => 'required|min:1',
					//'notes' => 'required|min:1',
				];
				break;
			case 'invoicePurchaseOrder':
				return [
					'invoice_number' => 'required|min:1',
					'purchase_order_id' => 'required|min:1',
					//'notes' => 'required|min:1',
				];
				break;
			case 'invoiceServiceOrder':
				return [
					'invoice_number' => 'required|min:1',
					'service_order_id' => 'required|min:1',
					//'notes' => 'required|min:1',
				];
				break;
			default:
				return [''];
		}
    }
}