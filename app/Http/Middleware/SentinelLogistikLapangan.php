<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class SentinelLogistikLapangan
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */	
	public function handle($request, Closure $next)
    {
        if (!Sentinel::inRole('logistik_lapangan'))
			return Redirect::to('admin/unauthorized');

        return $next($request);
    }
}
