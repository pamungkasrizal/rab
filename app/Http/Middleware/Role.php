<?php

namespace App\Http\Middleware;

use Closure;
use Sentinel;
use Redirect;

class Role
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */	
	public function handle($request, Closure $next, ... $roles)
	{
		if (!Sentinel::check()) {
            if ($request->ajax()) {
                return response('Unauthorized.', 401);
            } else {
                return Redirect::to('admin/signin')->with('info', 'You must be logged in!');
            }
        }

		if(Sentinel::inRole('admin'))
			return $next($request);

		foreach($roles as $role) {
			if(Sentinel::inRole($role))
				return $next($request);
		}

		return Redirect::to('admin/unauthorized');
	}
}
