<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Project extends Model
{
    #use SoftDeletes;
	
	protected $fillable = ['id','project_name','client_id','project_start_date','project_end_date','address'];
	#protected $dates = ['deleted_at'];
    protected $table = 'project';
    protected $guarded = ['id'];
}