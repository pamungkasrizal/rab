<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InvoiceServiceOrderItem extends Model
{
    protected $fillable = ['id','invoice_id','service_order_item_id','service_order_id','retention','progress','total_price'];
	#protected $dates = ['deleted_at'];
    protected $table = 'invoice_service_order_item';
    protected $guarded = ['id'];
}
